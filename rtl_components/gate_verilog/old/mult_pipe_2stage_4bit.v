
module Mult_Pipe_2stage_syn_DW02_mult_2_stage_J1_0 ( A, B, TC, CLK, PRODUCT );
  input [3:0] A;
  input [3:0] B;
  output [7:0] PRODUCT;
  input TC, CLK;
  wire   n39, n40, n41, n42, \mult_x_1/n19 , \mult_x_1/n13 , \mult_x_1/n10 ,
         \mult_x_1/n9 , \mult_x_1/n8 , \mult_x_1/n7 , \mult_x_1/n4 , n3, n4,
         n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34;

  NAND2_X1 U1 ( .A1(B[0]), .A2(A[3]), .ZN(n3) );
  NAND2_X1 U2 ( .A1(A[2]), .A2(B[1]), .ZN(n4) );
  XOR2_X1 U3 ( .A(n3), .B(n4), .Z(n10) );
  NOR2_X1 U4 ( .A1(n3), .A2(n4), .ZN(n27) );
  FA_X1 U5 ( .A(n30), .B(n28), .CI(n5), .CO(PRODUCT[7]), .S(PRODUCT[6]) );
  FA_X1 U6 ( .A(n31), .B(n32), .CI(n6), .CO(n5), .S(PRODUCT[5]) );
  FA_X1 U7 ( .A(n33), .B(n34), .CI(n29), .CO(n6), .S(PRODUCT[4]) );
  AND2_X4 U8 ( .A1(B[1]), .A2(A[0]), .ZN(n19) );
  AND2_X4 U9 ( .A1(B[0]), .A2(A[1]), .ZN(n18) );
  AND2_X4 U10 ( .A1(B[0]), .A2(A[2]), .ZN(n16) );
  AND2_X4 U11 ( .A1(B[1]), .A2(A[1]), .ZN(n8) );
  AND2_X4 U12 ( .A1(B[2]), .A2(A[0]), .ZN(n7) );
  AND2_X4 U13 ( .A1(B[2]), .A2(A[1]), .ZN(n14) );
  AND2_X4 U14 ( .A1(B[3]), .A2(A[0]), .ZN(n13) );
  HA_X1 U15 ( .A(n8), .B(n7), .CO(n12), .S(n15) );
  FA_X1 U16 ( .A(n11), .B(n10), .CI(n9), .CO(\mult_x_1/n4 ), .S(n39) );
  FA_X1 U17 ( .A(n14), .B(n13), .CI(n12), .CO(\mult_x_1/n13 ), .S(n9) );
  FA_X1 U18 ( .A(n17), .B(n16), .CI(n15), .CO(n11), .S(n40) );
  HA_X1 U19 ( .A(n19), .B(n18), .CO(n17), .S(n41) );
  AND2_X4 U20 ( .A1(A[3]), .A2(B[2]), .ZN(n22) );
  AND2_X4 U21 ( .A1(B[3]), .A2(A[2]), .ZN(n21) );
  AND2_X4 U22 ( .A1(A[3]), .A2(B[1]), .ZN(n24) );
  AND2_X4 U23 ( .A1(B[3]), .A2(A[1]), .ZN(n23) );
  FA_X1 U24 ( .A(n22), .B(n21), .CI(n20), .CO(\mult_x_1/n7 ), .S(\mult_x_1/n8 ) );
  AND2_X4 U25 ( .A1(B[2]), .A2(A[2]), .ZN(n26) );
  HA_X1 U26 ( .A(n24), .B(n23), .CO(n20), .S(n25) );
  FA_X1 U27 ( .A(n27), .B(n26), .CI(n25), .CO(\mult_x_1/n9 ), .S(
        \mult_x_1/n10 ) );
  AND2_X4 U28 ( .A1(B[3]), .A2(A[3]), .ZN(\mult_x_1/n19 ) );
  AND2_X4 U29 ( .A1(B[0]), .A2(A[0]), .ZN(n42) );
  DFF_X1 CLK_r_REG10_S1 ( .D(n42), .CK(CLK), .Q(PRODUCT[0]) );
  DFF_X1 \mult_x_1/CLK_r_REG0_S1  ( .D(\mult_x_1/n19 ), .CK(CLK), .Q(n28) );
  DFF_X1 \mult_x_1/CLK_r_REG5_S1  ( .D(\mult_x_1/n4 ), .CK(CLK), .Q(n29) );
  DFF_X1 \mult_x_1/CLK_r_REG1_S1  ( .D(\mult_x_1/n7 ), .CK(CLK), .Q(n30) );
  DFF_X1 \mult_x_1/CLK_r_REG2_S1  ( .D(\mult_x_1/n8 ), .CK(CLK), .Q(n31) );
  DFF_X1 \mult_x_1/CLK_r_REG3_S1  ( .D(\mult_x_1/n9 ), .CK(CLK), .Q(n32) );
  DFF_X1 \mult_x_1/CLK_r_REG4_S1  ( .D(\mult_x_1/n10 ), .CK(CLK), .Q(n33) );
  DFF_X1 \mult_x_1/CLK_r_REG8_S1  ( .D(\mult_x_1/n13 ), .CK(CLK), .Q(n34) );
  DFF_X1 CLK_r_REG9_S1 ( .D(n41), .CK(CLK), .Q(PRODUCT[1]) );
  DFF_X1 CLK_r_REG7_S1 ( .D(n40), .CK(CLK), .Q(PRODUCT[2]) );
  DFF_X1 CLK_r_REG6_S1 ( .D(n39), .CK(CLK), .Q(PRODUCT[3]) );
endmodule


module Mult_Pipe_2stage_syn ( A, B, CLK, Z );
  input [3:0] A;
  input [3:0] B;
  output [7:0] Z;
  input CLK;


  Mult_Pipe_2stage_syn_DW02_mult_2_stage_J1_0 \Mult_Pipe_2stage_Mult_Pipe_2stage_dw0/Designware.Mult_Pipe_2stage_DW02_mult_2_stage_None0  ( 
        .A(A), .B(B), .TC(1'b0), .CLK(CLK), .PRODUCT(Z) );
endmodule

