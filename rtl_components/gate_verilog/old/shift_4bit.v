
module Shift_DESIGN0_WIDTH4_SWIDTH2_ZWIDTH4 ( A, S, Z );
  input [3:0] A;
  input [1:0] S;
  output [3:0] Z;
  wire   net225, net226, net227, net228, net230, net235, net229, net224,
         net219, net218, n1, n2, n3, n4, n5, n6, n7;

  OAI21_X2 U1 ( .B1(net228), .B2(net218), .A(net229), .ZN(Z[1]) );
  INV_X2 U2 ( .A(net225), .ZN(n1) );
  OAI211_X2 U3 ( .C1(n1), .C2(net219), .A(n3), .B(n2), .ZN(Z[3]) );
  OAI221_X2 U4 ( .B1(n7), .B2(A[3]), .C1(A[2]), .C2(n4), .A(n5), .ZN(n2) );
  INV_X4 U5 ( .A(S[1]), .ZN(n5) );
  NAND3_X1 U6 ( .A1(net227), .A2(A[1]), .A3(n5), .ZN(net229) );
  INV_X4 U7 ( .A(n5), .ZN(net235) );
  INV_X4 U8 ( .A(n7), .ZN(n4) );
  NAND3_X1 U9 ( .A1(A[1]), .A2(net235), .A3(n4), .ZN(n3) );
  AOI21_X4 U10 ( .B1(A[2]), .B2(n4), .A(net226), .ZN(net224) );
  INV_X8 U11 ( .A(n6), .ZN(n7) );
  NAND2_X2 U12 ( .A1(n7), .A2(A[0]), .ZN(net218) );
  NAND2_X4 U13 ( .A1(n7), .A2(A[1]), .ZN(net227) );
  INV_X4 U14 ( .A(S[0]), .ZN(n6) );
  OAI22_X2 U15 ( .A1(net224), .A2(net235), .B1(net225), .B2(net219), .ZN(Z[2])
         );
  INV_X4 U16 ( .A(net218), .ZN(net225) );
  NAND2_X2 U17 ( .A1(S[1]), .A2(A[0]), .ZN(net219) );
  INV_X4 U18 ( .A(net219), .ZN(net228) );
  INV_X1 U19 ( .A(A[0]), .ZN(net230) );
  NOR3_X2 U20 ( .A1(net225), .A2(net228), .A3(net230), .ZN(Z[0]) );
  INV_X4 U21 ( .A(net227), .ZN(net226) );
endmodule


module Shift_syn ( A, S, Z );
  input [3:0] A;
  input [1:0] S;
  output [3:0] Z;
  wire   n1, n2;

  Shift_DESIGN0_WIDTH4_SWIDTH2_ZWIDTH4 Shift_Shift_beh0 ( .A({A[3:2], n2, A[0]}), .S(S), .Z(Z) );
  INV_X2 U1 ( .A(A[1]), .ZN(n1) );
  INV_X4 U2 ( .A(n1), .ZN(n2) );
endmodule

