
module Mult_Pipe_2stage_syn_DW02_mult_2_stage_J1_0 ( A, B, TC, CLK, PRODUCT );
  input [7:0] A;
  input [7:0] B;
  output [15:0] PRODUCT;
  input TC, CLK;
  wire   n277, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18,
         n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32,
         n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46,
         n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60,
         n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74,
         n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88,
         n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
         n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112,
         n113, n114, n115, n116, n117, n118, n119, n120, n121, n122, n123,
         n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134,
         n135, n136, n137, n138, n139, n140, n141, n142, n143, n144, n145,
         n146, n147, n148, n149, n150, n151, n152, n153, n154, n155, n156,
         n157, n158, n159, n160, n161, n162, n163, n165, n166, n167, n168,
         n169, n170, n171, n172, n173, n174, n175, n176, n177, n178, n179,
         n180, n213, n214, n215, n216, n217, n218, n219, n220, n221, n222,
         n223, n224, n225, n227, n228;

  FA_X1 U1 ( .A(n225), .B(n228), .CI(n5), .CO(PRODUCT[15]), .S(PRODUCT[14]) );
  FA_X1 U2 ( .A(n223), .B(n224), .CI(n6), .CO(n5), .S(PRODUCT[13]) );
  FA_X1 U3 ( .A(n221), .B(n222), .CI(n7), .CO(n6), .S(PRODUCT[12]) );
  FA_X1 U4 ( .A(n220), .B(n219), .CI(n8), .CO(n7), .S(PRODUCT[11]) );
  FA_X1 U5 ( .A(n218), .B(n217), .CI(n9), .CO(n8), .S(PRODUCT[10]) );
  FA_X1 U6 ( .A(n216), .B(n215), .CI(n10), .CO(n9), .S(PRODUCT[9]) );
  FA_X1 U7 ( .A(n214), .B(n213), .CI(n227), .CO(n10), .S(PRODUCT[8]) );
  FA_X1 U8 ( .A(n179), .B(n180), .CI(n165), .CO(n11), .S(n277) );
  INV_X4 U9 ( .A(B[3]), .ZN(n96) );
  INV_X4 U10 ( .A(A[3]), .ZN(n93) );
  NOR2_X1 U11 ( .A1(n96), .A2(n93), .ZN(n135) );
  INV_X4 U12 ( .A(B[4]), .ZN(n94) );
  INV_X4 U13 ( .A(A[2]), .ZN(n97) );
  NOR2_X1 U14 ( .A1(n94), .A2(n97), .ZN(n134) );
  INV_X4 U15 ( .A(B[0]), .ZN(n163) );
  INV_X4 U16 ( .A(A[5]), .ZN(n87) );
  NOR2_X1 U17 ( .A1(n163), .A2(n87), .ZN(n13) );
  INV_X4 U18 ( .A(B[1]), .ZN(n77) );
  INV_X4 U19 ( .A(A[4]), .ZN(n95) );
  NOR2_X1 U20 ( .A1(n77), .A2(n95), .ZN(n12) );
  NOR2_X1 U21 ( .A1(n163), .A2(n95), .ZN(n24) );
  NOR2_X1 U22 ( .A1(n77), .A2(n93), .ZN(n23) );
  INV_X4 U23 ( .A(A[1]), .ZN(n89) );
  NOR2_X1 U24 ( .A1(n94), .A2(n89), .ZN(n18) );
  HA_X1 U25 ( .A(n13), .B(n12), .CO(n133), .S(n17) );
  INV_X4 U26 ( .A(B[2]), .ZN(n88) );
  NOR2_X1 U27 ( .A1(n88), .A2(n93), .ZN(n16) );
  INV_X4 U28 ( .A(B[5]), .ZN(n98) );
  INV_X4 U29 ( .A(A[0]), .ZN(n162) );
  NOR2_X1 U30 ( .A1(n98), .A2(n162), .ZN(n15) );
  NOR2_X1 U31 ( .A1(n96), .A2(n97), .ZN(n14) );
  INV_X4 U32 ( .A(A[6]), .ZN(n76) );
  NOR2_X1 U33 ( .A1(n163), .A2(n76), .ZN(n117) );
  NOR2_X1 U34 ( .A1(n77), .A2(n87), .ZN(n116) );
  NOR2_X1 U35 ( .A1(n88), .A2(n95), .ZN(n120) );
  INV_X4 U36 ( .A(B[6]), .ZN(n90) );
  NOR2_X1 U37 ( .A1(n90), .A2(n162), .ZN(n119) );
  NOR2_X1 U38 ( .A1(n98), .A2(n89), .ZN(n118) );
  FA_X1 U39 ( .A(n16), .B(n15), .CI(n14), .CO(n150), .S(n22) );
  NOR2_X1 U40 ( .A1(n88), .A2(n97), .ZN(n30) );
  NOR2_X1 U41 ( .A1(n94), .A2(n162), .ZN(n29) );
  NOR2_X1 U42 ( .A1(n96), .A2(n89), .ZN(n28) );
  FA_X1 U43 ( .A(n19), .B(n18), .CI(n17), .CO(n45), .S(n20) );
  FA_X1 U44 ( .A(n22), .B(n21), .CI(n20), .CO(n39), .S(n43) );
  HA_X1 U45 ( .A(n24), .B(n23), .CO(n19), .S(n27) );
  NOR2_X1 U46 ( .A1(n163), .A2(n93), .ZN(n34) );
  NOR2_X1 U47 ( .A1(n77), .A2(n97), .ZN(n33) );
  NOR2_X1 U48 ( .A1(n88), .A2(n89), .ZN(n37) );
  NOR2_X1 U49 ( .A1(n96), .A2(n162), .ZN(n36) );
  NOR2_X1 U50 ( .A1(n77), .A2(n89), .ZN(n32) );
  NOR2_X1 U51 ( .A1(n88), .A2(n162), .ZN(n31) );
  FA_X1 U52 ( .A(n27), .B(n26), .CI(n25), .CO(n42), .S(n49) );
  FA_X1 U53 ( .A(n30), .B(n29), .CI(n28), .CO(n21), .S(n48) );
  NOR2_X1 U54 ( .A1(n77), .A2(n162), .ZN(n57) );
  NOR2_X1 U55 ( .A1(n163), .A2(n89), .ZN(n56) );
  NOR2_X1 U56 ( .A1(n163), .A2(n97), .ZN(n54) );
  HA_X1 U57 ( .A(n32), .B(n31), .CO(n35), .S(n53) );
  HA_X1 U58 ( .A(n34), .B(n33), .CO(n26), .S(n51) );
  FA_X1 U59 ( .A(n37), .B(n36), .CI(n35), .CO(n25), .S(n50) );
  FA_X1 U60 ( .A(n40), .B(n39), .CI(n38), .CO(n165), .S(PRODUCT[6]) );
  FA_X1 U61 ( .A(n43), .B(n42), .CI(n41), .CO(n38), .S(PRODUCT[5]) );
  FA_X1 U62 ( .A(n46), .B(n45), .CI(n44), .CO(n180), .S(n40) );
  FA_X1 U63 ( .A(n49), .B(n48), .CI(n47), .CO(n41), .S(PRODUCT[4]) );
  FA_X1 U64 ( .A(n52), .B(n51), .CI(n50), .CO(n47), .S(PRODUCT[3]) );
  FA_X1 U65 ( .A(n55), .B(n54), .CI(n53), .CO(n52), .S(PRODUCT[2]) );
  HA_X1 U66 ( .A(n57), .B(n56), .CO(n55), .S(PRODUCT[1]) );
  INV_X4 U67 ( .A(A[7]), .ZN(n161) );
  NOR2_X1 U68 ( .A1(n161), .A2(n90), .ZN(n60) );
  INV_X4 U69 ( .A(B[7]), .ZN(n160) );
  NOR2_X1 U70 ( .A1(n160), .A2(n76), .ZN(n59) );
  NOR2_X1 U71 ( .A1(n161), .A2(n98), .ZN(n63) );
  NOR2_X1 U72 ( .A1(n160), .A2(n87), .ZN(n62) );
  NOR2_X1 U73 ( .A1(n90), .A2(n76), .ZN(n61) );
  FA_X1 U74 ( .A(n60), .B(n59), .CI(n58), .CO(n166), .S(n167) );
  FA_X1 U75 ( .A(n63), .B(n62), .CI(n61), .CO(n58), .S(n69) );
  NOR2_X1 U76 ( .A1(n161), .A2(n94), .ZN(n66) );
  NOR2_X1 U77 ( .A1(n160), .A2(n95), .ZN(n65) );
  NOR2_X1 U78 ( .A1(n98), .A2(n76), .ZN(n64) );
  NOR2_X1 U79 ( .A1(n161), .A2(n96), .ZN(n75) );
  NOR2_X1 U80 ( .A1(n160), .A2(n93), .ZN(n74) );
  NOR2_X1 U81 ( .A1(n94), .A2(n76), .ZN(n73) );
  NOR2_X1 U82 ( .A1(n90), .A2(n87), .ZN(n71) );
  FA_X1 U83 ( .A(n66), .B(n65), .CI(n64), .CO(n68), .S(n70) );
  FA_X1 U84 ( .A(n69), .B(n68), .CI(n67), .CO(n168), .S(n169) );
  FA_X1 U85 ( .A(n72), .B(n71), .CI(n70), .CO(n67), .S(n80) );
  NOR2_X1 U86 ( .A1(n98), .A2(n87), .ZN(n101) );
  NOR2_X1 U87 ( .A1(n90), .A2(n95), .ZN(n100) );
  NOR2_X1 U88 ( .A1(n161), .A2(n88), .ZN(n83) );
  NOR2_X1 U89 ( .A1(n160), .A2(n97), .ZN(n82) );
  NOR2_X1 U90 ( .A1(n96), .A2(n76), .ZN(n81) );
  FA_X1 U91 ( .A(n75), .B(n74), .CI(n73), .CO(n72), .S(n104) );
  NOR2_X1 U92 ( .A1(n94), .A2(n87), .ZN(n86) );
  NOR2_X1 U93 ( .A1(n90), .A2(n93), .ZN(n85) );
  NOR2_X1 U94 ( .A1(n98), .A2(n95), .ZN(n84) );
  NOR2_X1 U95 ( .A1(n96), .A2(n87), .ZN(n113) );
  NOR2_X1 U96 ( .A1(n94), .A2(n95), .ZN(n112) );
  NOR2_X1 U97 ( .A1(n160), .A2(n89), .ZN(n111) );
  NOR2_X1 U98 ( .A1(n161), .A2(n77), .ZN(n92) );
  NOR2_X1 U99 ( .A1(n88), .A2(n76), .ZN(n91) );
  NOR2_X1 U100 ( .A1(n98), .A2(n93), .ZN(n110) );
  NOR2_X1 U101 ( .A1(n90), .A2(n97), .ZN(n109) );
  NOR2_X1 U102 ( .A1(n161), .A2(n163), .ZN(n115) );
  NOR2_X1 U103 ( .A1(n77), .A2(n76), .ZN(n114) );
  FA_X1 U104 ( .A(n80), .B(n79), .CI(n78), .CO(n170), .S(n171) );
  FA_X1 U105 ( .A(n83), .B(n82), .CI(n81), .CO(n99), .S(n126) );
  FA_X1 U106 ( .A(n86), .B(n85), .CI(n84), .CO(n103), .S(n125) );
  NOR2_X1 U107 ( .A1(n88), .A2(n87), .ZN(n138) );
  NOR2_X1 U108 ( .A1(n160), .A2(n162), .ZN(n137) );
  NOR2_X1 U109 ( .A1(n90), .A2(n89), .ZN(n136) );
  HA_X1 U110 ( .A(n92), .B(n91), .CO(n122), .S(n140) );
  NOR2_X1 U111 ( .A1(n94), .A2(n93), .ZN(n132) );
  NOR2_X1 U112 ( .A1(n96), .A2(n95), .ZN(n131) );
  NOR2_X1 U113 ( .A1(n98), .A2(n97), .ZN(n130) );
  FA_X1 U114 ( .A(n101), .B(n100), .CI(n99), .CO(n79), .S(n106) );
  FA_X1 U115 ( .A(n104), .B(n103), .CI(n102), .CO(n78), .S(n105) );
  FA_X1 U116 ( .A(n107), .B(n106), .CI(n105), .CO(n172), .S(n173) );
  FA_X1 U117 ( .A(n110), .B(n109), .CI(n108), .CO(n121), .S(n144) );
  FA_X1 U118 ( .A(n113), .B(n112), .CI(n111), .CO(n123), .S(n143) );
  HA_X1 U119 ( .A(n115), .B(n114), .CO(n108), .S(n153) );
  HA_X1 U120 ( .A(n117), .B(n116), .CO(n152), .S(n149) );
  FA_X1 U121 ( .A(n120), .B(n119), .CI(n118), .CO(n151), .S(n148) );
  FA_X1 U122 ( .A(n123), .B(n122), .CI(n121), .CO(n102), .S(n128) );
  FA_X1 U123 ( .A(n126), .B(n125), .CI(n124), .CO(n107), .S(n127) );
  FA_X1 U124 ( .A(n129), .B(n128), .CI(n127), .CO(n174), .S(n175) );
  FA_X1 U125 ( .A(n132), .B(n131), .CI(n130), .CO(n139), .S(n156) );
  FA_X1 U126 ( .A(n135), .B(n134), .CI(n133), .CO(n155), .S(n46) );
  FA_X1 U127 ( .A(n138), .B(n137), .CI(n136), .CO(n141), .S(n154) );
  FA_X1 U128 ( .A(n141), .B(n140), .CI(n139), .CO(n124), .S(n146) );
  FA_X1 U129 ( .A(n144), .B(n143), .CI(n142), .CO(n129), .S(n145) );
  FA_X1 U130 ( .A(n147), .B(n146), .CI(n145), .CO(n176), .S(n177) );
  FA_X1 U131 ( .A(n150), .B(n149), .CI(n148), .CO(n159), .S(n44) );
  FA_X1 U132 ( .A(n153), .B(n152), .CI(n151), .CO(n142), .S(n158) );
  FA_X1 U133 ( .A(n156), .B(n155), .CI(n154), .CO(n147), .S(n157) );
  FA_X1 U134 ( .A(n159), .B(n158), .CI(n157), .CO(n178), .S(n179) );
  NOR2_X1 U136 ( .A1(n163), .A2(n162), .ZN(PRODUCT[0]) );
  SDFF_X2 CLK_r_REG24_S1 ( .D(B[7]), .SI(1'b0), .SE(n161), .CK(CLK), .Q(n228)
         );
  DFF_X1 CLK_r_REG26_S1 ( .D(n11), .CK(CLK), .Q(n227) );
  DFF_X1 CLK_r_REG27_S1 ( .D(n277), .CK(CLK), .Q(PRODUCT[7]) );
  DFF_X1 CLK_r_REG38_S1 ( .D(n166), .CK(CLK), .Q(n225) );
  DFF_X1 CLK_r_REG39_S1 ( .D(n167), .CK(CLK), .Q(n224) );
  DFF_X1 CLK_r_REG36_S1 ( .D(n168), .CK(CLK), .Q(n223) );
  DFF_X1 CLK_r_REG37_S1 ( .D(n169), .CK(CLK), .Q(n222) );
  DFF_X1 CLK_r_REG34_S1 ( .D(n170), .CK(CLK), .Q(n221) );
  DFF_X1 CLK_r_REG35_S1 ( .D(n171), .CK(CLK), .Q(n220) );
  DFF_X1 CLK_r_REG32_S1 ( .D(n172), .CK(CLK), .Q(n219) );
  DFF_X1 CLK_r_REG33_S1 ( .D(n173), .CK(CLK), .Q(n218) );
  DFF_X1 CLK_r_REG30_S1 ( .D(n174), .CK(CLK), .Q(n217) );
  DFF_X1 CLK_r_REG31_S1 ( .D(n175), .CK(CLK), .Q(n216) );
  DFF_X1 CLK_r_REG28_S1 ( .D(n176), .CK(CLK), .Q(n215) );
  DFF_X1 CLK_r_REG29_S1 ( .D(n177), .CK(CLK), .Q(n214) );
  DFF_X1 CLK_r_REG25_S1 ( .D(n178), .CK(CLK), .Q(n213) );
endmodule


module Mult_Pipe_2stage_syn ( A, B, CLK, Z );
  input [7:0] A;
  input [7:0] B;
  output [15:0] Z;
  input CLK;
  wire   n15, n16, n17, n18, n19, n20, n21;

  DFF_X1 CLK_r_REG40_S1 ( .D(n15), .CK(CLK), .Q(Z[6]) );
  DFF_X1 CLK_r_REG41_S1 ( .D(n16), .CK(CLK), .Q(Z[5]) );
  DFF_X1 CLK_r_REG42_S1 ( .D(n17), .CK(CLK), .Q(Z[4]) );
  DFF_X1 CLK_r_REG43_S1 ( .D(n18), .CK(CLK), .Q(Z[3]) );
  DFF_X1 CLK_r_REG44_S1 ( .D(n19), .CK(CLK), .Q(Z[2]) );
  DFF_X1 CLK_r_REG45_S1 ( .D(n20), .CK(CLK), .Q(Z[1]) );
  DFF_X1 CLK_r_REG46_S1 ( .D(n21), .CK(CLK), .Q(Z[0]) );
  Mult_Pipe_2stage_syn_DW02_mult_2_stage_J1_0 \Mult_Pipe_2stage_Mult_Pipe_2stage_dw0/Designware.Mult_Pipe_2stage_DW02_mult_2_stage_None0  ( 
        .A(A), .B(B), .TC(1'b0), .CLK(CLK), .PRODUCT({Z[15:7], n15, n16, n17, 
        n18, n19, n20, n21}) );
endmodule

