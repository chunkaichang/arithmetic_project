
module Add_DESIGN3_WIDTH4_LOGDEPTH2_MAX_LF_ARITY4 ( A, B, Z );
  input [3:0] A;
  input [3:0] B;
  output [3:0] Z;
  wire   net211, net212, net213, net214, net218, net232, net233, net237,
         net241, net243, net249, net300, net303, net227, net223, net221,
         net219, net217, net216, net230, net226, net224, net222, n1, n2, n3,
         n4, n5, n6;

  NAND2_X2 U1 ( .A1(net232), .A2(net233), .ZN(n6) );
  BUF_X16 U2 ( .A(A[0]), .Z(net303) );
  INV_X2 U3 ( .A(A[1]), .ZN(n1) );
  INV_X4 U4 ( .A(n1), .ZN(n2) );
  XNOR2_X1 U5 ( .A(B[1]), .B(n2), .ZN(net241) );
  INV_X2 U6 ( .A(net243), .ZN(net249) );
  INV_X4 U7 ( .A(net237), .ZN(net300) );
  INV_X8 U8 ( .A(net303), .ZN(net232) );
  NAND2_X2 U9 ( .A1(B[0]), .A2(A[0]), .ZN(n3) );
  INV_X4 U10 ( .A(n3), .ZN(net230) );
  INV_X1 U11 ( .A(B[0]), .ZN(net233) );
  NAND2_X2 U12 ( .A1(net222), .A2(net224), .ZN(net226) );
  XNOR2_X2 U13 ( .A(net226), .B(net227), .ZN(Z[2]) );
  OAI21_X2 U14 ( .B1(n2), .B2(B[1]), .A(net230), .ZN(net222) );
  INV_X2 U15 ( .A(net222), .ZN(net221) );
  NAND4_X2 U16 ( .A1(net222), .A2(net237), .A3(net216), .A4(net224), .ZN(
        net211) );
  INV_X1 U17 ( .A(net230), .ZN(net243) );
  NAND2_X2 U18 ( .A1(B[1]), .A2(n2), .ZN(net224) );
  INV_X4 U19 ( .A(net224), .ZN(net223) );
  NAND2_X2 U20 ( .A1(net216), .A2(net219), .ZN(net227) );
  NAND2_X2 U21 ( .A1(B[2]), .A2(A[2]), .ZN(net216) );
  OAI21_X2 U22 ( .B1(net300), .B2(net216), .A(net217), .ZN(net214) );
  OAI21_X2 U23 ( .B1(net218), .B2(net219), .A(net216), .ZN(net217) );
  NAND2_X2 U24 ( .A1(n4), .A2(n5), .ZN(net219) );
  NAND3_X2 U25 ( .A1(net300), .A2(net221), .A3(net219), .ZN(net213) );
  NAND3_X1 U26 ( .A1(net223), .A2(net219), .A3(net218), .ZN(net212) );
  INV_X4 U27 ( .A(B[2]), .ZN(n5) );
  INV_X4 U28 ( .A(A[2]), .ZN(n4) );
  XNOR2_X2 U29 ( .A(net241), .B(net249), .ZN(Z[1]) );
  XNOR2_X2 U30 ( .A(B[3]), .B(A[3]), .ZN(net218) );
  INV_X4 U31 ( .A(net218), .ZN(net237) );
  AND2_X2 U32 ( .A1(net243), .A2(n6), .ZN(Z[0]) );
  NAND4_X2 U33 ( .A1(net214), .A2(net213), .A3(net211), .A4(net212), .ZN(Z[3])
         );
endmodule


module Add_syn ( A, B, Z );
  input [3:0] A;
  input [3:0] B;
  output [3:0] Z;


  Add_DESIGN3_WIDTH4_LOGDEPTH2_MAX_LF_ARITY4 Add_Add_serial0 ( .A(A), .B(B), 
        .Z(Z) );
endmodule

