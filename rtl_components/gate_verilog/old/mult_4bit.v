
module FA_DESIGN0_15 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4;

  OAI22_X1 U1 ( .A1(n4), .A2(n3), .B1(n2), .B2(n1), .ZN(Cout) );
  XNOR2_X2 U2 ( .A(Cin), .B(n4), .ZN(S) );
  INV_X4 U3 ( .A(A), .ZN(n2) );
  XOR2_X2 U4 ( .A(n2), .B(B), .Z(n4) );
  INV_X4 U5 ( .A(Cin), .ZN(n3) );
  INV_X4 U6 ( .A(B), .ZN(n1) );
endmodule


module FA_DESIGN0_14 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4, n5;

  BUF_X32 U1 ( .A(B), .Z(n1) );
  XNOR2_X2 U2 ( .A(A), .B(B), .ZN(n5) );
  OAI22_X1 U3 ( .A1(n5), .A2(n4), .B1(n3), .B2(n2), .ZN(Cout) );
  XNOR2_X2 U4 ( .A(Cin), .B(n5), .ZN(S) );
  INV_X4 U5 ( .A(A), .ZN(n3) );
  INV_X4 U6 ( .A(Cin), .ZN(n4) );
  INV_X4 U7 ( .A(n1), .ZN(n2) );
endmodule


module Cpr_DESIGN1_DEPTH4_7 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_15 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), 
        .B(A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_14 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), 
        .B(\ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_0 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n3;

  XOR2_X2 U1 ( .A(Cin), .B(n3), .Z(S) );
  AOI22_X2 U2 ( .A1(B), .A2(A), .B1(n3), .B2(Cin), .ZN(n1) );
  XOR2_X2 U3 ( .A(A), .B(B), .Z(n3) );
  INV_X4 U4 ( .A(n1), .ZN(Cout) );
endmodule


module FA_DESIGN0_1 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n3;

  XOR2_X2 U1 ( .A(Cin), .B(n3), .Z(S) );
  AOI22_X2 U2 ( .A1(B), .A2(A), .B1(n3), .B2(Cin), .ZN(n1) );
  XOR2_X2 U3 ( .A(A), .B(B), .Z(n3) );
  INV_X4 U4 ( .A(n1), .ZN(Cout) );
endmodule


module Cpr_DESIGN1_DEPTH4_0 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_1 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), .B(
        A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_0 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), .B(
        \ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_2 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4;

  XNOR2_X2 U1 ( .A(Cin), .B(n4), .ZN(S) );
  INV_X4 U2 ( .A(A), .ZN(n2) );
  XOR2_X2 U3 ( .A(n2), .B(B), .Z(n4) );
  INV_X4 U4 ( .A(Cin), .ZN(n3) );
  INV_X4 U5 ( .A(B), .ZN(n1) );
  OAI22_X2 U6 ( .A1(n4), .A2(n3), .B1(n2), .B2(n1), .ZN(Cout) );
endmodule


module FA_DESIGN0_3 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n3;

  XOR2_X2 U1 ( .A(Cin), .B(n3), .Z(S) );
  AOI22_X2 U2 ( .A1(B), .A2(A), .B1(n3), .B2(Cin), .ZN(n1) );
  XOR2_X2 U3 ( .A(A), .B(B), .Z(n3) );
  INV_X4 U4 ( .A(n1), .ZN(Cout) );
endmodule


module Cpr_DESIGN1_DEPTH4_1 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_3 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), .B(
        A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_2 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), .B(
        \ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_4 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4;

  INV_X1 U1 ( .A(B), .ZN(n3) );
  OAI21_X1 U2 ( .B1(B), .B2(A), .A(Cin), .ZN(n2) );
  XNOR2_X2 U3 ( .A(Cin), .B(n1), .ZN(S) );
  XNOR2_X2 U4 ( .A(B), .B(A), .ZN(n1) );
  INV_X4 U5 ( .A(A), .ZN(n4) );
  OAI21_X2 U6 ( .B1(n4), .B2(n3), .A(n2), .ZN(Cout) );
endmodule


module FA_DESIGN0_5 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n4, n5, n6;

  INV_X4 U4 ( .A(n5), .ZN(Cout) );
  XNOR2_X2 U1 ( .A(Cin), .B(n4), .ZN(S) );
  AOI22_X1 U2 ( .A1(B), .A2(A), .B1(n6), .B2(Cin), .ZN(n5) );
  INV_X4 U3 ( .A(A), .ZN(n1) );
  XOR2_X2 U5 ( .A(n1), .B(B), .Z(n4) );
  INV_X4 U6 ( .A(n4), .ZN(n6) );
endmodule


module Cpr_DESIGN1_DEPTH4_2 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_5 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), .B(
        A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_4 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), .B(
        \ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_6 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   net712, net952, net913, net811, n1, n2, n3, n4, n5;
  assign Cout = net712;

  NAND2_X4 U1 ( .A1(net913), .A2(Cin), .ZN(net952) );
  INV_X4 U2 ( .A(Cin), .ZN(net811) );
  INV_X2 U3 ( .A(A), .ZN(n1) );
  AOI22_X2 U4 ( .A1(net811), .A2(n4), .B1(n1), .B2(n2), .ZN(net712) );
  NAND3_X2 U5 ( .A1(n5), .A2(net811), .A3(n4), .ZN(n3) );
  NAND2_X4 U6 ( .A1(net952), .A2(n3), .ZN(S) );
  NAND2_X4 U7 ( .A1(n5), .A2(n4), .ZN(net913) );
  NAND2_X4 U8 ( .A1(n2), .A2(n1), .ZN(n5) );
  INV_X4 U9 ( .A(B), .ZN(n2) );
  NAND2_X4 U10 ( .A1(B), .A2(A), .ZN(n4) );
endmodule


module FA_DESIGN0_7 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   net998, net910, net886, net818, net817, n1, n2, n3, n4;

  NAND2_X4 U1 ( .A1(n2), .A2(n1), .ZN(n4) );
  NAND2_X4 U2 ( .A1(n3), .A2(n4), .ZN(net910) );
  NAND2_X4 U3 ( .A1(A), .A2(net998), .ZN(n3) );
  INV_X4 U4 ( .A(B), .ZN(n2) );
  INV_X4 U5 ( .A(A), .ZN(n1) );
  OAI21_X1 U6 ( .B1(net886), .B2(A), .A(Cin), .ZN(net818) );
  BUF_X8 U7 ( .A(B), .Z(net886) );
  XNOR2_X2 U8 ( .A(net910), .B(Cin), .ZN(S) );
  INV_X4 U9 ( .A(net817), .ZN(net998) );
  INV_X1 U10 ( .A(net886), .ZN(net817) );
  OAI21_X2 U11 ( .B1(n1), .B2(net817), .A(net818), .ZN(Cout) );
endmodule


module Cpr_DESIGN1_DEPTH4_3 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_7 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), .B(
        A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_6 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), .B(
        \ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_8 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;

  NAND2_X4 U1 ( .A1(n1), .A2(n2), .ZN(n4) );
  INV_X4 U2 ( .A(Cin), .ZN(n6) );
  NAND2_X1 U3 ( .A1(B), .A2(A), .ZN(n3) );
  NAND2_X4 U4 ( .A1(n3), .A2(n4), .ZN(n9) );
  INV_X2 U5 ( .A(B), .ZN(n1) );
  INV_X1 U6 ( .A(A), .ZN(n2) );
  NOR2_X2 U7 ( .A1(B), .A2(A), .ZN(n10) );
  NAND2_X2 U8 ( .A1(n9), .A2(Cin), .ZN(n7) );
  NAND2_X4 U9 ( .A1(n5), .A2(n6), .ZN(n8) );
  NAND2_X4 U10 ( .A1(n8), .A2(n7), .ZN(S) );
  INV_X4 U11 ( .A(n9), .ZN(n5) );
  NAND2_X1 U12 ( .A1(A), .A2(B), .ZN(n11) );
  AOI21_X4 U13 ( .B1(n11), .B2(n6), .A(n10), .ZN(Cout) );
endmodule


module FA_DESIGN0_9 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;

  INV_X2 U1 ( .A(n7), .ZN(n1) );
  INV_X4 U2 ( .A(B), .ZN(n6) );
  INV_X4 U3 ( .A(A), .ZN(n3) );
  NAND2_X2 U4 ( .A1(Cin), .A2(B), .ZN(n8) );
  INV_X4 U5 ( .A(Cin), .ZN(n7) );
  INV_X8 U6 ( .A(n10), .ZN(n2) );
  OAI21_X2 U7 ( .B1(n3), .B2(n6), .A(n11), .ZN(Cout) );
  NAND2_X2 U8 ( .A1(n10), .A2(A), .ZN(n4) );
  NAND2_X4 U9 ( .A1(n2), .A2(n3), .ZN(n5) );
  NAND2_X4 U10 ( .A1(n5), .A2(n4), .ZN(S) );
  NAND2_X4 U11 ( .A1(n9), .A2(n8), .ZN(n10) );
  NAND2_X4 U12 ( .A1(n6), .A2(n7), .ZN(n9) );
  OAI21_X1 U13 ( .B1(A), .B2(B), .A(n1), .ZN(n11) );
endmodule


module Cpr_DESIGN1_DEPTH4_4 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_9 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), .B(
        A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_8 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), .B(
        \ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_10 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3;

  AOI21_X2 U1 ( .B1(B), .B2(A), .A(Cin), .ZN(n3) );
  NOR2_X2 U2 ( .A1(B), .A2(A), .ZN(n2) );
  XNOR2_X2 U3 ( .A(B), .B(A), .ZN(n1) );
  XNOR2_X1 U4 ( .A(n1), .B(Cin), .ZN(S) );
  NOR2_X4 U5 ( .A1(n3), .A2(n2), .ZN(Cout) );
endmodule


module FA_DESIGN0_11 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4, n5;

  INV_X2 U1 ( .A(n4), .ZN(n1) );
  INV_X2 U2 ( .A(B), .ZN(n4) );
  OAI21_X1 U3 ( .B1(n1), .B2(A), .A(Cin), .ZN(n3) );
  OAI21_X4 U4 ( .B1(n5), .B2(n4), .A(n3), .ZN(Cout) );
  XNOR2_X2 U5 ( .A(n2), .B(A), .ZN(S) );
  XNOR2_X2 U6 ( .A(B), .B(Cin), .ZN(n2) );
  INV_X4 U7 ( .A(A), .ZN(n5) );
endmodule


module Cpr_DESIGN1_DEPTH4_5 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_11 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), 
        .B(A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_10 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), 
        .B(\ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module FA_DESIGN0_12 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4, n5;

  BUF_X32 U1 ( .A(B), .Z(n1) );
  OAI22_X1 U2 ( .A1(n5), .A2(n4), .B1(n3), .B2(n2), .ZN(Cout) );
  INV_X1 U3 ( .A(n1), .ZN(n2) );
  XNOR2_X2 U4 ( .A(Cin), .B(n5), .ZN(S) );
  INV_X4 U5 ( .A(A), .ZN(n3) );
  XOR2_X2 U6 ( .A(B), .B(n3), .Z(n5) );
  INV_X4 U7 ( .A(Cin), .ZN(n4) );
endmodule


module FA_DESIGN0_13 ( A, B, Cin, S, Cout );
  input A, B, Cin;
  output S, Cout;
  wire   n1, n2, n3, n4, n5;

  XNOR2_X1 U1 ( .A(B), .B(A), .ZN(n1) );
  INV_X2 U2 ( .A(A), .ZN(n3) );
  XNOR2_X2 U3 ( .A(B), .B(A), .ZN(n5) );
  INV_X2 U4 ( .A(B), .ZN(n2) );
  XNOR2_X2 U5 ( .A(n5), .B(Cin), .ZN(S) );
  INV_X4 U6 ( .A(Cin), .ZN(n4) );
  OAI22_X2 U7 ( .A1(n1), .A2(n4), .B1(n3), .B2(n2), .ZN(Cout) );
endmodule


module Cpr_DESIGN1_DEPTH4_6 ( A, CI, S, C, CO );
  input [3:0] A;
  input [0:0] CI;
  output [0:0] CO;
  output S, C;
  wire   \ParallelFAZ.F[4] ;

  FA_DESIGN0_13 \ParallelFAZ.tree_fa_cpr[0].Cpr_FA_Behavioral2  ( .A(A[0]), 
        .B(A[1]), .Cin(A[2]), .S(\ParallelFAZ.F[4] ), .Cout(CO[0]) );
  FA_DESIGN0_12 \ParallelFAZ.tree_fa_cpr[1].Cpr_FA_Behavioral2  ( .A(A[3]), 
        .B(\ParallelFAZ.F[4] ), .Cin(CI[0]), .S(S), .Cout(C) );
endmodule


module AddMopCSV_DESIGN0_WIDTH8_DEPTH4 ( A, S, C );
  input [31:0] A;
  output [7:0] S;
  output [7:0] C;

  wire   [8:1] \StructuralZ.CI ;
  assign C[0] = 1'b0;

  Cpr_DESIGN1_DEPTH4_7 \StructuralZ.bits[0].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[24], A[16], A[8], A[0]}), .CI(1'b0), .S(S[0]), .C(C[1]), .CO(
        \StructuralZ.CI [1]) );
  Cpr_DESIGN1_DEPTH4_6 \StructuralZ.bits[1].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[25], A[17], A[9], A[1]}), .CI(\StructuralZ.CI [1]), .S(S[1]), .C(
        C[2]), .CO(\StructuralZ.CI [2]) );
  Cpr_DESIGN1_DEPTH4_5 \StructuralZ.bits[2].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[26], A[18], A[10], A[2]}), .CI(\StructuralZ.CI [2]), .S(S[2]), .C(
        C[3]), .CO(\StructuralZ.CI [3]) );
  Cpr_DESIGN1_DEPTH4_4 \StructuralZ.bits[3].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[27], A[19], A[11], A[3]}), .CI(\StructuralZ.CI [3]), .S(S[3]), .C(
        C[4]), .CO(\StructuralZ.CI [4]) );
  Cpr_DESIGN1_DEPTH4_3 \StructuralZ.bits[4].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[28], A[20], A[12], A[4]}), .CI(\StructuralZ.CI [4]), .S(S[4]), .C(
        C[5]), .CO(\StructuralZ.CI [5]) );
  Cpr_DESIGN1_DEPTH4_2 \StructuralZ.bits[5].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[29], A[21], A[13], A[5]}), .CI(\StructuralZ.CI [5]), .S(S[5]), .C(
        C[6]), .CO(\StructuralZ.CI [6]) );
  Cpr_DESIGN1_DEPTH4_1 \StructuralZ.bits[6].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[30], A[22], A[14], A[6]}), .CI(\StructuralZ.CI [6]), .S(S[6]), .C(
        C[7]), .CO(\StructuralZ.CI [7]) );
  Cpr_DESIGN1_DEPTH4_0 \StructuralZ.bits[7].AddMopCSV_Cpr_ParallelFAZ0  ( .A({
        A[31], A[23], A[15], A[7]}), .CI(\StructuralZ.CI [7]), .S(S[7]) );
endmodule


module Add_DESIGN6_WIDTH8 ( A, B, Z );
  input [7:0] A;
  input [7:0] B;
  output [7:0] Z;
  wire   net732, net735, net736, net737, net739, net741, net742, net743,
         net745, net746, net748, net753, net754, net757, net759, net760,
         net775, net778, net779, net872, net969, net776, net769, net738,
         net916, net871, net764, net762, net755, n1, n2, n3, n4, n5, n6, n7,
         n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
         n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
         n36, n37, n38, n39, n40, n41;

  INV_X2 U1 ( .A(A[2]), .ZN(n25) );
  INV_X4 U2 ( .A(A[3]), .ZN(net769) );
  NOR2_X4 U3 ( .A1(n1), .A2(n2), .ZN(n3) );
  NOR2_X4 U4 ( .A1(n3), .A2(net754), .ZN(n34) );
  INV_X2 U5 ( .A(net775), .ZN(n1) );
  INV_X4 U6 ( .A(net739), .ZN(n2) );
  NAND2_X2 U7 ( .A1(n13), .A2(n14), .ZN(net743) );
  INV_X1 U8 ( .A(n25), .ZN(n4) );
  XOR2_X2 U9 ( .A(net769), .B(B[3]), .Z(n18) );
  INV_X8 U10 ( .A(A[5]), .ZN(n8) );
  INV_X4 U11 ( .A(n8), .ZN(n5) );
  XNOR2_X2 U12 ( .A(n5), .B(n6), .ZN(net872) );
  INV_X2 U13 ( .A(n9), .ZN(n6) );
  NAND2_X4 U14 ( .A1(n30), .A2(n31), .ZN(n29) );
  INV_X1 U15 ( .A(n30), .ZN(n32) );
  NAND2_X4 U16 ( .A1(n25), .A2(n24), .ZN(n30) );
  NOR2_X4 U17 ( .A1(n8), .A2(n9), .ZN(net916) );
  XNOR2_X2 U18 ( .A(net762), .B(net871), .ZN(Z[6]) );
  AND2_X2 U19 ( .A1(n10), .A2(n7), .ZN(net871) );
  AOI211_X4 U20 ( .C1(n11), .C2(net764), .A(net916), .B(n12), .ZN(net762) );
  AOI211_X4 U21 ( .C1(n8), .C2(n9), .A(n14), .B(n13), .ZN(n12) );
  INV_X8 U22 ( .A(A[4]), .ZN(n13) );
  AOI22_X2 U23 ( .A1(n8), .A2(n9), .B1(n13), .B2(n14), .ZN(n11) );
  INV_X4 U24 ( .A(B[4]), .ZN(n14) );
  INV_X4 U25 ( .A(B[5]), .ZN(n9) );
  NAND2_X2 U26 ( .A1(n8), .A2(n9), .ZN(net742) );
  NAND2_X2 U27 ( .A1(n17), .A2(net738), .ZN(net764) );
  NAND2_X2 U28 ( .A1(A[3]), .A2(B[3]), .ZN(n17) );
  NAND2_X2 U29 ( .A1(B[6]), .A2(A[6]), .ZN(n10) );
  OAI21_X1 U30 ( .B1(net746), .B2(n7), .A(n10), .ZN(net759) );
  INV_X4 U31 ( .A(n10), .ZN(net760) );
  NAND2_X2 U32 ( .A1(n10), .A2(net757), .ZN(net755) );
  NAND2_X2 U33 ( .A1(n15), .A2(n16), .ZN(n7) );
  NAND2_X2 U34 ( .A1(net746), .A2(n7), .ZN(net745) );
  INV_X4 U35 ( .A(B[6]), .ZN(n16) );
  INV_X4 U36 ( .A(A[6]), .ZN(n15) );
  NOR2_X1 U37 ( .A1(net755), .A2(net916), .ZN(net748) );
  NAND2_X2 U38 ( .A1(net735), .A2(net916), .ZN(net732) );
  AOI21_X1 U39 ( .B1(A[4]), .B2(B[4]), .A(net776), .ZN(net775) );
  NAND2_X1 U40 ( .A1(B[4]), .A2(A[4]), .ZN(net737) );
  OAI21_X1 U41 ( .B1(B[3]), .B2(A[3]), .A(net779), .ZN(net738) );
  NAND3_X2 U42 ( .A1(net738), .A2(net737), .A3(net739), .ZN(net736) );
  NAND2_X2 U43 ( .A1(net739), .A2(net738), .ZN(net778) );
  INV_X4 U44 ( .A(net738), .ZN(net776) );
  NAND2_X4 U45 ( .A1(A[3]), .A2(B[3]), .ZN(net739) );
  XNOR2_X2 U46 ( .A(n18), .B(net779), .ZN(Z[3]) );
  NAND2_X1 U47 ( .A1(net737), .A2(net743), .ZN(n19) );
  INV_X1 U48 ( .A(net741), .ZN(net969) );
  XOR2_X2 U49 ( .A(n19), .B(net753), .Z(Z[4]) );
  XNOR2_X2 U50 ( .A(n29), .B(n28), .ZN(Z[2]) );
  NAND2_X1 U51 ( .A1(B[1]), .A2(A[1]), .ZN(n26) );
  XNOR2_X2 U52 ( .A(n20), .B(A[1]), .ZN(Z[1]) );
  XNOR2_X2 U53 ( .A(B[1]), .B(n21), .ZN(n20) );
  OAI21_X1 U54 ( .B1(A[1]), .B2(B[1]), .A(n21), .ZN(n27) );
  AND2_X2 U55 ( .A1(B[0]), .A2(A[0]), .ZN(n21) );
  NAND3_X2 U56 ( .A1(n38), .A2(net735), .A3(net736), .ZN(n39) );
  INV_X4 U57 ( .A(net743), .ZN(net754) );
  XNOR2_X2 U58 ( .A(net872), .B(n34), .ZN(Z[5]) );
  OAI21_X1 U59 ( .B1(net753), .B2(net754), .A(net737), .ZN(n36) );
  INV_X4 U60 ( .A(A[0]), .ZN(n23) );
  INV_X4 U61 ( .A(B[0]), .ZN(n22) );
  AOI21_X2 U62 ( .B1(n23), .B2(n22), .A(n21), .ZN(Z[0]) );
  INV_X4 U63 ( .A(B[2]), .ZN(n24) );
  NAND2_X2 U64 ( .A1(n4), .A2(B[2]), .ZN(n31) );
  NAND2_X2 U65 ( .A1(n27), .A2(n26), .ZN(n28) );
  INV_X4 U66 ( .A(n28), .ZN(n33) );
  OAI21_X2 U67 ( .B1(n33), .B2(n32), .A(n31), .ZN(net779) );
  INV_X4 U68 ( .A(net778), .ZN(net753) );
  XNOR2_X2 U69 ( .A(B[7]), .B(A[7]), .ZN(net746) );
  INV_X4 U70 ( .A(net746), .ZN(net757) );
  NAND2_X2 U71 ( .A1(net760), .A2(net757), .ZN(n35) );
  NAND2_X2 U72 ( .A1(n35), .A2(net759), .ZN(n41) );
  NAND2_X2 U73 ( .A1(n36), .A2(net969), .ZN(n37) );
  NAND2_X2 U74 ( .A1(net748), .A2(n37), .ZN(n40) );
  INV_X4 U75 ( .A(net745), .ZN(net735) );
  INV_X4 U76 ( .A(net742), .ZN(net741) );
  NOR2_X4 U77 ( .A1(net741), .A2(net754), .ZN(n38) );
  NAND4_X2 U78 ( .A1(n39), .A2(n40), .A3(net732), .A4(n41), .ZN(Z[7]) );
endmodule


module Mult_DESIGN1_WIDTH4_ZWIDTH8_WIDTHP8 ( A, B, Z );
  input [3:0] A;
  input [3:0] B;
  output [7:0] Z;
  wire   \Structural.PPS_21 , \Structural.PPS_20 , \Structural.PPS_19 ,
         \Structural.PPS_18 , \Structural.PPS_12 , \Structural.PPS_11 ,
         \Structural.PPS_10 , \Structural.PPS_9 , \Structural.PPS_3 ,
         \Structural.PPS_2 , \Structural.PPS_1 , \Structural.PPS_0 , net858,
         net859, net860, net861, net864, net865, net1365, net1655, net1686,
         net1679, n1, n2, n3;
  wire   [30:27] \Structural.PPS ;
  wire   [7:0] \Structural.ST ;
  wire   [7:0] \Structural.CT ;

  AddMopCSV_DESIGN0_WIDTH8_DEPTH4 \Structural.Mult_AddMopCSV_StructuralZ0  ( 
        .A({1'b0, \Structural.PPS , 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        \Structural.PPS_21 , \Structural.PPS_20 , \Structural.PPS_19 , 
        \Structural.PPS_18 , 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, \Structural.PPS_12 , 
        \Structural.PPS_11 , \Structural.PPS_10 , \Structural.PPS_9 , 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, \Structural.PPS_3 , \Structural.PPS_2 , 
        \Structural.PPS_1 , \Structural.PPS_0 }), .S(\Structural.ST ), .C(
        \Structural.CT ) );
  Add_DESIGN6_WIDTH8 \Structural.Mult_Add_LF1  ( .A(\Structural.ST ), .B({
        \Structural.CT [7:1], 1'b0}), .Z(Z) );
  NOR2_X4 U2 ( .A1(n1), .A2(net858), .ZN(\Structural.PPS_9 ) );
  NOR2_X4 U3 ( .A1(net858), .A2(net865), .ZN(\Structural.PPS [27]) );
  INV_X4 U4 ( .A(net1679), .ZN(net1365) );
  BUF_X8 U5 ( .A(n2), .Z(n1) );
  NOR2_X2 U6 ( .A1(n3), .A2(net864), .ZN(\Structural.PPS_21 ) );
  NOR2_X4 U7 ( .A1(net859), .A2(net1655), .ZN(\Structural.PPS_2 ) );
  NOR2_X4 U8 ( .A1(n2), .A2(net861), .ZN(\Structural.PPS_11 ) );
  NOR2_X4 U9 ( .A1(n2), .A2(n3), .ZN(\Structural.PPS_12 ) );
  INV_X4 U10 ( .A(B[1]), .ZN(n2) );
  NOR2_X2 U11 ( .A1(net860), .A2(n2), .ZN(\Structural.PPS_10 ) );
  INV_X4 U12 ( .A(A[3]), .ZN(n3) );
  INV_X2 U13 ( .A(n3), .ZN(net1679) );
  INV_X2 U14 ( .A(net1686), .ZN(net1655) );
  INV_X2 U15 ( .A(net861), .ZN(net1686) );
  NOR2_X4 U16 ( .A1(net859), .A2(net1365), .ZN(\Structural.PPS_3 ) );
  NOR2_X2 U17 ( .A1(net865), .A2(net1365), .ZN(\Structural.PPS [30]) );
  NOR2_X1 U18 ( .A1(net860), .A2(net865), .ZN(\Structural.PPS [28]) );
  NOR2_X4 U19 ( .A1(net860), .A2(net864), .ZN(\Structural.PPS_19 ) );
  INV_X4 U21 ( .A(B[2]), .ZN(net864) );
  INV_X4 U22 ( .A(B[3]), .ZN(net865) );
  INV_X4 U23 ( .A(A[2]), .ZN(net861) );
  NOR2_X2 U24 ( .A1(net1655), .A2(net865), .ZN(\Structural.PPS [29]) );
  INV_X4 U25 ( .A(A[1]), .ZN(net860) );
  INV_X4 U26 ( .A(A[0]), .ZN(net858) );
  NOR2_X2 U27 ( .A1(net861), .A2(net864), .ZN(\Structural.PPS_20 ) );
  NOR2_X2 U28 ( .A1(net864), .A2(net858), .ZN(\Structural.PPS_18 ) );
  INV_X4 U29 ( .A(B[0]), .ZN(net859) );
  NOR2_X2 U30 ( .A1(net859), .A2(net860), .ZN(\Structural.PPS_1 ) );
  NOR2_X2 U31 ( .A1(net858), .A2(net859), .ZN(\Structural.PPS_0 ) );
endmodule


module Mult_syn ( A, B, Z );
  input [3:0] A;
  input [3:0] B;
  output [7:0] Z;


  Mult_DESIGN1_WIDTH4_ZWIDTH8_WIDTHP8 Mult_Mult_struct0 ( .A(A), .B(B), .Z(Z)
         );
endmodule

