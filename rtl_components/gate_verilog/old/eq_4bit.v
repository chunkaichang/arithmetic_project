
module Eq_DESIGN2_WIDTH4 ( A, B, Z );
  input [3:0] A;
  input [3:0] B;
  output Z;
  wire   net145, net144, net143, n1, n2, n3, n4, n5, n6, n7;

  NOR3_X4 U1 ( .A1(net143), .A2(net144), .A3(net145), .ZN(Z) );
  XNOR2_X2 U2 ( .A(n7), .B(A[2]), .ZN(net145) );
  INV_X2 U3 ( .A(B[2]), .ZN(n7) );
  NAND2_X2 U4 ( .A1(n5), .A2(n6), .ZN(net144) );
  NAND2_X2 U5 ( .A1(n3), .A2(A[3]), .ZN(n6) );
  INV_X4 U6 ( .A(B[3]), .ZN(n3) );
  NAND2_X2 U7 ( .A1(B[3]), .A2(n4), .ZN(n5) );
  INV_X4 U8 ( .A(A[3]), .ZN(n4) );
  NAND2_X2 U9 ( .A1(n1), .A2(n2), .ZN(net143) );
  XNOR2_X2 U10 ( .A(B[1]), .B(A[1]), .ZN(n2) );
  XNOR2_X2 U11 ( .A(B[0]), .B(A[0]), .ZN(n1) );
endmodule


module Eq_syn ( A, B, Z );
  input [3:0] A;
  input [3:0] B;
  output Z;


  Eq_DESIGN2_WIDTH4 Eq_Eq_b20 ( .A(A), .B(B), .Z(Z) );
endmodule

