
module DW_fp_addsub_inst_DW_lzd_1 ( a, enc, dec );
  input [27:0] a;
  output [5:0] enc;
  output [27:0] dec;
  wire   \U1/or2_inv[0][6] , \U1/or2_inv[0][10] , \U1/or2_inv[0][12] ,
         \U1/or2_inv[0][14] , \U1/or2_inv[0][18] , \U1/or2_inv[0][20] ,
         \U1/or2_inv[0][22] , \U1/or2_inv[0][24] , \U1/or2_inv[0][26] ,
         \U1/or2_inv[0][28] , \U1/or2_inv[1][20] , \U1/or2_inv[1][24] ,
         \U1/or2_inv[2][8] , \U1/or2_inv[2][24] , \U1/or2_tree[1][2][16] ,
         \U1/or2_tree[1][2][24] , \U1/or2_tree[0][1][8] ,
         \U1/or2_tree[0][1][12] , \U1/or2_tree[0][1][16] ,
         \U1/or2_tree[0][1][20] , \U1/or2_tree[0][1][24] ,
         \U1/or2_tree[0][1][28] , \U1/or2_tree[0][2][16] ,
         \U1/or2_tree[0][2][24] , \U1/enc_tree[0][3][8] ,
         \U1/enc_tree[0][3][24] , \U1/enc_tree[0][4][16] ,
         \U1/enc_tree[1][2][4] , \U1/enc_tree[1][2][12] ,
         \U1/enc_tree[1][2][20] , \U1/enc_tree[1][2][28] ,
         \U1/enc_tree[1][3][8] , \U1/enc_tree[1][3][24] ,
         \U1/enc_tree[1][4][16] , \U1/enc_tree[2][3][8] ,
         \U1/enc_tree[2][3][24] , \U1/enc_tree[2][4][16] ,
         \U1/enc_tree[3][4][16] , \U1/enc_tree[0][1][6] ,
         \U1/enc_tree[0][1][10] , \U1/enc_tree[0][1][14] ,
         \U1/enc_tree[0][1][18] , \U1/enc_tree[0][1][22] ,
         \U1/enc_tree[0][1][26] , \U1/enc_tree[0][1][30] ,
         \U1/enc_tree[0][2][4] , \U1/enc_tree[0][2][12] ,
         \U1/enc_tree[0][2][20] , \U1/enc_tree[0][2][28] , \U1/or_tree[1][4] ,
         \U1/or_tree[1][6] , \U1/or_tree[1][8] , \U1/or_tree[1][10] ,
         \U1/or_tree[1][12] , \U1/or_tree[1][14] , \U1/or_tree[1][16] ,
         \U1/or_tree[1][18] , \U1/or_tree[1][20] , \U1/or_tree[1][22] ,
         \U1/or_tree[1][24] , \U1/or_tree[1][26] , \U1/or_tree[1][28] ,
         \U1/or_tree[1][30] , \U1/or_tree[2][4] , \U1/or_tree[2][8] ,
         \U1/or_tree[2][12] , \U1/or_tree[2][16] , \U1/or_tree[2][20] ,
         \U1/or_tree[2][24] , \U1/or_tree[2][28] , \U1/or_tree[3][0] ,
         \U1/or_tree[3][8] , \U1/or_tree[3][16] , \U1/or_tree[3][24] ,
         \U1/or_tree[4][0] , \U1/or_tree[4][16] , n1, n2, n3, n4, n5, n6, n7,
         n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
         n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34;

  NAND2_X1 \U1/UORT1_2_1  ( .A1(\U1/or_tree[1][4] ), .A2(\U1/or_tree[1][6] ), 
        .ZN(\U1/or_tree[2][4] ) );
  NAND2_X1 \U1/UORT1_2_2  ( .A1(\U1/or_tree[1][8] ), .A2(\U1/or_tree[1][10] ), 
        .ZN(\U1/or_tree[2][8] ) );
  NAND2_X1 \U1/UORT1_2_3  ( .A1(\U1/or_tree[1][12] ), .A2(\U1/or_tree[1][14] ), 
        .ZN(\U1/or_tree[2][12] ) );
  NAND2_X1 \U1/UORT1_2_4  ( .A1(\U1/or_tree[1][18] ), .A2(\U1/or_tree[1][16] ), 
        .ZN(\U1/or_tree[2][16] ) );
  NAND2_X1 \U1/UORT1_2_5  ( .A1(\U1/or_tree[1][20] ), .A2(\U1/or_tree[1][22] ), 
        .ZN(\U1/or_tree[2][20] ) );
  NAND2_X1 \U1/UORT1_2_6  ( .A1(\U1/or_tree[1][24] ), .A2(\U1/or_tree[1][26] ), 
        .ZN(\U1/or_tree[2][24] ) );
  NAND2_X1 \U1/UORT1_2_7  ( .A1(\U1/or_tree[1][28] ), .A2(\U1/or_tree[1][30] ), 
        .ZN(\U1/or_tree[2][28] ) );
  NAND2_X1 \U1/UORT1_4_0  ( .A1(\U1/or_tree[3][8] ), .A2(\U1/or_tree[3][0] ), 
        .ZN(\U1/or_tree[4][0] ) );
  NAND2_X1 \U1/UORT1_4_1  ( .A1(\U1/or_tree[3][16] ), .A2(\U1/or_tree[3][24] ), 
        .ZN(\U1/or_tree[4][16] ) );
  NAND2_X1 \U1/UOR21_0_2_2  ( .A1(\U1/or2_tree[0][1][16] ), .A2(
        \U1/or2_tree[0][1][20] ), .ZN(\U1/or2_tree[0][2][16] ) );
  NAND2_X1 \U1/UOR21_0_2_3  ( .A1(\U1/or2_tree[0][1][24] ), .A2(
        \U1/or2_tree[0][1][28] ), .ZN(\U1/or2_tree[0][2][24] ) );
  NAND2_X1 \U1/UOR21_1_2_2  ( .A1(n18), .A2(n33), .ZN(\U1/or2_tree[1][2][16] )
         );
  NAND2_X1 \U1/UOR21_1_2_3  ( .A1(\U1/or_tree[1][24] ), .A2(
        \U1/or_tree[1][28] ), .ZN(\U1/or2_tree[1][2][24] ) );
  NOR2_X1 \U1/UORT0_1_11  ( .A1(a[18]), .A2(a[19]), .ZN(\U1/or_tree[1][22] )
         );
  NOR2_X1 \U1/UORT0_5_0  ( .A1(\U1/or_tree[4][0] ), .A2(\U1/or_tree[4][16] ), 
        .ZN(enc[5]) );
  NOR2_X1 \U1/UOR20_0_1_5  ( .A1(n8), .A2(n27), .ZN(\U1/or2_tree[0][1][20] )
         );
  NOR2_X1 \U1/UOR20_0_1_2  ( .A1(a[4]), .A2(n15), .ZN(\U1/or2_tree[0][1][8] )
         );
  NOR2_X1 \U1/UORT0_1_5  ( .A1(a[7]), .A2(a[6]), .ZN(\U1/or_tree[1][10] ) );
  NOR2_X1 \U1/UOR20_0_1_6  ( .A1(a[20]), .A2(a[22]), .ZN(
        \U1/or2_tree[0][1][24] ) );
  NOR2_X1 \U1/UORT0_1_13  ( .A1(a[22]), .A2(a[23]), .ZN(\U1/or_tree[1][26] )
         );
  NOR2_X1 \U1/UOR20_0_1_4  ( .A1(n17), .A2(a[14]), .ZN(\U1/or2_tree[0][1][16] ) );
  NOR2_X1 \U1/UORT0_1_9  ( .A1(a[14]), .A2(a[15]), .ZN(\U1/or_tree[1][18] ) );
  AOI21_X1 \U1/UEN0_0_1_5  ( .B1(\U1/or2_inv[0][22] ), .B2(n14), .A(a[19]), 
        .ZN(\U1/enc_tree[0][1][22] ) );
  OAI21_X1 \U1/UEN1_1_2_2  ( .B1(n23), .B2(\U1/or2_inv[1][20] ), .A(n26), .ZN(
        \U1/enc_tree[1][2][20] ) );
  AOI21_X1 \U1/UEN0_0_1_3  ( .B1(\U1/or2_inv[0][14] ), .B2(n13), .A(n7), .ZN(
        \U1/enc_tree[0][1][14] ) );
  NOR2_X1 \U1/UOR20_0_1_3  ( .A1(a[10]), .A2(a[8]), .ZN(
        \U1/or2_tree[0][1][12] ) );
  NOR2_X1 \U1/UORT0_1_7  ( .A1(a[10]), .A2(a[11]), .ZN(\U1/or_tree[1][14] ) );
  AOI21_X1 \U1/UEN0_0_1_2  ( .B1(\U1/or2_inv[0][10] ), .B2(n10), .A(n30), .ZN(
        \U1/enc_tree[0][1][10] ) );
  NOR2_X1 \U1/UORT0_1_4  ( .A1(a[5]), .A2(a[4]), .ZN(\U1/or_tree[1][8] ) );
  AOI21_X1 \U1/UEN0_0_1_4  ( .B1(\U1/or2_inv[0][18] ), .B2(n11), .A(n16), .ZN(
        \U1/enc_tree[0][1][18] ) );
  NOR2_X1 \U1/UORT0_1_8  ( .A1(a[12]), .A2(a[13]), .ZN(\U1/or_tree[1][16] ) );
  AOI21_X1 \U1/UEN0_0_1_6  ( .B1(a[21]), .B2(\U1/or2_inv[0][26] ), .A(n12), 
        .ZN(\U1/enc_tree[0][1][26] ) );
  NOR2_X1 \U1/UOR20_0_1_7  ( .A1(a[24]), .A2(a[26]), .ZN(
        \U1/or2_tree[0][1][28] ) );
  NOR2_X1 \U1/UORT0_1_6  ( .A1(a[8]), .A2(a[9]), .ZN(\U1/or_tree[1][12] ) );
  NOR2_X1 \U1/UORT0_1_14  ( .A1(a[24]), .A2(a[25]), .ZN(\U1/or_tree[1][28] )
         );
  OAI21_X1 \U1/UEN1_0_2_1  ( .B1(\U1/enc_tree[0][1][10] ), .B2(
        \U1/or2_inv[0][12] ), .A(\U1/enc_tree[0][1][14] ), .ZN(
        \U1/enc_tree[0][2][12] ) );
  AOI21_X1 \U1/UEN0_0_3_0  ( .B1(\U1/enc_tree[0][2][4] ), .B2(n2), .A(
        \U1/enc_tree[0][2][12] ), .ZN(\U1/enc_tree[0][3][8] ) );
  NOR2_X1 \U1/UORT0_1_3  ( .A1(a[2]), .A2(a[3]), .ZN(\U1/or_tree[1][6] ) );
  OAI21_X1 \U1/UEN1_1_2_3  ( .B1(n9), .B2(n31), .A(\U1/or_tree[1][30] ), .ZN(
        \U1/enc_tree[1][2][28] ) );
  AOI21_X1 \U1/UEN0_1_3_1  ( .B1(\U1/enc_tree[1][2][20] ), .B2(
        \U1/or2_inv[1][24] ), .A(\U1/enc_tree[1][2][28] ), .ZN(
        \U1/enc_tree[1][3][24] ) );
  OAI21_X1 \U1/UEN1_0_2_3  ( .B1(\U1/enc_tree[0][1][26] ), .B2(
        \U1/or2_inv[0][28] ), .A(\U1/enc_tree[0][1][30] ), .ZN(
        \U1/enc_tree[0][2][28] ) );
  OAI21_X1 \U1/UEN1_0_2_2  ( .B1(\U1/enc_tree[0][1][18] ), .B2(
        \U1/or2_inv[0][20] ), .A(\U1/enc_tree[0][1][22] ), .ZN(
        \U1/enc_tree[0][2][20] ) );
  AOI21_X1 \U1/UEN0_0_3_1  ( .B1(\U1/enc_tree[0][2][20] ), .B2(
        \U1/or2_inv[0][24] ), .A(\U1/enc_tree[0][2][28] ), .ZN(
        \U1/enc_tree[0][3][24] ) );
  AOI21_X1 \U1/UEN0_0_1_1  ( .B1(a[1]), .B2(\U1/or2_inv[0][6] ), .A(a[3]), 
        .ZN(\U1/enc_tree[0][1][6] ) );
  NOR2_X1 \U1/UORT0_1_2  ( .A1(a[0]), .A2(a[1]), .ZN(\U1/or_tree[1][4] ) );
  NOR2_X1 \U1/UORT0_1_12  ( .A1(a[20]), .A2(a[21]), .ZN(\U1/or_tree[1][24] )
         );
  OAI21_X1 \U1/UEN1_0_4_0  ( .B1(\U1/enc_tree[0][3][8] ), .B2(n5), .A(
        \U1/enc_tree[0][3][24] ), .ZN(\U1/enc_tree[0][4][16] ) );
  OAI21_X1 \U1/UEN1_1_4_0  ( .B1(\U1/enc_tree[1][3][8] ), .B2(n4), .A(
        \U1/enc_tree[1][3][24] ), .ZN(\U1/enc_tree[1][4][16] ) );
  OAI21_X1 \U1/UEN1_1_2_1  ( .B1(\U1/or_tree[1][10] ), .B2(n34), .A(n20), .ZN(
        \U1/enc_tree[1][2][12] ) );
  AOI21_X1 \U1/UEN0_1_3_0  ( .B1(\U1/enc_tree[1][2][4] ), .B2(n3), .A(
        \U1/enc_tree[1][2][12] ), .ZN(\U1/enc_tree[1][3][8] ) );
  NOR2_X1 \U1/UORT0_3_1  ( .A1(\U1/or_tree[2][8] ), .A2(\U1/or_tree[2][12] ), 
        .ZN(\U1/or_tree[3][8] ) );
  AOI21_X1 \U1/UEN0_2_3_0  ( .B1(\U1/or_tree[2][4] ), .B2(\U1/or2_inv[2][8] ), 
        .A(n21), .ZN(\U1/enc_tree[2][3][8] ) );
  AOI21_X1 \U1/UEN0_2_3_1  ( .B1(n19), .B2(\U1/or2_inv[2][24] ), .A(
        \U1/or_tree[2][28] ), .ZN(\U1/enc_tree[2][3][24] ) );
  NOR2_X1 \U1/UORT0_3_3  ( .A1(\U1/or_tree[2][24] ), .A2(\U1/or_tree[2][28] ), 
        .ZN(\U1/or_tree[3][24] ) );
  OAI21_X1 \U1/UEN1_3_4_0  ( .B1(n25), .B2(n32), .A(n28), .ZN(
        \U1/enc_tree[3][4][16] ) );
  OAI21_X1 \U1/UEN1_2_4_0  ( .B1(\U1/enc_tree[2][3][8] ), .B2(n6), .A(
        \U1/enc_tree[2][3][24] ), .ZN(\U1/enc_tree[2][4][16] ) );
  NOR2_X1 \U1/UORT0_3_2  ( .A1(\U1/or_tree[2][16] ), .A2(\U1/or_tree[2][20] ), 
        .ZN(\U1/or_tree[3][16] ) );
  NOR2_X1 \U1/UORT0_1_10  ( .A1(a[16]), .A2(a[17]), .ZN(\U1/or_tree[1][20] )
         );
  INV_X2 U1 ( .A(\U1/enc_tree[2][4][16] ), .ZN(enc[2]) );
  CLKBUF_X1 U2 ( .A(a[25]), .Z(n1) );
  INV_X2 U3 ( .A(\U1/enc_tree[1][4][16] ), .ZN(enc[1]) );
  AND2_X1 U4 ( .A1(\U1/or2_tree[0][1][8] ), .A2(\U1/or2_tree[0][1][12] ), .ZN(
        n2) );
  AND2_X1 U5 ( .A1(n29), .A2(\U1/or_tree[1][12] ), .ZN(n3) );
  OR2_X1 U6 ( .A1(\U1/or2_tree[1][2][16] ), .A2(\U1/or2_tree[1][2][24] ), .ZN(
        n4) );
  OR2_X1 U7 ( .A1(\U1/or2_tree[0][2][16] ), .A2(\U1/or2_tree[0][2][24] ), .ZN(
        n5) );
  OR2_X1 U8 ( .A1(\U1/or_tree[2][16] ), .A2(n22), .ZN(n6) );
  INV_X1 U9 ( .A(\U1/enc_tree[3][4][16] ), .ZN(enc[3]) );
  CLKBUF_X1 U10 ( .A(a[11]), .Z(n7) );
  CLKBUF_X1 U11 ( .A(a[16]), .Z(n8) );
  CLKBUF_X1 U12 ( .A(\U1/or_tree[1][26] ), .Z(n9) );
  CLKBUF_X1 U13 ( .A(a[5]), .Z(n10) );
  CLKBUF_X1 U14 ( .A(a[13]), .Z(n11) );
  CLKBUF_X1 U15 ( .A(a[23]), .Z(n12) );
  CLKBUF_X1 U16 ( .A(a[9]), .Z(n13) );
  CLKBUF_X1 U17 ( .A(a[17]), .Z(n14) );
  INV_X1 U18 ( .A(\U1/or2_inv[0][10] ), .ZN(n15) );
  CLKBUF_X1 U19 ( .A(a[15]), .Z(n16) );
  CLKBUF_X1 U20 ( .A(a[12]), .Z(n17) );
  BUF_X1 U21 ( .A(\U1/or_tree[2][12] ), .Z(n21) );
  NOR2_X1 U22 ( .A1(n17), .A2(n11), .ZN(n18) );
  CLKBUF_X1 U23 ( .A(\U1/or_tree[2][20] ), .Z(n19) );
  NOR2_X1 U24 ( .A1(a[10]), .A2(n7), .ZN(n20) );
  INV_X1 U25 ( .A(\U1/or2_inv[2][24] ), .ZN(n22) );
  NOR2_X1 U26 ( .A1(n24), .A2(n16), .ZN(n23) );
  INV_X1 U27 ( .A(\U1/or2_inv[0][18] ), .ZN(n24) );
  NOR2_X1 U28 ( .A1(\U1/or_tree[2][8] ), .A2(n21), .ZN(n25) );
  CLKBUF_X1 U29 ( .A(\U1/or_tree[1][22] ), .Z(n26) );
  CLKBUF_X1 U30 ( .A(a[18]), .Z(n27) );
  NOR2_X1 U31 ( .A1(\U1/or_tree[2][24] ), .A2(\U1/or_tree[2][28] ), .ZN(n28)
         );
  NOR2_X1 U32 ( .A1(a[4]), .A2(n10), .ZN(n29) );
  CLKBUF_X1 U33 ( .A(a[7]), .Z(n30) );
  OR2_X1 U34 ( .A1(a[24]), .A2(a[25]), .ZN(n31) );
  OR2_X1 U35 ( .A1(\U1/or_tree[2][16] ), .A2(\U1/or_tree[2][20] ), .ZN(n32) );
  NOR2_X1 U36 ( .A1(n14), .A2(n8), .ZN(n33) );
  OR2_X1 U37 ( .A1(a[8]), .A2(a[9]), .ZN(n34) );
  INV_X1 U38 ( .A(\U1/or_tree[2][4] ), .ZN(\U1/or_tree[3][0] ) );
  INV_X1 U39 ( .A(\U1/or_tree[2][24] ), .ZN(\U1/or2_inv[2][24] ) );
  INV_X1 U40 ( .A(\U1/or_tree[2][8] ), .ZN(\U1/or2_inv[2][8] ) );
  INV_X1 U41 ( .A(\U1/or_tree[1][6] ), .ZN(\U1/enc_tree[1][2][4] ) );
  INV_X1 U42 ( .A(\U1/enc_tree[0][4][16] ), .ZN(enc[0]) );
  INV_X1 U43 ( .A(\U1/or2_tree[0][1][20] ), .ZN(\U1/or2_inv[0][20] ) );
  INV_X1 U44 ( .A(a[2]), .ZN(\U1/or2_inv[0][6] ) );
  INV_X1 U45 ( .A(\U1/or2_tree[0][2][24] ), .ZN(\U1/or2_inv[0][24] ) );
  INV_X1 U46 ( .A(\U1/or2_tree[1][2][24] ), .ZN(\U1/or2_inv[1][24] ) );
  INV_X1 U47 ( .A(\U1/enc_tree[0][1][6] ), .ZN(\U1/enc_tree[0][2][4] ) );
  INV_X1 U48 ( .A(a[26]), .ZN(\U1/or_tree[1][30] ) );
  INV_X1 U49 ( .A(\U1/or2_tree[0][1][12] ), .ZN(\U1/or2_inv[0][12] ) );
  INV_X1 U50 ( .A(\U1/or2_tree[0][1][28] ), .ZN(\U1/or2_inv[0][28] ) );
  NAND2_X1 U51 ( .A1(n1), .A2(\U1/or_tree[1][30] ), .ZN(
        \U1/enc_tree[0][1][30] ) );
  INV_X1 U52 ( .A(a[10]), .ZN(\U1/or2_inv[0][14] ) );
  INV_X1 U53 ( .A(n33), .ZN(\U1/or2_inv[1][20] ) );
  INV_X1 U54 ( .A(a[14]), .ZN(\U1/or2_inv[0][18] ) );
  INV_X1 U55 ( .A(a[22]), .ZN(\U1/or2_inv[0][26] ) );
  INV_X1 U56 ( .A(a[6]), .ZN(\U1/or2_inv[0][10] ) );
  INV_X1 U57 ( .A(n27), .ZN(\U1/or2_inv[0][22] ) );
  INV_X2 U58 ( .A(\U1/or_tree[4][16] ), .ZN(enc[4]) );
endmodule


module DW_fp_addsub_inst_DW01_add_4 ( A, B, CI, SUM, CO );
  input [27:0] A;
  input [27:0] B;
  output [27:0] SUM;
  input CI;
  output CO;
  wire   n1, n3, n4, n5, n7, n8, n10, n11, n12, n13, n14, n15, n16, n17, n18,
         n20, n21, n23, n24, n25, n28, n30, n32, n33, n34, n35, n37, n38, n39,
         n40, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n54, n56, n57,
         n58, n59, n63, n65, n66, n67, n68, n69, n73, n74, n75, n76, n78, n80,
         n81, n82, n83, n87, n89, n90, n91, n92, n93, n94, n95, n98, n100,
         n102, n103, n104, n105, n109, n111, n112, n115, n116, n117, n118,
         n121, n122, n123, n124, n125, n127, n128, n130, n132, n133, n134,
         n135, n136, n137, n138, n139, n140, n143, n144, n145, n146, n147,
         n148, n150, n153, n154, n155, n156, n157, n159, n160, n161, n162,
         n164, n166, n169, n170, n171, n172, n173, n174, n175, n177, n178,
         n179, n180, n182, n183, n184, n189, n190, n191, n192, n193, n194,
         n195, n196, n197, n198, n199, n200, n201, n202, n203, n204, n205,
         n208, n209, n210, n211, n213, n217, n218, n220, n221, n222, n223,
         n224, n225, n226, n227, n228, n229, n230, n231, n233, n234, n235,
         n236, n238, n240, n241, n248, n249, n250, n251, n252, n253, n254,
         n255, n256, n257, n258, n259, n261, n263, n356, n357, n358, n359,
         n360, n361, n362, n363, n364, n365, n366, n367, n368, n369, n370,
         n371, n372, n373, n374, n375, n376, n377, n378, n379, n380, n381,
         n382, n383, n384, n385, n386, n387, n388, n389, n390, n391, n392,
         n393, n394, n395, n396, n397, n398, n399, n400, n401, n402, n403,
         n404, n405, n406, n407, n408, n409, n410, n411, n412, n413, n414,
         n415, n416, n417, n418, n419, n420, n421, n423, n424, n425, n426,
         n427, n428, n429, n430;
  assign n35 = B[26];
  assign n236 = B[1];

  BUF_X1 U297 ( .A(n68), .Z(n356) );
  BUF_X1 U298 ( .A(n208), .Z(n368) );
  CLKBUF_X1 U299 ( .A(n134), .Z(n357) );
  CLKBUF_X1 U300 ( .A(n376), .Z(n365) );
  NOR2_X1 U301 ( .A1(B[25]), .A2(A[25]), .ZN(n44) );
  BUF_X1 U302 ( .A(n221), .Z(n363) );
  OR2_X1 U303 ( .A1(n192), .A2(n189), .ZN(n358) );
  OR2_X1 U304 ( .A1(B[6]), .A2(A[6]), .ZN(n359) );
  OR2_X1 U305 ( .A1(B[0]), .A2(CI), .ZN(n360) );
  OR2_X1 U306 ( .A1(n419), .A2(n74), .ZN(n392) );
  CLKBUF_X1 U307 ( .A(n95), .Z(n361) );
  CLKBUF_X1 U308 ( .A(n125), .Z(n362) );
  OR2_X2 U309 ( .A1(B[22]), .A2(A[22]), .ZN(n425) );
  NOR2_X1 U310 ( .A1(n376), .A2(n140), .ZN(n364) );
  INV_X1 U311 ( .A(n177), .ZN(n366) );
  CLKBUF_X1 U312 ( .A(n178), .Z(n367) );
  AND2_X1 U313 ( .A1(n396), .A2(n397), .ZN(n390) );
  OR2_X2 U314 ( .A1(B[19]), .A2(A[19]), .ZN(n426) );
  NOR2_X1 U315 ( .A1(B[12]), .A2(A[12]), .ZN(n369) );
  NOR2_X1 U316 ( .A1(n371), .A2(n205), .ZN(n370) );
  NOR2_X1 U317 ( .A1(B[8]), .A2(A[8]), .ZN(n371) );
  NOR2_X1 U318 ( .A1(B[6]), .A2(A[6]), .ZN(n372) );
  NOR2_X1 U319 ( .A1(B[6]), .A2(A[6]), .ZN(n373) );
  CLKBUF_X1 U320 ( .A(n148), .Z(n374) );
  CLKBUF_X1 U321 ( .A(n409), .Z(n375) );
  NOR2_X1 U322 ( .A1(B[16]), .A2(A[16]), .ZN(n376) );
  CLKBUF_X1 U323 ( .A(n371), .Z(n377) );
  XNOR2_X1 U324 ( .A(n81), .B(n378), .ZN(SUM[22]) );
  NAND2_X1 U325 ( .A1(n425), .A2(n80), .ZN(n378) );
  OR2_X1 U326 ( .A1(n363), .A2(n372), .ZN(n379) );
  INV_X1 U327 ( .A(n381), .ZN(n398) );
  OR2_X1 U328 ( .A1(B[4]), .A2(A[4]), .ZN(n380) );
  AND2_X2 U329 ( .A1(n391), .A2(n382), .ZN(n381) );
  NOR2_X1 U330 ( .A1(n192), .A2(n189), .ZN(n382) );
  NOR2_X1 U331 ( .A1(n192), .A2(n189), .ZN(n183) );
  NOR2_X1 U332 ( .A1(B[10]), .A2(A[10]), .ZN(n411) );
  NOR2_X1 U333 ( .A1(n419), .A2(n74), .ZN(n68) );
  NOR2_X1 U334 ( .A1(B[18]), .A2(A[18]), .ZN(n121) );
  AND2_X1 U335 ( .A1(n396), .A2(n397), .ZN(n127) );
  OR2_X1 U336 ( .A1(n375), .A2(n160), .ZN(n383) );
  NOR2_X1 U337 ( .A1(n240), .A2(n384), .ZN(n233) );
  NAND2_X1 U338 ( .A1(n236), .A2(B[2]), .ZN(n384) );
  NOR2_X1 U339 ( .A1(B[6]), .A2(A[6]), .ZN(n385) );
  INV_X1 U340 ( .A(n159), .ZN(n386) );
  BUF_X1 U341 ( .A(n169), .Z(n391) );
  CLKBUF_X1 U342 ( .A(n205), .Z(n387) );
  INV_X1 U343 ( .A(n195), .ZN(n388) );
  OAI21_X1 U344 ( .B1(n222), .B2(n372), .A(n217), .ZN(n389) );
  OR2_X1 U345 ( .A1(B[21]), .A2(A[21]), .ZN(n427) );
  XNOR2_X1 U346 ( .A(n194), .B(n393), .ZN(SUM[9]) );
  AND2_X1 U347 ( .A1(n257), .A2(n430), .ZN(n393) );
  INV_X1 U348 ( .A(n251), .ZN(n394) );
  CLKBUF_X1 U349 ( .A(n231), .Z(n395) );
  AND2_X1 U350 ( .A1(n169), .A2(n183), .ZN(n396) );
  AND2_X1 U351 ( .A1(n364), .A2(n147), .ZN(n397) );
  NOR2_X1 U352 ( .A1(n369), .A2(n178), .ZN(n169) );
  NOR2_X1 U353 ( .A1(B[4]), .A2(A[4]), .ZN(n227) );
  XOR2_X1 U354 ( .A(n112), .B(n399), .Z(SUM[19]) );
  AND2_X1 U355 ( .A1(n426), .A2(n111), .ZN(n399) );
  AOI21_X1 U356 ( .B1(n198), .B2(n211), .A(n199), .ZN(n400) );
  AOI21_X1 U357 ( .B1(n198), .B2(n389), .A(n199), .ZN(n197) );
  OAI21_X1 U358 ( .B1(n164), .B2(n402), .A(n130), .ZN(n401) );
  NAND2_X1 U359 ( .A1(n364), .A2(n147), .ZN(n402) );
  NOR2_X1 U360 ( .A1(B[7]), .A2(A[7]), .ZN(n205) );
  NOR2_X1 U361 ( .A1(B[10]), .A2(A[10]), .ZN(n189) );
  AOI21_X1 U362 ( .B1(n390), .B2(n418), .A(n401), .ZN(n403) );
  AOI21_X1 U363 ( .B1(n418), .B2(n390), .A(n401), .ZN(n429) );
  CLKBUF_X1 U364 ( .A(n222), .Z(n404) );
  INV_X1 U365 ( .A(n379), .ZN(n405) );
  AOI21_X1 U366 ( .B1(n233), .B2(n225), .A(n226), .ZN(n406) );
  AOI21_X1 U367 ( .B1(n233), .B2(n225), .A(n226), .ZN(n224) );
  INV_X1 U368 ( .A(n257), .ZN(n407) );
  INV_X1 U369 ( .A(n383), .ZN(n408) );
  NOR2_X1 U370 ( .A1(n153), .A2(n160), .ZN(n147) );
  NOR2_X1 U371 ( .A1(B[14]), .A2(A[14]), .ZN(n409) );
  AOI21_X1 U372 ( .B1(n391), .B2(n412), .A(n170), .ZN(n410) );
  OAI21_X1 U373 ( .B1(n411), .B2(n193), .A(n190), .ZN(n412) );
  AOI21_X1 U374 ( .B1(n417), .B2(n127), .A(n128), .ZN(n413) );
  AOI21_X1 U375 ( .B1(n417), .B2(n127), .A(n128), .ZN(n1) );
  INV_X1 U376 ( .A(n213), .ZN(n414) );
  NOR2_X1 U377 ( .A1(B[15]), .A2(A[15]), .ZN(n140) );
  OR2_X1 U378 ( .A1(n235), .A2(n234), .ZN(n415) );
  INV_X1 U379 ( .A(n166), .ZN(n416) );
  OAI21_X1 U380 ( .B1(n406), .B2(n196), .A(n197), .ZN(n417) );
  OAI21_X1 U381 ( .B1(n224), .B2(n196), .A(n400), .ZN(n418) );
  OAI21_X1 U382 ( .B1(n224), .B2(n196), .A(n400), .ZN(n195) );
  AND2_X1 U383 ( .A1(n73), .A2(n116), .ZN(n419) );
  XNOR2_X1 U384 ( .A(n218), .B(n420), .ZN(SUM[6]) );
  AND2_X1 U385 ( .A1(n217), .A2(n359), .ZN(n420) );
  NOR2_X1 U386 ( .A1(n51), .A2(n44), .ZN(n40) );
  INV_X1 U387 ( .A(n32), .ZN(n30) );
  INV_X1 U388 ( .A(n94), .ZN(n92) );
  OR2_X1 U389 ( .A1(n67), .A2(n33), .ZN(n421) );
  INV_X1 U390 ( .A(n406), .ZN(n223) );
  OAI21_X1 U391 ( .B1(n118), .B2(n361), .A(n98), .ZN(n94) );
  INV_X1 U392 ( .A(n52), .ZN(n50) );
  INV_X1 U393 ( .A(n116), .ZN(n118) );
  NAND2_X1 U394 ( .A1(n381), .A2(n253), .ZN(n156) );
  INV_X1 U395 ( .A(n40), .ZN(n33) );
  NAND2_X1 U396 ( .A1(n381), .A2(n138), .ZN(n136) );
  NAND2_X1 U397 ( .A1(n248), .A2(n122), .ZN(n10) );
  NAND2_X1 U398 ( .A1(n256), .A2(n190), .ZN(n18) );
  NAND2_X1 U399 ( .A1(n255), .A2(n366), .ZN(n17) );
  NAND2_X1 U400 ( .A1(n241), .A2(n45), .ZN(n3) );
  INV_X1 U401 ( .A(n44), .ZN(n241) );
  AOI21_X1 U402 ( .B1(n223), .B2(n261), .A(n220), .ZN(n218) );
  XNOR2_X1 U403 ( .A(n162), .B(n15), .ZN(SUM[13]) );
  NAND2_X1 U404 ( .A1(n253), .A2(n386), .ZN(n15) );
  XNOR2_X1 U405 ( .A(n135), .B(n12), .ZN(SUM[16]) );
  NAND2_X1 U406 ( .A1(n250), .A2(n357), .ZN(n12) );
  INV_X1 U407 ( .A(n124), .ZN(n249) );
  XOR2_X1 U408 ( .A(n236), .B(n238), .Z(SUM[1]) );
  AOI21_X1 U409 ( .B1(n423), .B2(n63), .A(n54), .ZN(n52) );
  INV_X1 U410 ( .A(n56), .ZN(n54) );
  XOR2_X1 U411 ( .A(n209), .B(n21), .Z(SUM[7]) );
  NAND2_X1 U412 ( .A1(n368), .A2(n259), .ZN(n21) );
  XOR2_X1 U413 ( .A(n25), .B(n415), .Z(SUM[3]) );
  INV_X1 U414 ( .A(n230), .ZN(n263) );
  XNOR2_X1 U415 ( .A(n229), .B(n24), .ZN(SUM[4]) );
  NAND2_X1 U416 ( .A1(n380), .A2(n228), .ZN(n24) );
  XNOR2_X1 U417 ( .A(n144), .B(n13), .ZN(SUM[15]) );
  INV_X1 U418 ( .A(n140), .ZN(n251) );
  XNOR2_X1 U419 ( .A(n223), .B(n23), .ZN(SUM[5]) );
  OAI21_X1 U420 ( .B1(n121), .B2(n125), .A(n122), .ZN(n116) );
  NOR2_X1 U421 ( .A1(n383), .A2(n394), .ZN(n138) );
  NAND2_X1 U422 ( .A1(n252), .A2(n154), .ZN(n14) );
  NAND2_X1 U423 ( .A1(n254), .A2(n172), .ZN(n16) );
  INV_X1 U424 ( .A(n179), .ZN(n177) );
  NOR2_X1 U425 ( .A1(n124), .A2(n121), .ZN(n115) );
  AOI21_X1 U426 ( .B1(n166), .B2(n253), .A(n159), .ZN(n157) );
  INV_X1 U427 ( .A(n161), .ZN(n159) );
  AOI21_X1 U428 ( .B1(n166), .B2(n138), .A(n139), .ZN(n137) );
  INV_X1 U429 ( .A(n367), .ZN(n255) );
  OAI21_X1 U430 ( .B1(n52), .B2(n44), .A(n45), .ZN(n43) );
  INV_X1 U431 ( .A(n363), .ZN(n261) );
  INV_X1 U432 ( .A(n51), .ZN(n49) );
  INV_X1 U433 ( .A(n111), .ZN(n109) );
  XOR2_X1 U434 ( .A(n202), .B(n20), .Z(SUM[8]) );
  NAND2_X1 U435 ( .A1(n258), .A2(n201), .ZN(n20) );
  AOI21_X1 U436 ( .B1(n203), .B2(n223), .A(n204), .ZN(n202) );
  NAND2_X1 U437 ( .A1(B[12]), .A2(A[12]), .ZN(n172) );
  AND2_X1 U438 ( .A1(n360), .A2(n240), .ZN(SUM[0]) );
  NAND2_X1 U439 ( .A1(B[25]), .A2(A[25]), .ZN(n45) );
  OR2_X1 U440 ( .A1(B[24]), .A2(A[24]), .ZN(n423) );
  NAND2_X1 U441 ( .A1(B[7]), .A2(A[7]), .ZN(n208) );
  OR2_X1 U442 ( .A1(B[20]), .A2(A[20]), .ZN(n424) );
  NAND2_X1 U443 ( .A1(B[16]), .A2(A[16]), .ZN(n134) );
  NAND2_X1 U444 ( .A1(B[15]), .A2(A[15]), .ZN(n143) );
  INV_X1 U445 ( .A(B[2]), .ZN(n234) );
  XOR2_X1 U446 ( .A(n28), .B(CI), .Z(SUM[27]) );
  NAND2_X1 U447 ( .A1(B[8]), .A2(A[8]), .ZN(n201) );
  OR2_X1 U448 ( .A1(B[23]), .A2(A[23]), .ZN(n428) );
  INV_X1 U449 ( .A(n192), .ZN(n257) );
  AOI21_X1 U450 ( .B1(n425), .B2(n87), .A(n78), .ZN(n76) );
  AOI21_X1 U451 ( .B1(n94), .B2(n427), .A(n87), .ZN(n83) );
  INV_X1 U452 ( .A(n365), .ZN(n250) );
  NOR2_X1 U453 ( .A1(B[16]), .A2(A[16]), .ZN(n133) );
  NOR2_X1 U454 ( .A1(B[12]), .A2(A[12]), .ZN(n171) );
  XNOR2_X1 U455 ( .A(n173), .B(n16), .ZN(SUM[12]) );
  AOI21_X1 U456 ( .B1(n424), .B2(n109), .A(n100), .ZN(n98) );
  NAND2_X1 U457 ( .A1(n426), .A2(n424), .ZN(n95) );
  NAND2_X1 U458 ( .A1(n428), .A2(n423), .ZN(n51) );
  NAND2_X1 U459 ( .A1(n423), .A2(n56), .ZN(n4) );
  AOI21_X1 U460 ( .B1(n184), .B2(n169), .A(n170), .ZN(n164) );
  NOR2_X1 U461 ( .A1(n43), .A2(n35), .ZN(n34) );
  INV_X1 U462 ( .A(n80), .ZN(n78) );
  NAND2_X1 U463 ( .A1(n428), .A2(n65), .ZN(n5) );
  INV_X1 U464 ( .A(n65), .ZN(n63) );
  NAND2_X1 U465 ( .A1(n424), .A2(n102), .ZN(n8) );
  INV_X1 U466 ( .A(n102), .ZN(n100) );
  NAND2_X1 U467 ( .A1(n427), .A2(n89), .ZN(n7) );
  INV_X1 U468 ( .A(n89), .ZN(n87) );
  NOR2_X1 U469 ( .A1(n221), .A2(n373), .ZN(n210) );
  NAND2_X1 U470 ( .A1(A[13]), .A2(B[13]), .ZN(n161) );
  XNOR2_X1 U471 ( .A(n155), .B(n14), .ZN(SUM[14]) );
  XNOR2_X1 U472 ( .A(n180), .B(n17), .ZN(SUM[11]) );
  NOR2_X1 U473 ( .A1(B[17]), .A2(A[17]), .ZN(n124) );
  AOI21_X1 U474 ( .B1(n412), .B2(n255), .A(n177), .ZN(n175) );
  INV_X1 U475 ( .A(n412), .ZN(n182) );
  OAI21_X1 U476 ( .B1(n411), .B2(n193), .A(n190), .ZN(n184) );
  INV_X1 U477 ( .A(n387), .ZN(n259) );
  NOR2_X1 U478 ( .A1(n379), .A2(n387), .ZN(n203) );
  OAI21_X1 U479 ( .B1(n213), .B2(n387), .A(n368), .ZN(n204) );
  INV_X1 U480 ( .A(n377), .ZN(n258) );
  OAI21_X1 U481 ( .B1(n200), .B2(n208), .A(n201), .ZN(n199) );
  NOR2_X1 U482 ( .A1(B[8]), .A2(A[8]), .ZN(n200) );
  NAND2_X1 U483 ( .A1(n251), .A2(n143), .ZN(n13) );
  OAI21_X1 U484 ( .B1(n150), .B2(n394), .A(n143), .ZN(n139) );
  OAI21_X1 U485 ( .B1(n133), .B2(n143), .A(n134), .ZN(n132) );
  NAND2_X1 U486 ( .A1(n93), .A2(n427), .ZN(n82) );
  INV_X1 U487 ( .A(n93), .ZN(n91) );
  INV_X1 U488 ( .A(n115), .ZN(n117) );
  NAND2_X1 U489 ( .A1(B[18]), .A2(A[18]), .ZN(n122) );
  NOR2_X1 U490 ( .A1(n117), .A2(n361), .ZN(n93) );
  NOR2_X1 U491 ( .A1(n95), .A2(n75), .ZN(n73) );
  NOR2_X1 U492 ( .A1(n230), .A2(n227), .ZN(n225) );
  NAND2_X1 U493 ( .A1(n381), .A2(n408), .ZN(n145) );
  INV_X1 U494 ( .A(n160), .ZN(n253) );
  NAND2_X1 U495 ( .A1(B[10]), .A2(A[10]), .ZN(n190) );
  XNOR2_X1 U496 ( .A(n191), .B(n18), .ZN(SUM[10]) );
  INV_X1 U497 ( .A(n410), .ZN(n166) );
  INV_X1 U498 ( .A(n121), .ZN(n248) );
  XOR2_X1 U499 ( .A(n403), .B(n11), .Z(SUM[17]) );
  NOR2_X1 U500 ( .A1(n371), .A2(n205), .ZN(n198) );
  NAND2_X1 U501 ( .A1(n263), .A2(n395), .ZN(n25) );
  OAI21_X1 U502 ( .B1(n415), .B2(n230), .A(n395), .ZN(n229) );
  NOR2_X1 U503 ( .A1(B[3]), .A2(A[3]), .ZN(n230) );
  NAND2_X1 U504 ( .A1(n249), .A2(n362), .ZN(n11) );
  NAND2_X1 U505 ( .A1(A[17]), .A2(B[17]), .ZN(n125) );
  XOR2_X1 U506 ( .A(n234), .B(n235), .Z(SUM[2]) );
  NAND2_X1 U507 ( .A1(B[22]), .A2(A[22]), .ZN(n80) );
  INV_X1 U508 ( .A(n67), .ZN(n69) );
  OAI21_X1 U509 ( .B1(n164), .B2(n402), .A(n130), .ZN(n128) );
  CLKBUF_X1 U510 ( .A(n193), .Z(n430) );
  INV_X1 U511 ( .A(n153), .ZN(n252) );
  INV_X1 U512 ( .A(n374), .ZN(n150) );
  AOI21_X1 U513 ( .B1(n364), .B2(n148), .A(n132), .ZN(n130) );
  OAI21_X1 U514 ( .B1(n409), .B2(n161), .A(n154), .ZN(n148) );
  NAND2_X1 U515 ( .A1(B[24]), .A2(A[24]), .ZN(n56) );
  NAND2_X1 U516 ( .A1(n238), .A2(n236), .ZN(n235) );
  AOI21_X1 U517 ( .B1(n392), .B2(n49), .A(n50), .ZN(n48) );
  OAI21_X1 U518 ( .B1(n68), .B2(n33), .A(n34), .ZN(n32) );
  AOI21_X1 U519 ( .B1(n392), .B2(n40), .A(n43), .ZN(n39) );
  AOI21_X1 U520 ( .B1(n392), .B2(n428), .A(n63), .ZN(n59) );
  NOR2_X1 U521 ( .A1(B[13]), .A2(A[13]), .ZN(n160) );
  NAND2_X1 U522 ( .A1(n261), .A2(n404), .ZN(n23) );
  AOI21_X1 U523 ( .B1(n223), .B2(n405), .A(n414), .ZN(n209) );
  INV_X1 U524 ( .A(n404), .ZN(n220) );
  INV_X1 U525 ( .A(n389), .ZN(n213) );
  OAI21_X1 U526 ( .B1(n222), .B2(n385), .A(n217), .ZN(n211) );
  NAND2_X1 U527 ( .A1(n382), .A2(n255), .ZN(n174) );
  INV_X1 U528 ( .A(n369), .ZN(n254) );
  OAI21_X1 U529 ( .B1(n179), .B2(n171), .A(n172), .ZN(n170) );
  INV_X1 U530 ( .A(n189), .ZN(n256) );
  INV_X1 U531 ( .A(n240), .ZN(n238) );
  NAND2_X1 U532 ( .A1(A[3]), .A2(B[3]), .ZN(n231) );
  NAND2_X1 U533 ( .A1(B[4]), .A2(A[4]), .ZN(n228) );
  NAND2_X1 U534 ( .A1(B[23]), .A2(A[23]), .ZN(n65) );
  OAI21_X1 U535 ( .B1(n429), .B2(n421), .A(n30), .ZN(n28) );
  OAI21_X1 U536 ( .B1(n429), .B2(n38), .A(n39), .ZN(n37) );
  OAI21_X1 U537 ( .B1(n231), .B2(n227), .A(n228), .ZN(n226) );
  NAND2_X1 U538 ( .A1(B[6]), .A2(A[6]), .ZN(n217) );
  NOR2_X1 U539 ( .A1(B[11]), .A2(A[11]), .ZN(n178) );
  NAND2_X1 U540 ( .A1(B[11]), .A2(A[11]), .ZN(n179) );
  XNOR2_X1 U541 ( .A(n46), .B(n3), .ZN(SUM[25]) );
  OAI21_X1 U542 ( .B1(n403), .B2(n47), .A(n48), .ZN(n46) );
  INV_X1 U543 ( .A(n195), .ZN(n194) );
  OAI21_X1 U544 ( .B1(n194), .B2(n145), .A(n146), .ZN(n144) );
  OAI21_X1 U545 ( .B1(n194), .B2(n398), .A(n416), .ZN(n162) );
  OAI21_X1 U546 ( .B1(n194), .B2(n136), .A(n137), .ZN(n135) );
  OAI21_X1 U547 ( .B1(n194), .B2(n156), .A(n157), .ZN(n155) );
  OAI21_X1 U548 ( .B1(n388), .B2(n174), .A(n175), .ZN(n173) );
  OAI21_X1 U549 ( .B1(n388), .B2(n358), .A(n182), .ZN(n180) );
  NAND2_X1 U550 ( .A1(n69), .A2(n428), .ZN(n58) );
  NAND2_X1 U551 ( .A1(n69), .A2(n40), .ZN(n38) );
  NAND2_X1 U552 ( .A1(n69), .A2(n49), .ZN(n47) );
  NAND2_X1 U553 ( .A1(n73), .A2(n115), .ZN(n67) );
  NAND2_X1 U554 ( .A1(B[20]), .A2(A[20]), .ZN(n102) );
  NAND2_X1 U555 ( .A1(n210), .A2(n370), .ZN(n196) );
  XNOR2_X1 U556 ( .A(n66), .B(n5), .ZN(SUM[23]) );
  OAI21_X1 U557 ( .B1(n413), .B2(n67), .A(n356), .ZN(n66) );
  NOR2_X1 U558 ( .A1(B[5]), .A2(A[5]), .ZN(n221) );
  NAND2_X1 U559 ( .A1(A[5]), .A2(B[5]), .ZN(n222) );
  OAI21_X1 U560 ( .B1(n1), .B2(n117), .A(n118), .ZN(n112) );
  OAI21_X1 U561 ( .B1(n413), .B2(n58), .A(n59), .ZN(n57) );
  OAI21_X1 U562 ( .B1(n388), .B2(n407), .A(n430), .ZN(n191) );
  NOR2_X1 U563 ( .A1(B[9]), .A2(A[9]), .ZN(n192) );
  NAND2_X1 U564 ( .A1(A[9]), .A2(B[9]), .ZN(n193) );
  XNOR2_X1 U565 ( .A(n37), .B(n35), .ZN(SUM[26]) );
  XNOR2_X1 U566 ( .A(n57), .B(n4), .ZN(SUM[24]) );
  OAI21_X1 U567 ( .B1(n98), .B2(n75), .A(n76), .ZN(n74) );
  NAND2_X1 U568 ( .A1(B[21]), .A2(A[21]), .ZN(n89) );
  NOR2_X1 U569 ( .A1(B[14]), .A2(A[14]), .ZN(n153) );
  NAND2_X1 U570 ( .A1(B[14]), .A2(A[14]), .ZN(n154) );
  AOI21_X1 U571 ( .B1(n166), .B2(n408), .A(n374), .ZN(n146) );
  NAND2_X1 U572 ( .A1(n425), .A2(n427), .ZN(n75) );
  OAI21_X1 U573 ( .B1(n429), .B2(n91), .A(n92), .ZN(n90) );
  XNOR2_X1 U574 ( .A(n90), .B(n7), .ZN(SUM[21]) );
  OAI21_X1 U575 ( .B1(n1), .B2(n82), .A(n83), .ZN(n81) );
  XNOR2_X1 U576 ( .A(n123), .B(n10), .ZN(SUM[18]) );
  OAI21_X1 U577 ( .B1(n413), .B2(n124), .A(n362), .ZN(n123) );
  NAND2_X1 U578 ( .A1(n115), .A2(n426), .ZN(n104) );
  AOI21_X1 U579 ( .B1(n116), .B2(n426), .A(n109), .ZN(n105) );
  NAND2_X1 U580 ( .A1(B[19]), .A2(A[19]), .ZN(n111) );
  NAND2_X1 U581 ( .A1(B[0]), .A2(CI), .ZN(n240) );
  XNOR2_X1 U582 ( .A(n103), .B(n8), .ZN(SUM[20]) );
  OAI21_X1 U583 ( .B1(n403), .B2(n104), .A(n105), .ZN(n103) );
endmodule


module DW_fp_addsub_inst_DW01_inc_2 ( A, SUM );
  input [22:0] A;
  output [22:0] SUM;
  wire   n1, n3, n6, n7, n8, n9, n10, n11, n12, n13, n15, n16, n18, n19, n21,
         n22, n23, n24, n25, n28, n30, n31, n34, n35, n36, n37, n39, n40, n41,
         n42, n44, n45, n46, n47, n49, n51, n52, n54, n55, n56, n57, n60, n61,
         n63, n64, n66, n67, n68, n69, n70, n73, n75, n76, n78, n79, n80, n81,
         n82, n84, n85, n86, n88, n89, n90, n92, n94, n96, n97, n98, n99, n100,
         n101, n103, n104, n106, n107, n108, n110, n161, n162;
  assign n16 = A[19];
  assign n21 = A[18];
  assign n28 = A[17];
  assign n31 = A[16];
  assign n37 = A[15];
  assign n42 = A[14];
  assign n49 = A[13];
  assign n54 = A[12];
  assign n61 = A[11];
  assign n66 = A[10];
  assign n73 = A[9];
  assign n76 = A[8];
  assign n82 = A[7];
  assign n86 = A[6];
  assign n92 = A[5];
  assign n96 = A[4];
  assign n101 = A[3];
  assign n104 = A[2];
  assign n108 = A[1];
  assign n110 = A[0];

  OR2_X1 U136 ( .A1(n70), .A2(n60), .ZN(n161) );
  NAND2_X1 U137 ( .A1(n31), .A2(n28), .ZN(n25) );
  NOR2_X1 U138 ( .A1(n70), .A2(n60), .ZN(n57) );
  NOR2_X1 U139 ( .A1(n100), .A2(n107), .ZN(n99) );
  AND2_X1 U140 ( .A1(n12), .A2(n6), .ZN(n162) );
  NOR2_X1 U141 ( .A1(n25), .A2(n15), .ZN(n12) );
  NAND2_X1 U142 ( .A1(n1), .A2(n19), .ZN(n18) );
  NOR2_X1 U143 ( .A1(n25), .A2(n22), .ZN(n19) );
  INV_X1 U144 ( .A(n99), .ZN(n98) );
  INV_X1 U145 ( .A(n107), .ZN(n106) );
  NAND2_X1 U146 ( .A1(n78), .A2(n64), .ZN(n63) );
  NOR2_X1 U147 ( .A1(n70), .A2(n67), .ZN(n64) );
  INV_X1 U148 ( .A(n70), .ZN(n69) );
  INV_X1 U149 ( .A(n25), .ZN(n24) );
  NAND2_X1 U150 ( .A1(n52), .A2(n78), .ZN(n51) );
  NOR2_X1 U151 ( .A1(n161), .A2(n55), .ZN(n52) );
  NAND2_X1 U152 ( .A1(n45), .A2(n78), .ZN(n44) );
  NOR2_X1 U153 ( .A1(n161), .A2(n46), .ZN(n45) );
  NOR2_X2 U154 ( .A1(n34), .A2(n79), .ZN(n1) );
  NAND2_X1 U155 ( .A1(n57), .A2(n35), .ZN(n34) );
  NOR2_X1 U156 ( .A1(n46), .A2(n36), .ZN(n35) );
  NAND2_X1 U157 ( .A1(n42), .A2(n37), .ZN(n36) );
  NAND2_X1 U158 ( .A1(n76), .A2(n73), .ZN(n70) );
  NAND2_X1 U159 ( .A1(n66), .A2(n61), .ZN(n60) );
  NAND2_X1 U160 ( .A1(n1), .A2(n31), .ZN(n30) );
  NAND2_X1 U161 ( .A1(n1), .A2(n162), .ZN(n3) );
  NOR2_X1 U162 ( .A1(n10), .A2(n7), .ZN(n6) );
  INV_X1 U163 ( .A(n79), .ZN(n78) );
  NAND2_X1 U164 ( .A1(n54), .A2(n49), .ZN(n46) );
  INV_X1 U165 ( .A(n66), .ZN(n67) );
  INV_X1 U166 ( .A(n54), .ZN(n55) );
  INV_X1 U167 ( .A(n21), .ZN(n22) );
  NAND2_X1 U168 ( .A1(n108), .A2(n110), .ZN(n107) );
  NAND2_X1 U169 ( .A1(n101), .A2(n104), .ZN(n100) );
  NAND2_X1 U170 ( .A1(n78), .A2(n76), .ZN(n75) );
  NAND2_X1 U171 ( .A1(n106), .A2(n104), .ZN(n103) );
  NOR2_X1 U172 ( .A1(n13), .A2(n10), .ZN(n9) );
  INV_X1 U173 ( .A(n12), .ZN(n13) );
  NOR2_X1 U174 ( .A1(n98), .A2(n89), .ZN(n88) );
  NOR2_X1 U175 ( .A1(n98), .A2(n97), .ZN(n94) );
  NAND2_X1 U176 ( .A1(n40), .A2(n78), .ZN(n39) );
  NOR2_X1 U177 ( .A1(n161), .A2(n41), .ZN(n40) );
  NAND2_X1 U178 ( .A1(n47), .A2(n42), .ZN(n41) );
  INV_X1 U179 ( .A(n46), .ZN(n47) );
  NAND2_X1 U180 ( .A1(n21), .A2(n16), .ZN(n15) );
  NAND2_X1 U181 ( .A1(n96), .A2(n92), .ZN(n89) );
  NAND2_X1 U182 ( .A1(n80), .A2(n99), .ZN(n79) );
  NOR2_X1 U183 ( .A1(n89), .A2(n81), .ZN(n80) );
  NAND2_X1 U184 ( .A1(n86), .A2(n82), .ZN(n81) );
  INV_X1 U185 ( .A(A[20]), .ZN(n10) );
  INV_X1 U186 ( .A(n96), .ZN(n97) );
  INV_X1 U187 ( .A(A[21]), .ZN(n7) );
  NOR2_X1 U188 ( .A1(n98), .A2(n85), .ZN(n84) );
  NAND2_X1 U189 ( .A1(n90), .A2(n86), .ZN(n85) );
  INV_X1 U190 ( .A(n89), .ZN(n90) );
  XOR2_X1 U191 ( .A(n88), .B(n86), .Z(SUM[6]) );
  XOR2_X1 U192 ( .A(n78), .B(n76), .Z(SUM[8]) );
  XNOR2_X1 U193 ( .A(n44), .B(n42), .ZN(SUM[14]) );
  XOR2_X1 U194 ( .A(n1), .B(n31), .Z(SUM[16]) );
  XOR2_X1 U195 ( .A(n106), .B(n104), .Z(SUM[2]) );
  INV_X1 U196 ( .A(n110), .ZN(SUM[0]) );
  XNOR2_X1 U197 ( .A(n101), .B(n103), .ZN(SUM[3]) );
  XOR2_X1 U198 ( .A(n98), .B(n97), .Z(SUM[4]) );
  XOR2_X1 U199 ( .A(n68), .B(n67), .Z(SUM[10]) );
  NAND2_X1 U200 ( .A1(n78), .A2(n69), .ZN(n68) );
  XOR2_X1 U201 ( .A(n23), .B(n22), .Z(SUM[18]) );
  NAND2_X1 U202 ( .A1(n1), .A2(n24), .ZN(n23) );
  XOR2_X1 U203 ( .A(n56), .B(n55), .Z(SUM[12]) );
  NAND2_X1 U204 ( .A1(n78), .A2(n57), .ZN(n56) );
  XOR2_X1 U205 ( .A(n108), .B(n110), .Z(SUM[1]) );
  XOR2_X1 U206 ( .A(n11), .B(n10), .Z(SUM[20]) );
  NAND2_X1 U207 ( .A1(n1), .A2(n12), .ZN(n11) );
  XOR2_X1 U208 ( .A(n8), .B(n7), .Z(SUM[21]) );
  NAND2_X1 U209 ( .A1(n1), .A2(n9), .ZN(n8) );
  XNOR2_X1 U210 ( .A(n3), .B(A[22]), .ZN(SUM[22]) );
  XNOR2_X1 U211 ( .A(n30), .B(n28), .ZN(SUM[17]) );
  XOR2_X1 U212 ( .A(n94), .B(n92), .Z(SUM[5]) );
  XNOR2_X1 U213 ( .A(n75), .B(n73), .ZN(SUM[9]) );
  XNOR2_X1 U214 ( .A(n63), .B(n61), .ZN(SUM[11]) );
  XNOR2_X1 U215 ( .A(n51), .B(n49), .ZN(SUM[13]) );
  XOR2_X1 U216 ( .A(n84), .B(n82), .Z(SUM[7]) );
  XNOR2_X1 U217 ( .A(n18), .B(n16), .ZN(SUM[19]) );
  XNOR2_X1 U218 ( .A(n39), .B(n37), .ZN(SUM[15]) );
endmodule


module DW_fp_addsub_inst_DW01_add_5 ( A, B, CI, SUM, CO );
  input [4:0] A;
  input [4:0] B;
  output [4:0] SUM;
  input CI;
  output CO;
  wire   n1, n4, n5, n8, n9, n10, n11, n13, n15, n40, n41, n42, n43, n45, n46,
         n47, n48, n49;
  assign n11 = A[2];
  assign n15 = A[1];

  BUF_X1 U27 ( .A(B[0]), .Z(n42) );
  NOR2_X1 U28 ( .A1(n10), .A2(n8), .ZN(n40) );
  CLKBUF_X1 U29 ( .A(n1), .Z(n41) );
  AND2_X1 U30 ( .A1(n42), .A2(n43), .ZN(n5) );
  AND2_X1 U31 ( .A1(A[0]), .A2(n40), .ZN(n43) );
  AND2_X1 U32 ( .A1(n41), .A2(n47), .ZN(SUM[0]) );
  XNOR2_X1 U33 ( .A(n1), .B(n15), .ZN(SUM[1]) );
  AND2_X1 U34 ( .A1(B[0]), .A2(n45), .ZN(n9) );
  AND2_X1 U35 ( .A1(A[0]), .A2(n46), .ZN(n45) );
  INV_X1 U36 ( .A(n10), .ZN(n46) );
  INV_X1 U37 ( .A(A[4]), .ZN(n4) );
  INV_X1 U38 ( .A(A[3]), .ZN(n8) );
  NAND2_X1 U39 ( .A1(n15), .A2(n11), .ZN(n10) );
  OR2_X1 U40 ( .A1(n42), .A2(A[0]), .ZN(n47) );
  NAND2_X1 U41 ( .A1(B[0]), .A2(A[0]), .ZN(n1) );
  XNOR2_X1 U42 ( .A(n13), .B(n48), .ZN(SUM[2]) );
  XNOR2_X1 U43 ( .A(n5), .B(n4), .ZN(SUM[4]) );
  NOR2_X1 U44 ( .A1(n1), .A2(n49), .ZN(n13) );
  XNOR2_X1 U45 ( .A(n9), .B(n8), .ZN(SUM[3]) );
  INV_X1 U46 ( .A(n11), .ZN(n48) );
  INV_X1 U47 ( .A(n15), .ZN(n49) );
endmodule


module DW_fp_addsub_inst_DW_cmp_2 ( A, B, TC, GE_LT, GE_GT_EQ, GE_LT_GT_LE, 
        EQ_NE );
  input [30:0] A;
  input [30:0] B;
  input TC, GE_LT, GE_GT_EQ;
  output GE_LT_GT_LE, EQ_NE;
  wire   n1, n2, n3, n4, n5, n6, n11, n12, n13, n14, n15, n16, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n26, n28, n30, n31, n32, n33, n34, n35,
         n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
         n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63,
         n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77,
         n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91,
         n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104,
         n105, n106, n107, n108, n109, n110, n111, n112, n113, n114, n115,
         n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126,
         n127, n128, n129, n130, n131, n132, n133, n134, n135, n136, n137,
         n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148,
         n1327, n1328, n1329, n1330, n1331, n1332, n1333, n1334, n1335, n1336,
         n1337, n1338, n1339, n1340, n1341, n1342, n1343;

  NOR2_X1 U734 ( .A1(n131), .A2(B[13]), .ZN(n1327) );
  NAND2_X1 U735 ( .A1(n11), .A2(n1342), .ZN(n1328) );
  INV_X1 U736 ( .A(A[7]), .ZN(n1329) );
  NOR2_X1 U737 ( .A1(n141), .A2(B[23]), .ZN(n1330) );
  OR2_X1 U738 ( .A1(n148), .A2(B[30]), .ZN(n1331) );
  OR2_X1 U739 ( .A1(B[30]), .A2(n148), .ZN(n1342) );
  NOR2_X1 U740 ( .A1(n17), .A2(n1328), .ZN(n1332) );
  INV_X1 U741 ( .A(A[27]), .ZN(n1333) );
  INV_X1 U742 ( .A(A[23]), .ZN(n1334) );
  INV_X1 U743 ( .A(A[28]), .ZN(n1335) );
  AND2_X1 U744 ( .A1(n1336), .A2(n1337), .ZN(n25) );
  OR2_X1 U745 ( .A1(n143), .A2(B[25]), .ZN(n1336) );
  OR2_X1 U746 ( .A1(n142), .A2(B[24]), .ZN(n1337) );
  NOR2_X1 U747 ( .A1(n133), .A2(B[15]), .ZN(n1338) );
  NOR2_X1 U748 ( .A1(n143), .A2(B[25]), .ZN(n1339) );
  AND2_X1 U749 ( .A1(n148), .A2(B[30]), .ZN(n1340) );
  NAND2_X1 U750 ( .A1(n121), .A2(B[3]), .ZN(n111) );
  NAND2_X1 U751 ( .A1(n120), .A2(B[2]), .ZN(n113) );
  NAND2_X1 U752 ( .A1(n127), .A2(B[9]), .ZN(n89) );
  NAND2_X1 U753 ( .A1(n126), .A2(B[8]), .ZN(n91) );
  INV_X1 U754 ( .A(A[8]), .ZN(n126) );
  INV_X1 U755 ( .A(A[12]), .ZN(n130) );
  INV_X1 U756 ( .A(A[4]), .ZN(n122) );
  INV_X1 U757 ( .A(A[2]), .ZN(n120) );
  NOR2_X1 U758 ( .A1(n126), .A2(B[8]), .ZN(n90) );
  OAI21_X1 U759 ( .B1(n103), .B2(n106), .A(n104), .ZN(n102) );
  NAND2_X1 U760 ( .A1(n123), .A2(B[5]), .ZN(n104) );
  NAND2_X1 U761 ( .A1(n122), .A2(B[4]), .ZN(n106) );
  NOR2_X1 U762 ( .A1(n130), .A2(B[12]), .ZN(n76) );
  NOR2_X1 U763 ( .A1(n105), .A2(n103), .ZN(n101) );
  NOR2_X1 U764 ( .A1(n122), .A2(B[4]), .ZN(n105) );
  NOR2_X1 U765 ( .A1(n133), .A2(B[15]), .ZN(n68) );
  NAND2_X1 U766 ( .A1(n133), .A2(B[15]), .ZN(n69) );
  NAND2_X1 U767 ( .A1(n125), .A2(B[7]), .ZN(n98) );
  NAND2_X1 U768 ( .A1(n124), .A2(B[6]), .ZN(n100) );
  NAND2_X1 U769 ( .A1(n128), .A2(B[10]), .ZN(n85) );
  NAND2_X1 U770 ( .A1(n129), .A2(B[11]), .ZN(n83) );
  NAND2_X1 U771 ( .A1(n136), .A2(B[18]), .ZN(n54) );
  NAND2_X1 U772 ( .A1(n137), .A2(B[19]), .ZN(n52) );
  NAND2_X1 U773 ( .A1(n131), .A2(B[13]), .ZN(n75) );
  NAND2_X1 U774 ( .A1(n130), .A2(B[12]), .ZN(n77) );
  NAND2_X1 U775 ( .A1(n135), .A2(B[17]), .ZN(n58) );
  NAND2_X1 U776 ( .A1(n134), .A2(B[16]), .ZN(n60) );
  NAND2_X1 U777 ( .A1(n118), .A2(B[0]), .ZN(n117) );
  NAND2_X1 U778 ( .A1(n119), .A2(B[1]), .ZN(n116) );
  NOR2_X1 U779 ( .A1(n119), .A2(B[1]), .ZN(n115) );
  INV_X1 U780 ( .A(A[16]), .ZN(n134) );
  INV_X1 U781 ( .A(A[1]), .ZN(n119) );
  NOR2_X1 U782 ( .A1(n134), .A2(B[16]), .ZN(n59) );
  NOR2_X1 U783 ( .A1(n120), .A2(B[2]), .ZN(n112) );
  INV_X1 U784 ( .A(A[9]), .ZN(n127) );
  INV_X1 U785 ( .A(A[10]), .ZN(n128) );
  INV_X1 U786 ( .A(A[17]), .ZN(n135) );
  INV_X1 U787 ( .A(A[18]), .ZN(n136) );
  INV_X1 U788 ( .A(A[11]), .ZN(n129) );
  INV_X1 U789 ( .A(A[19]), .ZN(n137) );
  INV_X1 U790 ( .A(A[6]), .ZN(n124) );
  INV_X1 U791 ( .A(A[13]), .ZN(n131) );
  INV_X1 U792 ( .A(A[3]), .ZN(n121) );
  INV_X1 U793 ( .A(A[0]), .ZN(n118) );
  INV_X1 U794 ( .A(A[24]), .ZN(n142) );
  INV_X1 U795 ( .A(A[29]), .ZN(n147) );
  NOR2_X1 U796 ( .A1(n128), .A2(B[10]), .ZN(n84) );
  NAND2_X1 U797 ( .A1(n132), .A2(B[14]), .ZN(n71) );
  NOR2_X1 U798 ( .A1(n132), .A2(B[14]), .ZN(n70) );
  INV_X1 U799 ( .A(A[14]), .ZN(n132) );
  NOR2_X1 U800 ( .A1(n136), .A2(B[18]), .ZN(n53) );
  OAI21_X1 U801 ( .B1(n88), .B2(n91), .A(n89), .ZN(n87) );
  NOR2_X1 U802 ( .A1(n90), .A2(n88), .ZN(n86) );
  OAI21_X1 U803 ( .B1(n110), .B2(n113), .A(n111), .ZN(n109) );
  NOR2_X1 U804 ( .A1(n112), .A2(n110), .ZN(n108) );
  INV_X1 U805 ( .A(A[15]), .ZN(n133) );
  INV_X1 U806 ( .A(A[25]), .ZN(n143) );
  NAND2_X1 U807 ( .A1(n138), .A2(B[20]), .ZN(n46) );
  NOR2_X1 U808 ( .A1(n138), .A2(B[20]), .ZN(n45) );
  INV_X1 U809 ( .A(A[20]), .ZN(n138) );
  OAI21_X1 U810 ( .B1(n82), .B2(n85), .A(n83), .ZN(n81) );
  NOR2_X1 U811 ( .A1(n84), .A2(n82), .ZN(n80) );
  OAI21_X1 U812 ( .B1(n115), .B2(n117), .A(n116), .ZN(n114) );
  NOR2_X1 U813 ( .A1(n37), .A2(n39), .ZN(n35) );
  OAI21_X1 U814 ( .B1(n97), .B2(n100), .A(n98), .ZN(n96) );
  AOI21_X1 U815 ( .B1(n12), .B2(n1331), .A(n1340), .ZN(n6) );
  NOR2_X1 U816 ( .A1(n70), .A2(n1338), .ZN(n1341) );
  OAI21_X1 U817 ( .B1(n1343), .B2(n24), .A(n22), .ZN(n20) );
  INV_X1 U818 ( .A(A[26]), .ZN(n144) );
  OAI21_X1 U819 ( .B1(n1327), .B2(n77), .A(n75), .ZN(n73) );
  NOR2_X1 U820 ( .A1(n76), .A2(n74), .ZN(n72) );
  NOR2_X1 U821 ( .A1(n131), .A2(B[13]), .ZN(n74) );
  NOR2_X1 U822 ( .A1(n17), .A2(n5), .ZN(n3) );
  NAND2_X1 U823 ( .A1(n11), .A2(n1342), .ZN(n5) );
  NOR2_X1 U824 ( .A1(n21), .A2(n23), .ZN(n19) );
  AOI21_X1 U825 ( .B1(n80), .B2(n87), .A(n81), .ZN(n79) );
  NAND2_X1 U826 ( .A1(n86), .A2(n80), .ZN(n78) );
  NOR2_X1 U827 ( .A1(n47), .A2(n33), .ZN(n31) );
  NOR2_X1 U828 ( .A1(n59), .A2(n57), .ZN(n55) );
  OAI21_X1 U829 ( .B1(n57), .B2(n60), .A(n58), .ZN(n56) );
  NOR2_X1 U830 ( .A1(n135), .A2(B[17]), .ZN(n57) );
  NOR2_X1 U831 ( .A1(n1333), .A2(B[27]), .ZN(n1343) );
  NOR2_X1 U832 ( .A1(n145), .A2(B[27]), .ZN(n21) );
  INV_X1 U833 ( .A(A[27]), .ZN(n145) );
  INV_X1 U834 ( .A(A[28]), .ZN(n146) );
  INV_X1 U835 ( .A(A[30]), .ZN(n148) );
  OAI21_X1 U836 ( .B1(n107), .B2(n93), .A(n94), .ZN(n92) );
  AOI21_X1 U837 ( .B1(n108), .B2(n114), .A(n109), .ZN(n107) );
  NOR2_X1 U838 ( .A1(n78), .A2(n64), .ZN(n62) );
  NAND2_X1 U839 ( .A1(n35), .A2(n41), .ZN(n33) );
  NOR2_X1 U840 ( .A1(n45), .A2(n43), .ZN(n41) );
  NOR2_X1 U841 ( .A1(n127), .A2(B[9]), .ZN(n88) );
  NOR2_X1 U842 ( .A1(n121), .A2(B[3]), .ZN(n110) );
  NAND2_X1 U843 ( .A1(n142), .A2(B[24]), .ZN(n30) );
  AOI21_X1 U844 ( .B1(n95), .B2(n102), .A(n96), .ZN(n94) );
  NAND2_X1 U845 ( .A1(n95), .A2(n101), .ZN(n93) );
  NAND2_X1 U846 ( .A1(n143), .A2(B[25]), .ZN(n28) );
  NOR2_X1 U847 ( .A1(n129), .A2(B[11]), .ZN(n82) );
  NAND2_X1 U848 ( .A1(n140), .A2(B[22]), .ZN(n40) );
  NOR2_X1 U849 ( .A1(n140), .A2(B[22]), .ZN(n39) );
  INV_X1 U850 ( .A(A[22]), .ZN(n140) );
  OAI21_X1 U851 ( .B1(n79), .B2(n64), .A(n65), .ZN(n63) );
  NOR2_X1 U852 ( .A1(n123), .A2(B[5]), .ZN(n103) );
  INV_X1 U853 ( .A(A[5]), .ZN(n123) );
  NAND2_X1 U854 ( .A1(n147), .A2(B[29]), .ZN(n14) );
  NOR2_X1 U855 ( .A1(n139), .A2(B[21]), .ZN(n43) );
  NAND2_X1 U856 ( .A1(n139), .A2(B[21]), .ZN(n44) );
  INV_X1 U857 ( .A(A[21]), .ZN(n139) );
  OAI21_X1 U858 ( .B1(n43), .B2(n46), .A(n44), .ZN(n42) );
  NAND2_X1 U859 ( .A1(n55), .A2(n49), .ZN(n47) );
  AOI21_X1 U860 ( .B1(n49), .B2(n56), .A(n50), .ZN(n48) );
  NOR2_X1 U861 ( .A1(n53), .A2(n51), .ZN(n49) );
  OAI21_X1 U862 ( .B1(n51), .B2(n54), .A(n52), .ZN(n50) );
  NOR2_X1 U863 ( .A1(n137), .A2(B[19]), .ZN(n51) );
  NOR2_X1 U864 ( .A1(n1334), .A2(B[23]), .ZN(n37) );
  NAND2_X1 U865 ( .A1(B[26]), .A2(n144), .ZN(n24) );
  NOR2_X1 U866 ( .A1(n144), .A2(B[26]), .ZN(n23) );
  INV_X1 U867 ( .A(A[7]), .ZN(n125) );
  OAI21_X1 U868 ( .B1(n1), .B2(n61), .A(n2), .ZN(GE_LT_GT_LE) );
  AOI21_X1 U869 ( .B1(n62), .B2(n92), .A(n63), .ZN(n61) );
  NOR2_X1 U870 ( .A1(n99), .A2(n97), .ZN(n95) );
  NOR2_X1 U871 ( .A1(n124), .A2(B[6]), .ZN(n99) );
  NOR2_X1 U872 ( .A1(n1329), .A2(B[7]), .ZN(n97) );
  AOI21_X1 U873 ( .B1(n35), .B2(n42), .A(n36), .ZN(n34) );
  OAI21_X1 U874 ( .B1(n1330), .B2(n40), .A(n38), .ZN(n36) );
  OAI21_X1 U875 ( .B1(n48), .B2(n33), .A(n34), .ZN(n32) );
  AOI21_X1 U876 ( .B1(n1341), .B2(n73), .A(n67), .ZN(n65) );
  NAND2_X1 U877 ( .A1(n72), .A2(n66), .ZN(n64) );
  OAI21_X1 U878 ( .B1(n68), .B2(n71), .A(n69), .ZN(n67) );
  NOR2_X1 U879 ( .A1(n70), .A2(n1338), .ZN(n66) );
  AOI21_X1 U880 ( .B1(n32), .B2(n1332), .A(n4), .ZN(n2) );
  NAND2_X1 U881 ( .A1(n3), .A2(n31), .ZN(n1) );
  NOR2_X1 U882 ( .A1(n15), .A2(n13), .ZN(n11) );
  NOR2_X1 U883 ( .A1(n147), .A2(B[29]), .ZN(n13) );
  OAI21_X1 U884 ( .B1(n18), .B2(n1328), .A(n6), .ZN(n4) );
  AOI21_X1 U885 ( .B1(n26), .B2(n19), .A(n20), .ZN(n18) );
  NAND2_X1 U886 ( .A1(n25), .A2(n19), .ZN(n17) );
  NAND2_X1 U887 ( .A1(n1335), .A2(B[28]), .ZN(n16) );
  NOR2_X1 U888 ( .A1(B[28]), .A2(n146), .ZN(n15) );
  OAI21_X1 U889 ( .B1(n1339), .B2(n30), .A(n28), .ZN(n26) );
  NAND2_X1 U890 ( .A1(n141), .A2(B[23]), .ZN(n38) );
  INV_X1 U891 ( .A(A[23]), .ZN(n141) );
  OAI21_X1 U892 ( .B1(n16), .B2(n13), .A(n14), .ZN(n12) );
  NAND2_X1 U893 ( .A1(n145), .A2(B[27]), .ZN(n22) );
endmodule


module DW_fp_addsub_inst_DW_rightsh_2 ( A, DATA_TC, SH, B );
  input [25:0] A;
  input [7:0] SH;
  output [25:0] B;
  input DATA_TC;
  wire   n29, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
         n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n67,
         n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81,
         n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n94, n95, n96,
         n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108,
         n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, n119,
         n122, n123, n124, n125, n126, n127, n128, n129, n131, n132, n134,
         n135, n136, n137, n138, n139, n140, n141, n142, n143, n144, n145,
         n146, n147, n163, n164, n165, n166, n167, n168, n169, n171, n172,
         n173, n174, n175, n176, n177, n178, n179, n180, n182, n183, n184,
         n185, n186, n187, n188, n190, n258, n259, n260, n261, n262, n263,
         n264, n265, n266, n267, n268, n269, n270, n271, n272, n273, n274,
         n275, n276, n277, n278, n279, n280, n281, n282, n283, n284, n285,
         n286, n287, n288, n289, n290, n291, n292, n293, n294, n295, n296,
         n297, n298, n299, n300, n301, n302, n303, n304, n305, n306, n307,
         n308, n309, n310, n311, n312, n313, n314, n315, n316, n317, n318,
         n319, n320, n321, n322, n323, n324, n325, n326, n327, n328, n329,
         n330, n331, n332, n333, n334, n335, n336, n337, n338, n339, n340,
         n341, n342, n343, n344, n345, n346, n347, n348, n349;

  CLKBUF_X1 U221 ( .A(n344), .Z(n313) );
  CLKBUF_X3 U222 ( .A(n344), .Z(n314) );
  NAND2_X1 U223 ( .A1(n99), .A2(n258), .ZN(n259) );
  NAND2_X1 U224 ( .A1(n103), .A2(n341), .ZN(n260) );
  NAND2_X1 U225 ( .A1(n259), .A2(n260), .ZN(n72) );
  INV_X1 U226 ( .A(n341), .ZN(n258) );
  INV_X2 U227 ( .A(n342), .ZN(n341) );
  CLKBUF_X3 U228 ( .A(n336), .Z(n339) );
  MUX2_X1 U229 ( .A(n141), .B(n142), .S(n265), .Z(n261) );
  CLKBUF_X3 U230 ( .A(n188), .Z(n272) );
  AND2_X2 U231 ( .A1(n48), .A2(n338), .ZN(B[11]) );
  NAND2_X1 U232 ( .A1(n80), .A2(n262), .ZN(n263) );
  NAND2_X1 U233 ( .A1(n88), .A2(SH[3]), .ZN(n264) );
  NAND2_X1 U234 ( .A1(n263), .A2(n264), .ZN(n50) );
  INV_X1 U235 ( .A(SH[3]), .ZN(n262) );
  MUX2_X2 U236 ( .A(n111), .B(n107), .S(n342), .Z(n80) );
  AND2_X2 U237 ( .A1(n50), .A2(n338), .ZN(B[13]) );
  INV_X1 U238 ( .A(n342), .ZN(n340) );
  OR2_X1 U239 ( .A1(SH[6]), .A2(SH[5]), .ZN(n190) );
  AND2_X1 U240 ( .A1(n46), .A2(n338), .ZN(B[9]) );
  AND2_X1 U241 ( .A1(n44), .A2(n338), .ZN(B[7]) );
  AND2_X1 U242 ( .A1(n40), .A2(n338), .ZN(B[3]) );
  AND2_X1 U243 ( .A1(n43), .A2(n339), .ZN(B[6]) );
  INV_X1 U244 ( .A(n349), .ZN(n265) );
  INV_X1 U245 ( .A(n349), .ZN(n266) );
  AND2_X2 U246 ( .A1(n52), .A2(n338), .ZN(B[15]) );
  NAND2_X1 U247 ( .A1(A[4]), .A2(n267), .ZN(n268) );
  NAND2_X1 U248 ( .A1(A[20]), .A2(n337), .ZN(n269) );
  NAND2_X1 U249 ( .A1(n268), .A2(n269), .ZN(n167) );
  INV_X1 U250 ( .A(n337), .ZN(n267) );
  CLKBUF_X1 U251 ( .A(n188), .Z(n270) );
  CLKBUF_X1 U252 ( .A(n188), .Z(n271) );
  INV_X1 U253 ( .A(SH[4]), .ZN(n188) );
  MUX2_X1 U254 ( .A(n182), .B(n184), .S(n343), .Z(n273) );
  MUX2_X1 U255 ( .A(n101), .B(n105), .S(n341), .Z(n74) );
  MUX2_X1 U256 ( .A(n171), .B(n173), .S(n315), .Z(n274) );
  MUX2_X1 U257 ( .A(A[7]), .B(A[23]), .S(n337), .Z(n275) );
  MUX2_X1 U258 ( .A(n90), .B(n82), .S(n262), .Z(n52) );
  MUX2_X1 U259 ( .A(n134), .B(n292), .S(n349), .Z(n105) );
  MUX2_X1 U260 ( .A(n293), .B(n109), .S(n340), .Z(n78) );
  CLKBUF_X1 U261 ( .A(n337), .Z(n276) );
  NAND2_X1 U262 ( .A1(n167), .A2(n277), .ZN(n278) );
  NAND2_X1 U263 ( .A1(n169), .A2(n313), .ZN(n279) );
  NAND2_X1 U264 ( .A1(n278), .A2(n279), .ZN(n126) );
  INV_X1 U265 ( .A(n314), .ZN(n277) );
  NAND2_X1 U266 ( .A1(n166), .A2(n316), .ZN(n280) );
  NAND2_X1 U267 ( .A1(n168), .A2(n314), .ZN(n281) );
  NAND2_X1 U268 ( .A1(n280), .A2(n281), .ZN(n125) );
  NAND2_X1 U269 ( .A1(n78), .A2(n262), .ZN(n282) );
  NAND2_X1 U270 ( .A1(n86), .A2(SH[3]), .ZN(n283) );
  NAND2_X1 U271 ( .A1(n282), .A2(n283), .ZN(n48) );
  MUX2_X1 U272 ( .A(n134), .B(n292), .S(n349), .Z(n293) );
  INV_X1 U273 ( .A(n349), .ZN(n347) );
  INV_X2 U274 ( .A(SH[0]), .ZN(n349) );
  BUF_X2 U275 ( .A(n344), .Z(n315) );
  MUX2_X1 U276 ( .A(n127), .B(n126), .S(n331), .Z(n98) );
  BUF_X2 U277 ( .A(SH[4]), .Z(n337) );
  INV_X1 U278 ( .A(n349), .ZN(n346) );
  NAND2_X1 U279 ( .A1(n128), .A2(n284), .ZN(n285) );
  NAND2_X1 U280 ( .A1(n129), .A2(n266), .ZN(n286) );
  NAND2_X1 U281 ( .A1(n285), .A2(n286), .ZN(n100) );
  INV_X1 U282 ( .A(n266), .ZN(n284) );
  NAND2_X1 U283 ( .A1(n172), .A2(n316), .ZN(n287) );
  NAND2_X1 U284 ( .A1(n174), .A2(n314), .ZN(n288) );
  NAND2_X1 U285 ( .A1(n287), .A2(n288), .ZN(n131) );
  NAND2_X1 U286 ( .A1(n178), .A2(n289), .ZN(n290) );
  NAND2_X1 U287 ( .A1(n180), .A2(n343), .ZN(n291) );
  NAND2_X1 U288 ( .A1(n291), .A2(n290), .ZN(n137) );
  INV_X1 U289 ( .A(n343), .ZN(n289) );
  MUX2_X1 U290 ( .A(n174), .B(n176), .S(n315), .Z(n292) );
  MUX2_X1 U291 ( .A(n113), .B(n109), .S(n342), .Z(n82) );
  NAND2_X1 U292 ( .A1(A[2]), .A2(n294), .ZN(n295) );
  NAND2_X1 U293 ( .A1(A[18]), .A2(n337), .ZN(n296) );
  NAND2_X1 U294 ( .A1(n295), .A2(n296), .ZN(n165) );
  INV_X1 U295 ( .A(n337), .ZN(n294) );
  NAND2_X1 U296 ( .A1(n70), .A2(n262), .ZN(n297) );
  NAND2_X1 U297 ( .A1(n78), .A2(SH[3]), .ZN(n298) );
  NAND2_X1 U298 ( .A1(n298), .A2(n297), .ZN(n40) );
  NAND2_X1 U299 ( .A1(n137), .A2(n299), .ZN(n300) );
  NAND2_X1 U300 ( .A1(n138), .A2(n347), .ZN(n301) );
  NAND2_X1 U301 ( .A1(n300), .A2(n301), .ZN(n109) );
  INV_X1 U302 ( .A(n346), .ZN(n299) );
  NAND2_X1 U303 ( .A1(n275), .A2(n302), .ZN(n303) );
  NAND2_X1 U304 ( .A1(n172), .A2(n315), .ZN(n304) );
  NAND2_X1 U305 ( .A1(n303), .A2(n304), .ZN(n129) );
  INV_X1 U306 ( .A(n315), .ZN(n302) );
  NAND2_X1 U307 ( .A1(n72), .A2(n262), .ZN(n305) );
  NAND2_X1 U308 ( .A1(n80), .A2(SH[3]), .ZN(n306) );
  NAND2_X1 U309 ( .A1(n305), .A2(n306), .ZN(n42) );
  MUX2_X1 U310 ( .A(n135), .B(n134), .S(n349), .Z(n106) );
  MUX2_X1 U311 ( .A(n307), .B(n179), .S(n316), .Z(n138) );
  MUX2_X1 U312 ( .A(n179), .B(n177), .S(n316), .Z(n136) );
  AND2_X1 U313 ( .A1(A[18]), .A2(n272), .ZN(n307) );
  NAND2_X1 U314 ( .A1(n129), .A2(n284), .ZN(n308) );
  NAND2_X1 U315 ( .A1(n274), .A2(n266), .ZN(n309) );
  NAND2_X1 U316 ( .A1(n308), .A2(n309), .ZN(n101) );
  NAND2_X1 U317 ( .A1(n103), .A2(n310), .ZN(n311) );
  NAND2_X1 U318 ( .A1(n107), .A2(n341), .ZN(n312) );
  NAND2_X1 U319 ( .A1(n311), .A2(n312), .ZN(n76) );
  INV_X1 U320 ( .A(n341), .ZN(n310) );
  INV_X1 U321 ( .A(n345), .ZN(n344) );
  INV_X1 U322 ( .A(SH[1]), .ZN(n316) );
  NAND2_X1 U323 ( .A1(n128), .A2(n317), .ZN(n318) );
  NAND2_X1 U324 ( .A1(n127), .A2(n349), .ZN(n319) );
  NAND2_X1 U325 ( .A1(n319), .A2(n318), .ZN(n99) );
  INV_X1 U326 ( .A(n349), .ZN(n317) );
  NAND2_X1 U327 ( .A1(n75), .A2(n262), .ZN(n320) );
  NAND2_X1 U328 ( .A1(n83), .A2(SH[3]), .ZN(n321) );
  NAND2_X1 U329 ( .A1(n320), .A2(n321), .ZN(n45) );
  NAND2_X1 U330 ( .A1(A[8]), .A2(n270), .ZN(n322) );
  NAND2_X1 U331 ( .A1(n337), .A2(A[24]), .ZN(n323) );
  NAND2_X1 U332 ( .A1(n323), .A2(n322), .ZN(n171) );
  NAND2_X1 U333 ( .A1(n110), .A2(n324), .ZN(n325) );
  NAND2_X1 U334 ( .A1(n114), .A2(n340), .ZN(n326) );
  NAND2_X1 U335 ( .A1(n325), .A2(n326), .ZN(n83) );
  INV_X1 U336 ( .A(n340), .ZN(n324) );
  NAND2_X1 U337 ( .A1(n131), .A2(n299), .ZN(n327) );
  NAND2_X1 U338 ( .A1(n132), .A2(n347), .ZN(n328) );
  NAND2_X1 U339 ( .A1(n327), .A2(n328), .ZN(n103) );
  NAND2_X1 U340 ( .A1(n139), .A2(n331), .ZN(n329) );
  NAND2_X1 U341 ( .A1(n140), .A2(n317), .ZN(n330) );
  NAND2_X1 U342 ( .A1(n329), .A2(n330), .ZN(n111) );
  NAND2_X1 U343 ( .A1(n135), .A2(n331), .ZN(n332) );
  NAND2_X1 U344 ( .A1(n136), .A2(n348), .ZN(n333) );
  NAND2_X1 U345 ( .A1(n332), .A2(n333), .ZN(n107) );
  INV_X1 U346 ( .A(n346), .ZN(n331) );
  NAND2_X1 U347 ( .A1(n143), .A2(n299), .ZN(n334) );
  NAND2_X1 U348 ( .A1(n144), .A2(n266), .ZN(n335) );
  NAND2_X1 U349 ( .A1(n334), .A2(n335), .ZN(n115) );
  NOR2_X1 U350 ( .A1(n190), .A2(SH[7]), .ZN(n336) );
  AND2_X1 U351 ( .A1(n61), .A2(n339), .ZN(B[24]) );
  AND2_X1 U352 ( .A1(n91), .A2(n29), .ZN(n61) );
  AND2_X1 U353 ( .A1(n59), .A2(n339), .ZN(B[22]) );
  AND2_X1 U354 ( .A1(n89), .A2(n29), .ZN(n59) );
  AND2_X1 U355 ( .A1(n58), .A2(n338), .ZN(B[21]) );
  AND2_X1 U356 ( .A1(n88), .A2(n29), .ZN(n58) );
  AND2_X1 U357 ( .A1(n56), .A2(n338), .ZN(B[19]) );
  AND2_X1 U358 ( .A1(n86), .A2(n29), .ZN(n56) );
  MUX2_X1 U359 ( .A(n261), .B(n117), .S(n340), .Z(n86) );
  MUX2_X1 U360 ( .A(n102), .B(n106), .S(n341), .Z(n75) );
  AND2_X1 U361 ( .A1(n60), .A2(n338), .ZN(B[23]) );
  AND2_X1 U362 ( .A1(n90), .A2(n29), .ZN(n60) );
  AND2_X1 U363 ( .A1(n57), .A2(n339), .ZN(B[20]) );
  AND2_X1 U364 ( .A1(n87), .A2(n29), .ZN(n57) );
  AND2_X1 U365 ( .A1(n62), .A2(n338), .ZN(B[25]) );
  AND2_X1 U366 ( .A1(n92), .A2(n29), .ZN(n62) );
  AND2_X1 U367 ( .A1(n55), .A2(n339), .ZN(B[18]) );
  AND2_X1 U368 ( .A1(n85), .A2(n29), .ZN(n55) );
  MUX2_X1 U369 ( .A(n114), .B(n118), .S(n340), .Z(n87) );
  MUX2_X1 U370 ( .A(n112), .B(n116), .S(n340), .Z(n85) );
  MUX2_X1 U371 ( .A(n111), .B(n115), .S(n340), .Z(n84) );
  MUX2_X1 U372 ( .A(n104), .B(n108), .S(n340), .Z(n77) );
  MUX2_X1 U373 ( .A(n108), .B(n112), .S(n340), .Z(n81) );
  MUX2_X1 U374 ( .A(n106), .B(n110), .S(n340), .Z(n79) );
  AND2_X1 U375 ( .A1(n53), .A2(n339), .ZN(B[16]) );
  AND2_X1 U376 ( .A1(n45), .A2(n339), .ZN(B[8]) );
  AND2_X1 U377 ( .A1(n42), .A2(n338), .ZN(B[5]) );
  AND2_X1 U378 ( .A1(n41), .A2(n339), .ZN(B[4]) );
  MUX2_X1 U379 ( .A(n98), .B(n102), .S(n341), .Z(n71) );
  MUX2_X1 U380 ( .A(n145), .B(n146), .S(n347), .Z(n117) );
  MUX2_X1 U381 ( .A(n142), .B(n143), .S(n317), .Z(n114) );
  MUX2_X1 U382 ( .A(n141), .B(n142), .S(n265), .Z(n113) );
  MUX2_X1 U383 ( .A(n274), .B(n131), .S(n348), .Z(n102) );
  BUF_X1 U384 ( .A(n336), .Z(n338) );
  AND2_X1 U385 ( .A1(n54), .A2(n339), .ZN(B[17]) );
  MUX2_X1 U386 ( .A(n97), .B(n101), .S(n341), .Z(n70) );
  MUX2_X1 U387 ( .A(n125), .B(n126), .S(n265), .Z(n97) );
  AND2_X1 U388 ( .A1(n49), .A2(n339), .ZN(B[12]) );
  AND2_X1 U389 ( .A1(n47), .A2(n339), .ZN(B[10]) );
  MUX2_X1 U390 ( .A(n100), .B(n104), .S(n341), .Z(n73) );
  AND2_X1 U391 ( .A1(n39), .A2(n339), .ZN(B[2]) );
  MUX2_X1 U392 ( .A(n96), .B(n100), .S(n341), .Z(n69) );
  MUX2_X1 U393 ( .A(n124), .B(n125), .S(n317), .Z(n96) );
  AND2_X1 U394 ( .A1(n51), .A2(n339), .ZN(B[14]) );
  AND2_X1 U395 ( .A1(n38), .A2(n338), .ZN(B[1]) );
  MUX2_X1 U396 ( .A(n95), .B(n99), .S(n341), .Z(n68) );
  MUX2_X1 U397 ( .A(n123), .B(n124), .S(n347), .Z(n95) );
  AND2_X1 U398 ( .A1(n37), .A2(n339), .ZN(B[0]) );
  MUX2_X1 U399 ( .A(n94), .B(n98), .S(n341), .Z(n67) );
  MUX2_X1 U400 ( .A(n122), .B(n123), .S(n348), .Z(n94) );
  MUX2_X1 U401 ( .A(n146), .B(n147), .S(n348), .Z(n118) );
  MUX2_X1 U402 ( .A(n144), .B(n145), .S(n265), .Z(n116) );
  MUX2_X1 U403 ( .A(n138), .B(n139), .S(n347), .Z(n110) );
  MUX2_X1 U404 ( .A(n140), .B(n273), .S(n317), .Z(n112) );
  MUX2_X1 U405 ( .A(n136), .B(n137), .S(n265), .Z(n108) );
  MUX2_X1 U406 ( .A(n132), .B(n292), .S(n265), .Z(n104) );
  MUX2_X1 U407 ( .A(n115), .B(n119), .S(n340), .Z(n88) );
  AND2_X1 U408 ( .A1(n116), .A2(n342), .ZN(n89) );
  AND2_X1 U409 ( .A1(n118), .A2(n324), .ZN(n91) );
  AND2_X1 U410 ( .A1(n119), .A2(n310), .ZN(n92) );
  AND2_X1 U411 ( .A1(n117), .A2(n342), .ZN(n90) );
  AND2_X1 U412 ( .A1(n147), .A2(n349), .ZN(n119) );
  AND2_X1 U413 ( .A1(n187), .A2(n316), .ZN(n146) );
  INV_X1 U414 ( .A(SH[1]), .ZN(n345) );
  MUX2_X1 U415 ( .A(n163), .B(n165), .S(n313), .Z(n122) );
  MUX2_X1 U416 ( .A(n164), .B(n166), .S(n314), .Z(n123) );
  MUX2_X1 U417 ( .A(n165), .B(n167), .S(n313), .Z(n124) );
  MUX2_X1 U418 ( .A(n168), .B(n275), .S(n314), .Z(n127) );
  MUX2_X1 U419 ( .A(n173), .B(n175), .S(n315), .Z(n132) );
  MUX2_X1 U420 ( .A(n169), .B(n171), .S(n314), .Z(n128) );
  MUX2_X1 U421 ( .A(n176), .B(n178), .S(n343), .Z(n135) );
  MUX2_X1 U422 ( .A(n175), .B(n177), .S(n343), .Z(n134) );
  MUX2_X1 U423 ( .A(n182), .B(n184), .S(n343), .Z(n141) );
  MUX2_X1 U424 ( .A(n183), .B(n185), .S(n343), .Z(n142) );
  MUX2_X1 U425 ( .A(n180), .B(n182), .S(n343), .Z(n139) );
  MUX2_X1 U426 ( .A(n184), .B(n186), .S(n343), .Z(n143) );
  MUX2_X1 U427 ( .A(n307), .B(n183), .S(n343), .Z(n140) );
  MUX2_X1 U428 ( .A(n185), .B(n187), .S(n343), .Z(n144) );
  AND2_X1 U429 ( .A1(A[16]), .A2(n276), .ZN(n163) );
  AND2_X1 U430 ( .A1(n276), .A2(A[17]), .ZN(n164) );
  OR2_X1 U431 ( .A1(A[9]), .A2(n337), .ZN(n172) );
  MUX2_X1 U432 ( .A(A[3]), .B(A[19]), .S(n337), .Z(n166) );
  MUX2_X1 U433 ( .A(A[5]), .B(A[21]), .S(n337), .Z(n168) );
  MUX2_X1 U434 ( .A(A[6]), .B(A[22]), .S(n337), .Z(n169) );
  MUX2_X1 U435 ( .A(n68), .B(n76), .S(SH[3]), .Z(n38) );
  MUX2_X1 U436 ( .A(n67), .B(n75), .S(SH[3]), .Z(n37) );
  MUX2_X1 U437 ( .A(n84), .B(n92), .S(SH[3]), .Z(n54) );
  MUX2_X1 U438 ( .A(n83), .B(n91), .S(SH[3]), .Z(n53) );
  MUX2_X1 U439 ( .A(n74), .B(n82), .S(SH[3]), .Z(n44) );
  MUX2_X1 U440 ( .A(n73), .B(n81), .S(SH[3]), .Z(n43) );
  MUX2_X1 U441 ( .A(n69), .B(n77), .S(SH[3]), .Z(n39) );
  MUX2_X1 U442 ( .A(n79), .B(n87), .S(SH[3]), .Z(n49) );
  MUX2_X1 U443 ( .A(n77), .B(n85), .S(SH[3]), .Z(n47) );
  MUX2_X1 U444 ( .A(n81), .B(n89), .S(SH[3]), .Z(n51) );
  MUX2_X1 U445 ( .A(n71), .B(n79), .S(SH[3]), .Z(n41) );
  MUX2_X1 U446 ( .A(n76), .B(n84), .S(SH[3]), .Z(n46) );
  INV_X1 U447 ( .A(SH[3]), .ZN(n29) );
  AND2_X1 U448 ( .A1(n271), .A2(n316), .ZN(n147) );
  MUX2_X1 U449 ( .A(n186), .B(n272), .S(n343), .Z(n145) );
  AND2_X1 U450 ( .A1(A[17]), .A2(n272), .ZN(n180) );
  AND2_X1 U451 ( .A1(A[10]), .A2(n272), .ZN(n173) );
  AND2_X1 U452 ( .A1(A[13]), .A2(n272), .ZN(n176) );
  AND2_X1 U453 ( .A1(A[11]), .A2(n271), .ZN(n174) );
  AND2_X1 U454 ( .A1(A[19]), .A2(n272), .ZN(n182) );
  AND2_X1 U455 ( .A1(A[14]), .A2(n272), .ZN(n177) );
  AND2_X1 U456 ( .A1(A[12]), .A2(n272), .ZN(n175) );
  AND2_X1 U457 ( .A1(A[15]), .A2(n271), .ZN(n178) );
  AND2_X1 U458 ( .A1(A[16]), .A2(n272), .ZN(n179) );
  AND2_X1 U459 ( .A1(A[24]), .A2(n271), .ZN(n187) );
  AND2_X1 U460 ( .A1(A[20]), .A2(n270), .ZN(n183) );
  AND2_X1 U461 ( .A1(A[21]), .A2(n272), .ZN(n184) );
  AND2_X1 U462 ( .A1(A[22]), .A2(n270), .ZN(n185) );
  AND2_X1 U463 ( .A1(A[23]), .A2(n272), .ZN(n186) );
  INV_X2 U464 ( .A(n345), .ZN(n343) );
  INV_X1 U465 ( .A(SH[2]), .ZN(n342) );
  INV_X1 U466 ( .A(n349), .ZN(n348) );
endmodule


module DW_fp_addsub_inst_DW01_add_8 ( A, B, CI, SUM, CO );
  input [6:0] A;
  input [6:0] B;
  output [6:0] SUM;
  input CI;
  output CO;
  wire   \B[0] , \B[1] ;
  assign SUM[0] = \B[0] ;
  assign \B[0]  = B[0];
  assign SUM[1] = \B[1] ;
  assign \B[1]  = B[1];

endmodule


module DW_fp_addsub_inst_add_369_DP_OP_291_9752_4 ( I1, I2, O6, O2 );
  input [7:0] I1;
  input [4:0] I2;
  output [8:0] O6;
  output [9:0] O2;
  wire   n3, n11, n12, n14, n15, n16, n19, n22, n23, n24, n25, n26, n27, n28,
         n31, n32, n33, n34, n35, n36, n37, n38, n41, n46, n47, n48, n49, n50,
         n53, n54, n58, n61, n63, n65, n66, n67, n69, n71, n72, n73, n74, n80,
         n82, n83, n84, n85, n86, n87, n91, n92, n93, n94, n95, n100, n101,
         n102, n103, n104, n105, n106, n110, n113, n117, n118, n123, n124,
         n125, n129, n131, n133, n134, n135, n137, n138, n144, n145, n146,
         n147, n148, n149, n150, n151, n154, n155, n156, n157, n158, n159,
         n160, n162, n163, n164, n165, n166, n172, n173, n174, n175, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n203;
  assign n22 = I1[4];
  assign n26 = I1[3];
  assign n31 = I1[2];
  assign n35 = I1[1];
  assign n37 = I1[0];
  assign O6[0] = n38;
  assign n41 = I1[5];
  assign n47 = I1[7];
  assign n124 = I1[6];
  assign O2[1] = n144;
  assign O2[2] = n145;
  assign O2[3] = n146;
  assign O2[4] = n147;
  assign O2[5] = n148;
  assign O2[6] = n149;
  assign O2[7] = n150;
  assign O2[8] = n151;
  assign O6[1] = n154;
  assign O6[2] = n155;
  assign O6[3] = n156;
  assign O6[4] = n157;
  assign O6[5] = n158;
  assign O6[6] = n159;
  assign O6[7] = n160;
  assign n162 = I2[0];
  assign n163 = I2[1];
  assign n164 = I2[2];
  assign n165 = I2[3];
  assign n166 = I2[4];

  XNOR2_X1 U154 ( .A(n166), .B(n22), .ZN(n172) );
  XNOR2_X1 U155 ( .A(n166), .B(n22), .ZN(n173) );
  BUF_X1 U156 ( .A(n187), .Z(n174) );
  AOI21_X2 U157 ( .B1(n103), .B2(n91), .A(n92), .ZN(n46) );
  AND2_X2 U158 ( .A1(n203), .A2(n58), .ZN(O2[9]) );
  AND2_X1 U159 ( .A1(n135), .A2(n26), .ZN(n175) );
  AND2_X1 U160 ( .A1(n201), .A2(n113), .ZN(O2[0]) );
  BUF_X1 U161 ( .A(n85), .Z(n178) );
  OR2_X1 U162 ( .A1(n180), .A2(n137), .ZN(n177) );
  XNOR2_X1 U163 ( .A(n165), .B(n26), .ZN(n131) );
  CLKBUF_X1 U164 ( .A(n164), .Z(n179) );
  XNOR2_X1 U165 ( .A(n179), .B(n31), .ZN(n180) );
  CLKBUF_X1 U166 ( .A(n163), .Z(n185) );
  CLKBUF_X1 U167 ( .A(n200), .Z(n189) );
  INV_X1 U168 ( .A(n195), .ZN(n113) );
  XNOR2_X1 U169 ( .A(n164), .B(n31), .ZN(n181) );
  INV_X1 U170 ( .A(n118), .ZN(n182) );
  NOR2_X1 U171 ( .A1(n193), .A2(n131), .ZN(n100) );
  CLKBUF_X1 U172 ( .A(n101), .Z(n183) );
  AOI21_X1 U173 ( .B1(n200), .B2(n195), .A(n191), .ZN(n184) );
  OAI21_X1 U174 ( .B1(n104), .B2(n106), .A(n105), .ZN(n186) );
  NOR2_X1 U175 ( .A1(n173), .A2(n175), .ZN(n187) );
  AND2_X1 U176 ( .A1(n134), .A2(n22), .ZN(n188) );
  AOI21_X1 U177 ( .B1(n186), .B2(n91), .A(n92), .ZN(n190) );
  INV_X1 U178 ( .A(n191), .ZN(n110) );
  NOR2_X1 U179 ( .A1(n164), .A2(n32), .ZN(n193) );
  AND2_X1 U180 ( .A1(n163), .A2(n35), .ZN(n191) );
  XNOR2_X1 U181 ( .A(n102), .B(n192), .ZN(n146) );
  AND2_X1 U182 ( .A1(n101), .A2(n118), .ZN(n192) );
  NOR2_X1 U183 ( .A1(n172), .A2(n175), .ZN(n93) );
  XNOR2_X1 U184 ( .A(n164), .B(n31), .ZN(n133) );
  CLKBUF_X1 U185 ( .A(n184), .Z(n194) );
  XNOR2_X1 U186 ( .A(n166), .B(n22), .ZN(n129) );
  AND2_X2 U187 ( .A1(n138), .A2(n37), .ZN(n195) );
  NOR2_X1 U188 ( .A1(n188), .A2(n3), .ZN(n84) );
  NOR2_X1 U189 ( .A1(n25), .A2(n34), .ZN(n24) );
  INV_X1 U190 ( .A(n84), .ZN(n86) );
  OR2_X1 U191 ( .A1(n84), .A2(n66), .ZN(n196) );
  NAND2_X1 U192 ( .A1(n86), .A2(n178), .ZN(n50) );
  INV_X1 U193 ( .A(n85), .ZN(n87) );
  INV_X1 U194 ( .A(n65), .ZN(n63) );
  NAND2_X1 U195 ( .A1(n198), .A2(n82), .ZN(n49) );
  NAND2_X1 U196 ( .A1(n199), .A2(n71), .ZN(n48) );
  AOI21_X1 U197 ( .B1(n87), .B2(n198), .A(n80), .ZN(n74) );
  NAND2_X1 U198 ( .A1(n86), .A2(n198), .ZN(n73) );
  NAND2_X1 U199 ( .A1(n188), .A2(n3), .ZN(n85) );
  NAND2_X1 U200 ( .A1(n177), .A2(n105), .ZN(n53) );
  OAI21_X1 U201 ( .B1(n85), .B2(n66), .A(n67), .ZN(n65) );
  AOI21_X1 U202 ( .B1(n199), .B2(n80), .A(n69), .ZN(n67) );
  INV_X1 U203 ( .A(n71), .ZN(n69) );
  XOR2_X1 U204 ( .A(n95), .B(n197), .Z(n147) );
  AND2_X1 U205 ( .A1(n117), .A2(n94), .ZN(n197) );
  INV_X1 U206 ( .A(n82), .ZN(n80) );
  NAND2_X1 U207 ( .A1(n198), .A2(n199), .ZN(n66) );
  XNOR2_X1 U208 ( .A(n24), .B(n23), .ZN(n157) );
  XOR2_X1 U209 ( .A(n14), .B(n125), .Z(n159) );
  XOR2_X1 U210 ( .A(n11), .B(n123), .Z(n160) );
  XOR2_X1 U211 ( .A(n19), .B(n3), .Z(n158) );
  OR2_X1 U212 ( .A1(n125), .A2(n41), .ZN(n198) );
  INV_X1 U213 ( .A(n124), .ZN(n125) );
  INV_X1 U214 ( .A(n41), .ZN(n3) );
  OR2_X1 U215 ( .A1(n123), .A2(n124), .ZN(n199) );
  INV_X1 U216 ( .A(n47), .ZN(n123) );
  NAND2_X1 U217 ( .A1(n41), .A2(n22), .ZN(n16) );
  NAND2_X1 U218 ( .A1(n125), .A2(n41), .ZN(n82) );
  NAND2_X1 U219 ( .A1(n123), .A2(n124), .ZN(n71) );
  NAND2_X1 U220 ( .A1(n24), .A2(n22), .ZN(n19) );
  NAND2_X1 U221 ( .A1(n24), .A2(n15), .ZN(n14) );
  INV_X1 U222 ( .A(n16), .ZN(n15) );
  NAND2_X1 U223 ( .A1(n12), .A2(n24), .ZN(n11) );
  NOR2_X1 U224 ( .A1(n16), .A2(n125), .ZN(n12) );
  INV_X1 U225 ( .A(n22), .ZN(n23) );
  INV_X1 U226 ( .A(n34), .ZN(n33) );
  XNOR2_X1 U227 ( .A(n36), .B(n37), .ZN(n154) );
  XNOR2_X1 U228 ( .A(n33), .B(n32), .ZN(n155) );
  XOR2_X1 U229 ( .A(n28), .B(n27), .Z(n156) );
  OR2_X1 U230 ( .A1(n163), .A2(n35), .ZN(n200) );
  INV_X1 U231 ( .A(n37), .ZN(n38) );
  OR2_X1 U232 ( .A1(n138), .A2(n37), .ZN(n201) );
  INV_X1 U233 ( .A(n26), .ZN(n27) );
  INV_X1 U234 ( .A(n162), .ZN(n138) );
  INV_X1 U235 ( .A(n174), .ZN(n117) );
  AOI21_X1 U236 ( .B1(n200), .B2(n195), .A(n191), .ZN(n106) );
  NAND2_X1 U237 ( .A1(n129), .A2(n175), .ZN(n94) );
  OAI21_X1 U238 ( .B1(n184), .B2(n104), .A(n105), .ZN(n103) );
  NAND2_X1 U239 ( .A1(n133), .A2(n137), .ZN(n105) );
  INV_X1 U240 ( .A(n31), .ZN(n32) );
  NAND2_X1 U241 ( .A1(n33), .A2(n31), .ZN(n28) );
  NAND2_X1 U242 ( .A1(n31), .A2(n26), .ZN(n25) );
  OAI21_X1 U243 ( .B1(n102), .B2(n182), .A(n183), .ZN(n95) );
  INV_X1 U244 ( .A(n100), .ZN(n118) );
  XOR2_X1 U245 ( .A(n190), .B(n50), .Z(n148) );
  OAI21_X1 U246 ( .B1(n190), .B2(n196), .A(n63), .ZN(n61) );
  OR2_X1 U247 ( .A1(n46), .A2(n196), .ZN(n203) );
  NOR2_X1 U248 ( .A1(n65), .A2(n47), .ZN(n58) );
  OAI21_X1 U249 ( .B1(n46), .B2(n84), .A(n178), .ZN(n83) );
  XNOR2_X1 U250 ( .A(n61), .B(n47), .ZN(n151) );
  XNOR2_X1 U251 ( .A(n54), .B(n195), .ZN(n144) );
  INV_X1 U252 ( .A(n185), .ZN(n137) );
  NAND2_X1 U253 ( .A1(n189), .A2(n110), .ZN(n54) );
  XOR2_X1 U254 ( .A(n53), .B(n194), .Z(n145) );
  XNOR2_X1 U255 ( .A(n72), .B(n48), .ZN(n150) );
  OAI21_X1 U256 ( .B1(n46), .B2(n73), .A(n74), .ZN(n72) );
  XNOR2_X1 U257 ( .A(n83), .B(n49), .ZN(n149) );
  INV_X1 U258 ( .A(n165), .ZN(n135) );
  NOR2_X1 U259 ( .A1(n181), .A2(n137), .ZN(n104) );
  INV_X1 U260 ( .A(n35), .ZN(n36) );
  NAND2_X1 U261 ( .A1(n35), .A2(n37), .ZN(n34) );
  INV_X1 U262 ( .A(n186), .ZN(n102) );
  NAND2_X1 U263 ( .A1(n131), .A2(n193), .ZN(n101) );
  NOR2_X1 U264 ( .A1(n187), .A2(n100), .ZN(n91) );
  OAI21_X1 U265 ( .B1(n93), .B2(n101), .A(n94), .ZN(n92) );
  INV_X1 U266 ( .A(n166), .ZN(n134) );
endmodule


module DW_fp_addsub_inst_sub_256_DP_OP_292_7885_7 ( I1, I2, O11, O9 );
  input [7:0] I1;
  input [7:0] I2;
  output [8:0] O11;
  output [7:0] O9;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n11, n12, n13, n15, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n30, n31, n32, n33, n34, n35, n36, n41,
         n42, n43, n44, n45, n46, n47, n50, n51, n52, n53, n54, n55, n56, n57,
         n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71,
         n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89,
         n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n105, n106,
         n108, n109, n110, n111, n112, n117, n118, n119, n120, n121, n122,
         n123, n126, n128, n129, n131, n132, n133, n134, n135, n136, n137,
         n138, n143, n144, n145, n146, n147, n148, n149, n150, n151, n152,
         n153, n154, n155, n156, n157, n158, n159, n168, n169, n170, n171,
         n172, n173, n174, n175, n176, n177, n178, n179, n180, n181, n182,
         n183, n184, n185, n186, n187, n188, n189, n190, n191, n192, n193,
         n194, n195, n196, n197, n198, n199;
  assign n56 = I2[7];
  assign n58 = I2[6];
  assign n60 = I2[5];
  assign n62 = I2[4];
  assign n64 = I2[3];
  assign n66 = I2[2];
  assign n68 = I2[1];
  assign n70 = I2[0];
  assign O9[0] = n143;
  assign O9[1] = n144;
  assign O9[2] = n145;
  assign O9[3] = n146;
  assign O9[4] = n147;
  assign O9[5] = n148;
  assign O9[6] = n149;
  assign O9[7] = n150;
  assign O11[0] = n151;
  assign O11[1] = n152;
  assign O11[2] = n153;
  assign O11[3] = n154;
  assign O11[4] = n155;
  assign O11[5] = n156;
  assign O11[6] = n157;
  assign O11[7] = n158;
  assign O11[8] = n159;
  assign n168 = I1[0];
  assign n169 = I1[1];
  assign n170 = I1[2];
  assign n171 = I1[3];
  assign n172 = I1[4];
  assign n173 = I1[5];
  assign n174 = I1[6];
  assign n175 = I1[7];

  BUF_X1 U158 ( .A(n70), .Z(n176) );
  NOR2_X1 U159 ( .A1(n133), .A2(n173), .ZN(n98) );
  CLKBUF_X1 U160 ( .A(n110), .Z(n177) );
  BUF_X1 U161 ( .A(n97), .Z(n181) );
  XNOR2_X1 U162 ( .A(n190), .B(n178), .ZN(n155) );
  AND2_X1 U163 ( .A1(n31), .A2(n106), .ZN(n178) );
  AOI21_X1 U164 ( .B1(n32), .B2(n44), .A(n33), .ZN(n179) );
  AOI21_X1 U165 ( .B1(n32), .B2(n44), .A(n33), .ZN(n1) );
  BUF_X1 U166 ( .A(n123), .Z(n180) );
  AND2_X2 U167 ( .A1(n183), .A2(n199), .ZN(n190) );
  NOR2_X1 U168 ( .A1(n135), .A2(n171), .ZN(n110) );
  CLKBUF_X1 U169 ( .A(n66), .Z(n182) );
  NOR2_X1 U170 ( .A1(n136), .A2(n170), .ZN(n117) );
  NOR2_X1 U171 ( .A1(n105), .A2(n98), .ZN(n96) );
  CLKBUF_X1 U172 ( .A(n198), .Z(n183) );
  OAI21_X1 U173 ( .B1(n123), .B2(n121), .A(n122), .ZN(n184) );
  CLKBUF_X1 U174 ( .A(n106), .Z(n185) );
  NOR2_X1 U175 ( .A1(n131), .A2(n175), .ZN(n84) );
  AND2_X1 U176 ( .A1(n198), .A2(n199), .ZN(n71) );
  OR2_X1 U177 ( .A1(n131), .A2(n175), .ZN(n186) );
  CLKBUF_X1 U178 ( .A(n64), .Z(n187) );
  CLKBUF_X1 U179 ( .A(n68), .Z(n188) );
  CLKBUF_X1 U180 ( .A(n56), .Z(n189) );
  OR2_X1 U181 ( .A1(n137), .A2(n169), .ZN(n191) );
  INV_X1 U182 ( .A(n79), .ZN(n159) );
  XOR2_X1 U183 ( .A(n93), .B(n192), .Z(n157) );
  AND2_X1 U184 ( .A1(n89), .A2(n92), .ZN(n192) );
  XOR2_X1 U185 ( .A(n86), .B(n193), .Z(n158) );
  AND2_X1 U186 ( .A1(n186), .A2(n85), .ZN(n193) );
  XOR2_X1 U187 ( .A(n100), .B(n194), .Z(n156) );
  AND2_X1 U188 ( .A1(n126), .A2(n99), .ZN(n194) );
  INV_X1 U189 ( .A(n92), .ZN(n90) );
  INV_X1 U190 ( .A(n21), .ZN(n19) );
  INV_X1 U191 ( .A(n22), .ZN(n20) );
  XNOR2_X1 U192 ( .A(n18), .B(n3), .ZN(n149) );
  INV_X1 U193 ( .A(n91), .ZN(n89) );
  XOR2_X1 U194 ( .A(n119), .B(n77), .Z(n153) );
  XOR2_X1 U195 ( .A(n43), .B(n7), .Z(n145) );
  XNOR2_X1 U196 ( .A(n36), .B(n6), .ZN(n146) );
  NAND2_X1 U197 ( .A1(n52), .A2(n35), .ZN(n6) );
  OAI21_X1 U198 ( .B1(n43), .B2(n41), .A(n42), .ZN(n36) );
  XNOR2_X1 U199 ( .A(n112), .B(n76), .ZN(n154) );
  NAND2_X1 U200 ( .A1(n128), .A2(n111), .ZN(n76) );
  NAND2_X1 U201 ( .A1(n21), .A2(n195), .ZN(n12) );
  AOI21_X1 U202 ( .B1(n22), .B2(n195), .A(n15), .ZN(n13) );
  INV_X1 U203 ( .A(n17), .ZN(n15) );
  XOR2_X1 U204 ( .A(n78), .B(n180), .Z(n152) );
  NOR2_X1 U205 ( .A1(n132), .A2(n174), .ZN(n91) );
  OAI21_X1 U206 ( .B1(n23), .B2(n31), .A(n24), .ZN(n22) );
  NOR2_X1 U207 ( .A1(n30), .A2(n23), .ZN(n21) );
  XNOR2_X1 U208 ( .A(n11), .B(n2), .ZN(n150) );
  NOR2_X1 U209 ( .A1(n84), .A2(n91), .ZN(n82) );
  XNOR2_X1 U210 ( .A(n25), .B(n4), .ZN(n148) );
  NAND2_X1 U211 ( .A1(n51), .A2(n31), .ZN(n5) );
  INV_X1 U212 ( .A(n30), .ZN(n51) );
  NAND2_X1 U213 ( .A1(n50), .A2(n24), .ZN(n4) );
  INV_X1 U214 ( .A(n23), .ZN(n50) );
  NAND2_X1 U215 ( .A1(n195), .A2(n17), .ZN(n3) );
  NAND2_X1 U216 ( .A1(n53), .A2(n42), .ZN(n7) );
  INV_X1 U217 ( .A(n41), .ZN(n53) );
  NAND2_X1 U218 ( .A1(n54), .A2(n46), .ZN(n8) );
  INV_X1 U219 ( .A(n174), .ZN(n57) );
  NAND2_X1 U220 ( .A1(n134), .A2(n172), .ZN(n106) );
  AND2_X1 U221 ( .A1(n70), .A2(n197), .ZN(n123) );
  INV_X1 U222 ( .A(n169), .ZN(n67) );
  NOR2_X1 U223 ( .A1(n60), .A2(n59), .ZN(n23) );
  NOR2_X1 U224 ( .A1(n61), .A2(n62), .ZN(n30) );
  XOR2_X1 U225 ( .A(n138), .B(n69), .Z(n151) );
  OR2_X1 U226 ( .A1(n58), .A2(n57), .ZN(n195) );
  NAND2_X1 U227 ( .A1(n182), .A2(n65), .ZN(n42) );
  NAND2_X1 U228 ( .A1(n131), .A2(n175), .ZN(n85) );
  INV_X1 U229 ( .A(n175), .ZN(n55) );
  OR2_X1 U230 ( .A1(n9), .A2(n196), .ZN(n2) );
  AND2_X1 U231 ( .A1(n189), .A2(n55), .ZN(n196) );
  INV_X1 U232 ( .A(n170), .ZN(n65) );
  NOR2_X1 U233 ( .A1(n182), .A2(n65), .ZN(n41) );
  OAI21_X1 U234 ( .B1(n179), .B2(n12), .A(n13), .ZN(n11) );
  OAI21_X1 U235 ( .B1(n179), .B2(n19), .A(n20), .ZN(n18) );
  XOR2_X1 U236 ( .A(n1), .B(n5), .Z(n147) );
  OAI21_X1 U237 ( .B1(n179), .B2(n30), .A(n31), .ZN(n25) );
  NAND2_X1 U238 ( .A1(n191), .A2(n122), .ZN(n78) );
  NAND2_X1 U239 ( .A1(n129), .A2(n118), .ZN(n77) );
  INV_X1 U240 ( .A(n45), .ZN(n54) );
  NOR2_X1 U241 ( .A1(n188), .A2(n67), .ZN(n45) );
  INV_X1 U242 ( .A(n44), .ZN(n43) );
  INV_X1 U243 ( .A(n168), .ZN(n197) );
  NAND2_X1 U244 ( .A1(n135), .A2(n171), .ZN(n111) );
  INV_X1 U245 ( .A(n171), .ZN(n63) );
  NAND2_X1 U246 ( .A1(n132), .A2(n174), .ZN(n92) );
  NOR2_X1 U247 ( .A1(n70), .A2(n69), .ZN(n47) );
  XOR2_X1 U248 ( .A(n8), .B(n47), .Z(n144) );
  OAI21_X1 U249 ( .B1(n45), .B2(n47), .A(n46), .ZN(n44) );
  NAND2_X1 U250 ( .A1(n136), .A2(n170), .ZN(n118) );
  NAND2_X1 U251 ( .A1(n96), .A2(n89), .ZN(n87) );
  NAND2_X1 U252 ( .A1(n96), .A2(n82), .ZN(n80) );
  INV_X1 U253 ( .A(n96), .ZN(n94) );
  NOR2_X1 U254 ( .A1(n187), .A2(n63), .ZN(n34) );
  INV_X1 U255 ( .A(n173), .ZN(n59) );
  AND2_X1 U256 ( .A1(n62), .A2(n61), .ZN(n105) );
  INV_X1 U257 ( .A(n172), .ZN(n61) );
  INV_X1 U258 ( .A(n168), .ZN(n69) );
  NAND2_X1 U259 ( .A1(n188), .A2(n67), .ZN(n46) );
  INV_X1 U260 ( .A(n34), .ZN(n52) );
  NOR2_X1 U261 ( .A1(n34), .A2(n41), .ZN(n32) );
  OAI21_X1 U262 ( .B1(n34), .B2(n42), .A(n35), .ZN(n33) );
  INV_X1 U263 ( .A(n66), .ZN(n136) );
  NAND2_X1 U264 ( .A1(n58), .A2(n57), .ZN(n17) );
  INV_X1 U265 ( .A(n58), .ZN(n132) );
  XNOR2_X1 U266 ( .A(n176), .B(n69), .ZN(n143) );
  INV_X1 U267 ( .A(n70), .ZN(n138) );
  INV_X1 U268 ( .A(n120), .ZN(n119) );
  INV_X1 U269 ( .A(n117), .ZN(n129) );
  OAI21_X1 U270 ( .B1(n119), .B2(n117), .A(n118), .ZN(n112) );
  AOI21_X1 U271 ( .B1(n181), .B2(n89), .A(n90), .ZN(n88) );
  INV_X1 U272 ( .A(n181), .ZN(n95) );
  INV_X1 U273 ( .A(n98), .ZN(n126) );
  AOI21_X1 U274 ( .B1(n97), .B2(n82), .A(n83), .ZN(n81) );
  OAI21_X1 U275 ( .B1(n98), .B2(n106), .A(n99), .ZN(n97) );
  NAND2_X1 U276 ( .A1(n184), .A2(n108), .ZN(n198) );
  INV_X1 U277 ( .A(n109), .ZN(n199) );
  NAND2_X1 U278 ( .A1(n187), .A2(n63), .ZN(n35) );
  INV_X1 U279 ( .A(n64), .ZN(n135) );
  NAND2_X1 U280 ( .A1(n133), .A2(n173), .ZN(n99) );
  NOR2_X1 U281 ( .A1(n189), .A2(n55), .ZN(n9) );
  INV_X1 U282 ( .A(n56), .ZN(n131) );
  INV_X1 U283 ( .A(n68), .ZN(n137) );
  NOR2_X1 U284 ( .A1(n137), .A2(n169), .ZN(n121) );
  NAND2_X1 U285 ( .A1(n137), .A2(n169), .ZN(n122) );
  OAI21_X1 U286 ( .B1(n123), .B2(n121), .A(n122), .ZN(n120) );
  OAI21_X1 U287 ( .B1(n84), .B2(n92), .A(n85), .ZN(n83) );
  NAND2_X1 U288 ( .A1(n60), .A2(n59), .ZN(n24) );
  INV_X1 U289 ( .A(n60), .ZN(n133) );
  OAI21_X1 U290 ( .B1(n110), .B2(n118), .A(n111), .ZN(n109) );
  NOR2_X1 U291 ( .A1(n110), .A2(n117), .ZN(n108) );
  INV_X1 U292 ( .A(n177), .ZN(n128) );
  NAND2_X1 U293 ( .A1(n62), .A2(n61), .ZN(n31) );
  INV_X1 U294 ( .A(n62), .ZN(n134) );
  OAI21_X1 U295 ( .B1(n190), .B2(n87), .A(n88), .ZN(n86) );
  OAI21_X1 U296 ( .B1(n190), .B2(n94), .A(n95), .ZN(n93) );
  OAI21_X1 U297 ( .B1(n190), .B2(n105), .A(n185), .ZN(n100) );
  OAI21_X1 U298 ( .B1(n80), .B2(n71), .A(n81), .ZN(n79) );
endmodule


module DW_fp_addsub_inst_DW_leftsh_7 ( A, SH, B );
  input [26:0] A;
  input [5:0] SH;
  output [26:0] B;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n32, n33, n34,
         n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48,
         n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n67, n68, n69,
         n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83,
         n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n110, n111, n112,
         n113, n114, n115, n116, n117, n118, n119, n120, n121, n122, n123,
         n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134,
         n135, n136, n137, n164, n165, n166, n167, n168, n169, n170, n171,
         n172, n173, n174, n175, n176, n177, n178, n179, n180, n182, n184,
         n185, n186, n187, n188, n189, n190, n253, n254, n255, n256, n257,
         n258, n259, n260, n261, n262, n263, n264, n265, n266, n267, n268,
         n269, n270, n271, n272, n273, n274, n275, n276, n277, n278, n279,
         n280, n281, n282, n283, n284, n285, n286, n287, n288, n289, n290,
         n291, n292, n293, n294, n295, n296, n297, n298, n299, n300, n301,
         n302, n303, n304, n305, n306, n307, n308, n309, n310, n311, n312,
         n313, n314, n315, n316, n317, n318, n319, n320, n321, n322, n323;

  NAND2_X1 U218 ( .A1(n92), .A2(n308), .ZN(n253) );
  NAND2_X1 U219 ( .A1(n88), .A2(SH[2]), .ZN(n254) );
  NAND2_X1 U220 ( .A1(n253), .A2(n254), .ZN(n57) );
  CLKBUF_X1 U221 ( .A(n309), .Z(n255) );
  AND2_X1 U222 ( .A1(n299), .A2(n315), .ZN(n256) );
  MUX2_X1 U223 ( .A(n57), .B(n55), .S(SH[1]), .Z(n270) );
  AND2_X1 U224 ( .A1(n175), .A2(n289), .ZN(n121) );
  BUF_X1 U225 ( .A(SH[0]), .Z(n322) );
  BUF_X2 U226 ( .A(SH[3]), .Z(n259) );
  BUF_X1 U227 ( .A(SH[0]), .Z(n323) );
  CLKBUF_X1 U228 ( .A(SH[3]), .Z(n257) );
  BUF_X2 U229 ( .A(SH[3]), .Z(n258) );
  CLKBUF_X1 U230 ( .A(n310), .Z(n263) );
  NAND2_X1 U231 ( .A1(n134), .A2(n260), .ZN(n261) );
  NAND2_X1 U232 ( .A1(n126), .A2(n259), .ZN(n262) );
  NAND2_X1 U233 ( .A1(n261), .A2(n262), .ZN(n91) );
  INV_X1 U234 ( .A(n258), .ZN(n260) );
  INV_X1 U235 ( .A(SH[1]), .ZN(n321) );
  NAND2_X1 U236 ( .A1(n263), .A2(n255), .ZN(n264) );
  CLKBUF_X1 U237 ( .A(n130), .Z(n265) );
  NAND2_X1 U238 ( .A1(n133), .A2(n260), .ZN(n266) );
  NAND2_X1 U239 ( .A1(n125), .A2(n259), .ZN(n267) );
  NAND2_X1 U240 ( .A1(n266), .A2(n267), .ZN(n90) );
  CLKBUF_X1 U241 ( .A(n129), .Z(n268) );
  AND2_X1 U242 ( .A1(n257), .A2(n299), .ZN(n269) );
  MUX2_X1 U243 ( .A(n130), .B(n122), .S(n259), .Z(n271) );
  NAND2_X1 U244 ( .A1(n184), .A2(n272), .ZN(n273) );
  NAND2_X1 U245 ( .A1(n168), .A2(SH[4]), .ZN(n274) );
  NAND2_X1 U246 ( .A1(n273), .A2(n274), .ZN(n130) );
  INV_X1 U247 ( .A(SH[4]), .ZN(n272) );
  NAND2_X1 U248 ( .A1(n129), .A2(n275), .ZN(n276) );
  NAND2_X1 U249 ( .A1(n121), .A2(n259), .ZN(n277) );
  NAND2_X1 U250 ( .A1(n276), .A2(n277), .ZN(n278) );
  INV_X1 U251 ( .A(n258), .ZN(n275) );
  NAND2_X1 U252 ( .A1(n56), .A2(n279), .ZN(n280) );
  NAND2_X1 U253 ( .A1(n58), .A2(n321), .ZN(n281) );
  NAND2_X1 U254 ( .A1(n281), .A2(n280), .ZN(n27) );
  INV_X1 U255 ( .A(n321), .ZN(n279) );
  NAND2_X1 U256 ( .A1(n27), .A2(n282), .ZN(n283) );
  NAND2_X1 U257 ( .A1(n270), .A2(n322), .ZN(n284) );
  NAND2_X1 U258 ( .A1(n283), .A2(n284), .ZN(B[26]) );
  INV_X1 U259 ( .A(n322), .ZN(n282) );
  AND2_X1 U260 ( .A1(n67), .A2(n318), .ZN(n32) );
  BUF_X1 U261 ( .A(n137), .Z(n313) );
  NAND2_X1 U262 ( .A1(n185), .A2(n285), .ZN(n286) );
  NAND2_X1 U263 ( .A1(n169), .A2(SH[4]), .ZN(n287) );
  NAND2_X1 U264 ( .A1(n286), .A2(n287), .ZN(n131) );
  INV_X1 U265 ( .A(SH[4]), .ZN(n285) );
  AND2_X1 U266 ( .A1(n313), .A2(n272), .ZN(n288) );
  BUF_X2 U267 ( .A(n137), .Z(n315) );
  INV_X1 U268 ( .A(n321), .ZN(n319) );
  NAND2_X1 U269 ( .A1(n256), .A2(A[19]), .ZN(n290) );
  NAND2_X1 U270 ( .A1(n167), .A2(SH[4]), .ZN(n291) );
  NAND2_X1 U271 ( .A1(n290), .A2(n291), .ZN(n129) );
  INV_X1 U272 ( .A(SH[4]), .ZN(n289) );
  CLKBUF_X1 U273 ( .A(n56), .Z(n292) );
  BUF_X2 U274 ( .A(n137), .Z(n314) );
  CLKBUF_X1 U275 ( .A(n89), .Z(n293) );
  MUX2_X1 U276 ( .A(n57), .B(n264), .S(SH[1]), .Z(n26) );
  NAND2_X1 U277 ( .A1(n132), .A2(n260), .ZN(n294) );
  NAND2_X1 U278 ( .A1(n124), .A2(n258), .ZN(n295) );
  NAND2_X1 U279 ( .A1(n295), .A2(n294), .ZN(n89) );
  NAND2_X1 U280 ( .A1(n91), .A2(n296), .ZN(n297) );
  NAND2_X1 U281 ( .A1(n271), .A2(n316), .ZN(n298) );
  NAND2_X1 U282 ( .A1(n298), .A2(n297), .ZN(n56) );
  INV_X1 U283 ( .A(n316), .ZN(n296) );
  NAND2_X1 U284 ( .A1(n189), .A2(n285), .ZN(n300) );
  NAND2_X1 U285 ( .A1(n173), .A2(SH[4]), .ZN(n301) );
  NAND2_X1 U286 ( .A1(n300), .A2(n301), .ZN(n135) );
  INV_X1 U287 ( .A(SH[4]), .ZN(n299) );
  NAND2_X1 U288 ( .A1(n288), .A2(A[17]), .ZN(n302) );
  NAND2_X1 U289 ( .A1(n165), .A2(SH[4]), .ZN(n303) );
  NAND2_X1 U290 ( .A1(n303), .A2(n302), .ZN(n127) );
  NAND2_X1 U291 ( .A1(n136), .A2(n260), .ZN(n304) );
  NAND2_X1 U292 ( .A1(n128), .A2(n258), .ZN(n305) );
  NAND2_X1 U293 ( .A1(n304), .A2(n305), .ZN(n93) );
  NAND2_X1 U294 ( .A1(n131), .A2(n275), .ZN(n306) );
  NAND2_X1 U295 ( .A1(n269), .A2(n177), .ZN(n307) );
  NAND2_X1 U296 ( .A1(n306), .A2(n307), .ZN(n88) );
  NAND2_X1 U297 ( .A1(n90), .A2(n308), .ZN(n309) );
  NAND2_X1 U298 ( .A1(n278), .A2(n316), .ZN(n310) );
  NAND2_X1 U299 ( .A1(n310), .A2(n309), .ZN(n55) );
  INV_X1 U300 ( .A(n316), .ZN(n308) );
  NAND2_X1 U301 ( .A1(n135), .A2(n275), .ZN(n311) );
  NAND2_X1 U302 ( .A1(n127), .A2(n259), .ZN(n312) );
  NAND2_X1 U303 ( .A1(n312), .A2(n311), .ZN(n92) );
  INV_X1 U304 ( .A(n318), .ZN(n317) );
  MUX2_X1 U305 ( .A(n35), .B(n33), .S(n320), .Z(n4) );
  MUX2_X1 U306 ( .A(n38), .B(n36), .S(n320), .Z(n7) );
  MUX2_X1 U307 ( .A(n37), .B(n35), .S(n320), .Z(n6) );
  MUX2_X1 U308 ( .A(n36), .B(n34), .S(n320), .Z(n5) );
  MUX2_X1 U309 ( .A(n42), .B(n40), .S(n320), .Z(n11) );
  MUX2_X1 U310 ( .A(n40), .B(n38), .S(n320), .Z(n9) );
  INV_X1 U311 ( .A(n321), .ZN(n320) );
  MUX2_X1 U312 ( .A(n11), .B(n10), .S(n323), .Z(B[10]) );
  MUX2_X1 U313 ( .A(n21), .B(n20), .S(n322), .Z(B[20]) );
  MUX2_X1 U314 ( .A(n22), .B(n21), .S(n322), .Z(B[21]) );
  MUX2_X1 U315 ( .A(n7), .B(n6), .S(n323), .Z(B[6]) );
  MUX2_X1 U316 ( .A(n20), .B(n19), .S(n322), .Z(B[19]) );
  MUX2_X1 U317 ( .A(n13), .B(n12), .S(n323), .Z(B[12]) );
  MUX2_X1 U318 ( .A(n8), .B(n7), .S(n323), .Z(B[7]) );
  MUX2_X1 U319 ( .A(n14), .B(n13), .S(n323), .Z(B[13]) );
  MUX2_X1 U320 ( .A(n19), .B(n18), .S(n322), .Z(B[18]) );
  MUX2_X1 U321 ( .A(n18), .B(n17), .S(n322), .Z(B[17]) );
  MUX2_X1 U322 ( .A(n17), .B(n16), .S(n322), .Z(B[16]) );
  MUX2_X1 U323 ( .A(n16), .B(n15), .S(n322), .Z(B[15]) );
  MUX2_X1 U324 ( .A(n15), .B(n14), .S(n323), .Z(B[14]) );
  MUX2_X1 U325 ( .A(n9), .B(n8), .S(n323), .Z(B[8]) );
  MUX2_X1 U326 ( .A(n10), .B(n9), .S(n323), .Z(B[9]) );
  MUX2_X1 U327 ( .A(n12), .B(n11), .S(n323), .Z(B[11]) );
  MUX2_X1 U328 ( .A(n44), .B(n42), .S(n320), .Z(n13) );
  MUX2_X1 U329 ( .A(n43), .B(n41), .S(n320), .Z(n12) );
  MUX2_X1 U330 ( .A(n45), .B(n43), .S(n320), .Z(n14) );
  MUX2_X1 U331 ( .A(n39), .B(n37), .S(n320), .Z(n8) );
  MUX2_X1 U332 ( .A(n46), .B(n44), .S(n320), .Z(n15) );
  MUX2_X1 U333 ( .A(n41), .B(n39), .S(n320), .Z(n10) );
  MUX2_X1 U334 ( .A(n50), .B(n48), .S(n319), .Z(n19) );
  MUX2_X1 U335 ( .A(n49), .B(n47), .S(n319), .Z(n18) );
  MUX2_X1 U336 ( .A(n48), .B(n46), .S(n319), .Z(n17) );
  MUX2_X1 U337 ( .A(n47), .B(n45), .S(n319), .Z(n16) );
  MUX2_X1 U338 ( .A(n32), .B(n34), .S(n321), .Z(n3) );
  MUX2_X1 U339 ( .A(n71), .B(n67), .S(n317), .Z(n36) );
  MUX2_X1 U340 ( .A(n72), .B(n68), .S(n317), .Z(n37) );
  MUX2_X1 U341 ( .A(n73), .B(n69), .S(n317), .Z(n38) );
  MUX2_X1 U342 ( .A(n77), .B(n73), .S(n317), .Z(n42) );
  MUX2_X1 U343 ( .A(n76), .B(n72), .S(n317), .Z(n41) );
  MUX2_X1 U344 ( .A(n75), .B(n71), .S(n317), .Z(n40) );
  MUX2_X1 U345 ( .A(n6), .B(n5), .S(n323), .Z(B[5]) );
  MUX2_X1 U346 ( .A(n4), .B(n3), .S(n323), .Z(B[3]) );
  MUX2_X1 U347 ( .A(n5), .B(n4), .S(n323), .Z(B[4]) );
  AND2_X1 U348 ( .A1(n68), .A2(n296), .ZN(n33) );
  AND2_X1 U349 ( .A1(n70), .A2(n296), .ZN(n35) );
  AND2_X1 U350 ( .A1(n69), .A2(n296), .ZN(n34) );
  INV_X1 U351 ( .A(n259), .ZN(n59) );
  MUX2_X1 U352 ( .A(n23), .B(n22), .S(n322), .Z(B[22]) );
  MUX2_X1 U353 ( .A(n120), .B(n112), .S(n259), .Z(n77) );
  MUX2_X1 U354 ( .A(n119), .B(n111), .S(n258), .Z(n76) );
  MUX2_X1 U355 ( .A(n118), .B(n110), .S(n259), .Z(n75) );
  MUX2_X1 U356 ( .A(n52), .B(n50), .S(n319), .Z(n21) );
  MUX2_X1 U357 ( .A(n53), .B(n51), .S(n319), .Z(n22) );
  MUX2_X1 U358 ( .A(n51), .B(n49), .S(n319), .Z(n20) );
  MUX2_X1 U359 ( .A(n78), .B(n74), .S(n317), .Z(n43) );
  MUX2_X1 U360 ( .A(n74), .B(n70), .S(n317), .Z(n39) );
  MUX2_X1 U361 ( .A(n85), .B(n81), .S(n316), .Z(n50) );
  MUX2_X1 U362 ( .A(n84), .B(n80), .S(n316), .Z(n49) );
  MUX2_X1 U363 ( .A(n83), .B(n79), .S(n316), .Z(n48) );
  MUX2_X1 U364 ( .A(n79), .B(n75), .S(n317), .Z(n44) );
  MUX2_X1 U365 ( .A(n82), .B(n78), .S(n316), .Z(n47) );
  MUX2_X1 U366 ( .A(n80), .B(n76), .S(n317), .Z(n45) );
  MUX2_X1 U367 ( .A(n81), .B(n77), .S(n317), .Z(n46) );
  AND2_X1 U368 ( .A1(n111), .A2(n59), .ZN(n68) );
  AND2_X1 U369 ( .A1(n110), .A2(n59), .ZN(n67) );
  AND2_X1 U370 ( .A1(n113), .A2(n59), .ZN(n70) );
  AND2_X1 U371 ( .A1(n112), .A2(n59), .ZN(n69) );
  AND2_X1 U372 ( .A1(n115), .A2(n59), .ZN(n72) );
  AND2_X1 U373 ( .A1(n116), .A2(n59), .ZN(n73) );
  AND2_X1 U374 ( .A1(n114), .A2(n59), .ZN(n71) );
  MUX2_X1 U375 ( .A(n3), .B(n2), .S(n323), .Z(B[2]) );
  AND2_X1 U376 ( .A1(n33), .A2(n321), .ZN(n2) );
  MUX2_X1 U377 ( .A(n24), .B(n23), .S(n322), .Z(B[23]) );
  MUX2_X1 U378 ( .A(n125), .B(n117), .S(n258), .Z(n82) );
  MUX2_X1 U379 ( .A(n124), .B(n116), .S(n259), .Z(n81) );
  MUX2_X1 U380 ( .A(n123), .B(n115), .S(n258), .Z(n80) );
  MUX2_X1 U381 ( .A(n122), .B(n114), .S(n258), .Z(n79) );
  MUX2_X1 U382 ( .A(n121), .B(n113), .S(n258), .Z(n78) );
  MUX2_X1 U383 ( .A(n128), .B(n120), .S(n258), .Z(n85) );
  MUX2_X1 U384 ( .A(n127), .B(n119), .S(n259), .Z(n84) );
  MUX2_X1 U385 ( .A(n54), .B(n52), .S(n319), .Z(n23) );
  MUX2_X1 U386 ( .A(n86), .B(n82), .S(n316), .Z(n51) );
  MUX2_X1 U387 ( .A(n25), .B(n24), .S(n322), .Z(B[24]) );
  MUX2_X1 U388 ( .A(n292), .B(n54), .S(n319), .Z(n25) );
  AND2_X1 U389 ( .A1(n165), .A2(n299), .ZN(n111) );
  AND2_X1 U390 ( .A1(n167), .A2(n289), .ZN(n113) );
  AND2_X1 U391 ( .A1(n164), .A2(n285), .ZN(n110) );
  AND2_X1 U392 ( .A1(n166), .A2(n289), .ZN(n112) );
  AND2_X1 U393 ( .A1(n170), .A2(n289), .ZN(n116) );
  AND2_X1 U394 ( .A1(n169), .A2(n299), .ZN(n115) );
  AND2_X1 U395 ( .A1(n168), .A2(n285), .ZN(n114) );
  AND2_X1 U396 ( .A1(n117), .A2(n59), .ZN(n74) );
  AND2_X1 U397 ( .A1(n172), .A2(n289), .ZN(n118) );
  MUX2_X1 U398 ( .A(n268), .B(n121), .S(n259), .Z(n86) );
  MUX2_X1 U399 ( .A(n265), .B(n122), .S(n258), .Z(n87) );
  MUX2_X1 U400 ( .A(n182), .B(n166), .S(SH[4]), .Z(n128) );
  MUX2_X1 U401 ( .A(n188), .B(n172), .S(SH[4]), .Z(n134) );
  AND2_X1 U402 ( .A1(n176), .A2(n289), .ZN(n122) );
  MUX2_X1 U403 ( .A(n264), .B(n53), .S(n319), .Z(n24) );
  AND2_X1 U404 ( .A1(n171), .A2(n289), .ZN(n117) );
  AND2_X1 U405 ( .A1(A[2]), .A2(n314), .ZN(n166) );
  AND2_X1 U406 ( .A1(n178), .A2(n289), .ZN(n124) );
  AND2_X1 U407 ( .A1(A[0]), .A2(n315), .ZN(n164) );
  AND2_X1 U408 ( .A1(A[1]), .A2(n313), .ZN(n165) );
  AND2_X1 U409 ( .A1(n177), .A2(n299), .ZN(n123) );
  MUX2_X1 U410 ( .A(n186), .B(n170), .S(SH[4]), .Z(n132) );
  AND2_X1 U411 ( .A1(n174), .A2(n299), .ZN(n120) );
  MUX2_X1 U412 ( .A(n190), .B(n174), .S(SH[4]), .Z(n136) );
  AND2_X1 U413 ( .A1(n173), .A2(n285), .ZN(n119) );
  AND2_X1 U414 ( .A1(A[15]), .A2(n315), .ZN(n179) );
  AND2_X1 U415 ( .A1(n315), .A2(A[14]), .ZN(n178) );
  AND2_X1 U416 ( .A1(A[13]), .A2(n314), .ZN(n177) );
  AND2_X1 U417 ( .A1(A[12]), .A2(n314), .ZN(n176) );
  AND2_X1 U418 ( .A1(A[11]), .A2(n314), .ZN(n175) );
  AND2_X1 U419 ( .A1(A[10]), .A2(n314), .ZN(n174) );
  AND2_X1 U420 ( .A1(A[6]), .A2(n315), .ZN(n170) );
  AND2_X1 U421 ( .A1(A[9]), .A2(n313), .ZN(n173) );
  AND2_X1 U422 ( .A1(A[8]), .A2(n314), .ZN(n172) );
  AND2_X1 U423 ( .A1(n313), .A2(A[7]), .ZN(n171) );
  AND2_X1 U424 ( .A1(A[5]), .A2(n315), .ZN(n169) );
  AND2_X1 U425 ( .A1(A[4]), .A2(n314), .ZN(n168) );
  AND2_X1 U426 ( .A1(A[3]), .A2(n314), .ZN(n167) );
  INV_X1 U427 ( .A(SH[5]), .ZN(n137) );
  AND2_X1 U428 ( .A1(n179), .A2(n285), .ZN(n125) );
  AND2_X1 U429 ( .A1(A[18]), .A2(n315), .ZN(n182) );
  MUX2_X1 U430 ( .A(n293), .B(n85), .S(n316), .Z(n54) );
  MUX2_X1 U431 ( .A(n93), .B(n89), .S(n316), .Z(n58) );
  AND2_X1 U432 ( .A1(A[22]), .A2(n314), .ZN(n186) );
  MUX2_X1 U433 ( .A(n187), .B(n171), .S(SH[4]), .Z(n133) );
  MUX2_X1 U434 ( .A(n88), .B(n84), .S(n316), .Z(n53) );
  AND2_X1 U435 ( .A1(A[16]), .A2(n315), .ZN(n180) );
  MUX2_X1 U436 ( .A(n26), .B(n25), .S(n322), .Z(B[25]) );
  MUX2_X1 U437 ( .A(n180), .B(n164), .S(SH[4]), .Z(n126) );
  MUX2_X1 U438 ( .A(n126), .B(n118), .S(n259), .Z(n83) );
  MUX2_X1 U439 ( .A(n87), .B(n83), .S(n316), .Z(n52) );
  AND2_X1 U440 ( .A1(A[20]), .A2(n315), .ZN(n184) );
  AND2_X1 U441 ( .A1(A[21]), .A2(n313), .ZN(n185) );
  AND2_X1 U442 ( .A1(A[26]), .A2(n314), .ZN(n190) );
  AND2_X1 U443 ( .A1(A[24]), .A2(n315), .ZN(n188) );
  AND2_X1 U444 ( .A1(A[25]), .A2(n315), .ZN(n189) );
  AND2_X1 U445 ( .A1(A[23]), .A2(n314), .ZN(n187) );
  INV_X2 U446 ( .A(n318), .ZN(n316) );
  INV_X1 U447 ( .A(SH[2]), .ZN(n318) );
endmodule


module DW_fp_addsub_inst ( inst_a, inst_b, inst_rnd, inst_op, z_inst, 
        status_inst );
  input [31:0] inst_a;
  input [31:0] inst_b;
  input [2:0] inst_rnd;
  output [31:0] z_inst;
  output [7:0] status_inst;
  input inst_op;
  wire   \U1/Elz[0] , \U1/Elz[1] , \U1/Elz[2] , \U1/Elz[3] , \U1/Elz[4] ,
         \U1/Elz[5] , \U1/Elz[6] , \U1/Elz[7] , \U1/Elz[8] , \U1/Elz_12 ,
         \U1/E1[7] , \U1/E1[6] , \U1/E1[5] , \U1/E1[4] , \U1/E1[3] ,
         \U1/E1[2] , \U1/E1[1] , \U1/E1[0] , \U1/frac1[22] , \U1/frac1[21] ,
         \U1/frac1[20] , \U1/frac1[19] , \U1/frac1[18] , \U1/frac1[17] ,
         \U1/frac1[16] , \U1/frac1[15] , \U1/frac1[14] , \U1/frac1[13] ,
         \U1/frac1[12] , \U1/frac1[11] , \U1/frac1[10] , \U1/frac1[9] ,
         \U1/frac1[8] , \U1/frac1[7] , \U1/frac1[6] , \U1/frac1[5] ,
         \U1/frac1[4] , \U1/frac1[3] , \U1/frac1[2] , \U1/frac1[1] ,
         \U1/frac1[0] , \U1/num_zeros_used[4] , \U1/num_zeros_used[3] ,
         \U1/num_zeros_used[2] , \U1/num_zeros_used[0] , \U1/a_norm[4] ,
         \U1/a_norm[5] , \U1/a_norm[6] , \U1/a_norm[7] , \U1/a_norm[8] ,
         \U1/a_norm[9] , \U1/a_norm[10] , \U1/a_norm[11] , \U1/a_norm[12] ,
         \U1/a_norm[13] , \U1/a_norm[14] , \U1/a_norm[15] , \U1/a_norm[16] ,
         \U1/a_norm[17] , \U1/a_norm[18] , \U1/a_norm[19] , \U1/a_norm[20] ,
         \U1/a_norm[21] , \U1/a_norm[22] , \U1/a_norm[23] , \U1/a_norm[24] ,
         \U1/a_norm[25] , \U1/a_norm[26] , \U1/num_zeros_path2[1] ,
         \U1/num_zeros_path2[0] , \U1/num_zeros_path1_adj[4] ,
         \U1/num_zeros_path1_adj[3] , \U1/num_zeros_path1_adj[2] ,
         \U1/num_zeros_path1_adj[1] , \U1/num_zeros_path1_adj[0] ,
         \U1/extra_shift_path1 , \U1/a_norm_partial_path1[3] ,
         \U1/a_norm_partial_path1[4] , \U1/a_norm_partial_path1[5] ,
         \U1/a_norm_partial_path1[6] , \U1/a_norm_partial_path1[7] ,
         \U1/a_norm_partial_path1[8] , \U1/a_norm_partial_path1[9] ,
         \U1/a_norm_partial_path1[10] , \U1/a_norm_partial_path1[11] ,
         \U1/a_norm_partial_path1[12] , \U1/a_norm_partial_path1[13] ,
         \U1/a_norm_partial_path1[14] , \U1/a_norm_partial_path1[15] ,
         \U1/a_norm_partial_path1[16] , \U1/a_norm_partial_path1[17] ,
         \U1/a_norm_partial_path1[18] , \U1/a_norm_partial_path1[19] ,
         \U1/a_norm_partial_path1[20] , \U1/a_norm_partial_path1[21] ,
         \U1/a_norm_partial_path1[22] , \U1/a_norm_partial_path1[23] ,
         \U1/a_norm_partial_path1[24] , \U1/a_norm_partial_path1[25] ,
         \U1/a_norm_partial_path1[26] , \U1/a_norm_partial_path1[27] ,
         \U1/num_zeros_path1[0] , \U1/num_zeros_path1[1] ,
         \U1/num_zeros_path1[2] , \U1/num_zeros_path1[3] ,
         \U1/num_zeros_path1[4] , \U1/num_zeros_path1[5] , \U1/fr[1] ,
         \U1/fr[2] , \U1/fr[3] , \U1/fr[4] , \U1/fr[5] , \U1/fr[6] ,
         \U1/fr[7] , \U1/fr[8] , \U1/fr[9] , \U1/fr[10] , \U1/fr[11] ,
         \U1/fr[12] , \U1/fr[13] , \U1/fr[14] , \U1/fr[15] , \U1/fr[16] ,
         \U1/fr[17] , \U1/fr[18] , \U1/fr[19] , \U1/fr[20] , \U1/fr[21] ,
         \U1/fr[22] , \U1/fr[23] , \U1/fr[24] , \U1/fr[25] , \U1/fr[26] ,
         \U1/adder_output[0] , \U1/adder_output[1] , \U1/adder_output[2] ,
         \U1/adder_output[3] , \U1/adder_output[4] , \U1/adder_output[5] ,
         \U1/adder_output[6] , \U1/adder_output[7] , \U1/adder_output[8] ,
         \U1/adder_output[9] , \U1/adder_output[10] , \U1/adder_output[11] ,
         \U1/adder_output[12] , \U1/adder_output[13] , \U1/adder_output[14] ,
         \U1/adder_output[15] , \U1/adder_output[16] , \U1/adder_output[17] ,
         \U1/adder_output[18] , \U1/adder_output[19] , \U1/adder_output[20] ,
         \U1/adder_output[21] , \U1/adder_output[22] , \U1/adder_output[23] ,
         \U1/adder_output[24] , \U1/adder_output[25] , \U1/adder_output[26] ,
         \U1/adder_output[27] , \U1/sig_aligned2[0] , \U1/sig_aligned2[1] ,
         \U1/sig_aligned2[2] , \U1/sig_aligned2[3] , \U1/sig_aligned2[4] ,
         \U1/sig_aligned2[5] , \U1/sig_aligned2[6] , \U1/sig_aligned2[7] ,
         \U1/sig_aligned2[8] , \U1/sig_aligned2[9] , \U1/sig_aligned2[10] ,
         \U1/sig_aligned2[11] , \U1/sig_aligned2[12] , \U1/sig_aligned2[13] ,
         \U1/sig_aligned2[14] , \U1/sig_aligned2[15] , \U1/sig_aligned2[16] ,
         \U1/sig_aligned2[17] , \U1/sig_aligned2[18] , \U1/sig_aligned2[19] ,
         \U1/sig_aligned2[20] , \U1/sig_aligned2[21] , \U1/sig_aligned2[22] ,
         \U1/sig_aligned2[23] , \U1/sig_aligned2[24] , \U1/sig_aligned2[25] ,
         \U1/sig_aligned2[26] , \U1/sig_small_shifted[26] ,
         \U1/sig_small_shifted[25] , \U1/sig_small_shifted[24] ,
         \U1/sig_small_shifted[23] , \U1/sig_small_shifted[22] ,
         \U1/sig_small_shifted[21] , \U1/sig_small_shifted[20] ,
         \U1/sig_small_shifted[19] , \U1/sig_small_shifted[18] ,
         \U1/sig_small_shifted[17] , \U1/sig_small_shifted[16] ,
         \U1/sig_small_shifted[15] , \U1/sig_small_shifted[14] ,
         \U1/sig_small_shifted[13] , \U1/sig_small_shifted[12] ,
         \U1/sig_small_shifted[11] , \U1/sig_small_shifted[10] ,
         \U1/sig_small_shifted[9] , \U1/sig_small_shifted[8] ,
         \U1/sig_small_shifted[7] , \U1/sig_small_shifted[6] ,
         \U1/sig_small_shifted[5] , \U1/sig_small_shifted[4] ,
         \U1/sig_small_shifted[3] , \U1/sig_small_shifted[2] ,
         \U1/sig_small_shifted[1] , \U1/mag_exp_diff[0] , \U1/mag_exp_diff[1] ,
         \U1/mag_exp_diff[3] , \U1/mag_exp_diff[4] , \U1/mag_exp_diff[5] ,
         \U1/mag_exp_diff[6] , \U1/mag_exp_diff[7] , \U1/N99 , \U1/N98 ,
         \U1/N97 , \U1/N96 , \U1/N95 , \U1/N94 , \U1/N93 , \U1/N92 ,
         \U1/ediff[0] , \U1/ediff[1] , \U1/ediff[2] , \U1/ediff[3] ,
         \U1/ediff[4] , \U1/ediff[5] , \U1/ediff[6] , \U1/ediff[7] ,
         \U1/ediff[8] , \U1/small_p[1] , \U1/small_p[5] , \U1/small_p[11] ,
         \U1/small_p[15] , \U1/small_p[20] , \U1/large_p[0] , \U1/large_p[1] ,
         \U1/large_p[2] , \U1/large_p[3] , \U1/large_p[4] , \U1/large_p[5] ,
         \U1/large_p[6] , \U1/large_p[7] , \U1/large_p[8] , \U1/large_p[9] ,
         \U1/large_p[10] , \U1/large_p[11] , \U1/large_p[12] ,
         \U1/large_p[13] , \U1/large_p[14] , \U1/large_p[15] ,
         \U1/large_p[16] , \U1/large_p[17] , \U1/large_p[18] ,
         \U1/large_p[19] , \U1/large_p[20] , \U1/large_p[21] ,
         \U1/large_p[22] , \U1/large_p[23] , \U1/large_p[24] ,
         \U1/large_p[25] , \U1/large_p[26] , \U1/large_p[27] ,
         \U1/large_p[28] , \U1/large_p[29] , \U1/large_p[30] , \U1/swap ,
         \U1/U1/num_of_zeros[1] , n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11,
         n12, n13, n14, n15, n16, n18, n19, n20, n21, n22, n23, n24, n25, n26,
         n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40,
         n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54,
         n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68,
         n69, n70, n71, n72, n73, n75, n76, n77, n78, n79, n80, n81, n82, n83,
         n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97,
         n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108, n109,
         n110, n111, n112, n113, n114, n115, n116, n117, n118, n119, n121,
         n122, n123, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n202, n203, n204, n205, n206, n207, n208, n209, n210,
         n211, n212, n213, n214, n215, n216, n217, n218, n219, n220, n221,
         n222, n223, n224, n225, n226, n227, n228, n229, n230, n231, n232,
         n233, n234, n235, n236, n237, n238, n239, n240, n241, n242, n243,
         n244, n245, n246, n247, n248, n249, n250, n251, n252, n253, n254,
         n255, n256, n257, n258, n259, n260, n261, n262, n263, n264, n265,
         n266, n267, n268, n269, n270, n271, n272, n273, n274, n275, n276,
         n277, n278, n279, n280, n281, n282, n283, n284, n285, n286, n287,
         n288, n289, n290, n291, n292, n293, n294, n295, n296, n297, n298,
         n299, n300, n301, n302, n303, n304, n305, n306, n307, n308, n309,
         n310, n311, n312, n313, n314, n315, n316, n317, n318, n319, n320,
         n321, n322, n323, n324, n325, n326, n327, n328, n329, n330, n331,
         n332, n333, n334, n335, n336, n337, n338, n339, n340, n341, n342,
         n343, n344, n345, n346, n347, n348, n349, n350, n351, n352, n353,
         n354, n355, n356, n357, n358, n359, n360, n361, n362, n363, n364,
         n365, n366, n367, n368, n369, n370, n371, n372, n373, n374, n375,
         n376, n377, n378, n379, n380, n381, n382, n383, n384, n385, n386,
         n387, n388, n389, n390, n391, n392, n393, n394, n395, n396, n397,
         n398, n399, n400, n401, n402, n403, n404, n405, n406, n407, n408,
         n409, n410, n411, n412, n413, n414, n415, n416, n417, n418, n419,
         n420, n421, n422, n423, n424, n425, n426, n427, n428, n429, n430,
         n431, n432, n433, n434, n435, n436, n437, n438, n439, n440, n441,
         n442, n443, n444, n445, n446, n447, n448, n449, n450, n451, n452,
         n453, n454, n455, n456, n457, n458, n459, n460, n461, n462, n463,
         n464, n465, n466, n467, n468, n469, n470, n471, n472, n473, n474,
         n475, n476, n477, n478, n479, n480, n481, n482, n483, n484, n485,
         n486, n487, n488, n489, n490, n491, n492, n493, n494, n495, n496,
         n497, n498, n499, n500, n501, n502, n503, n504, n505, n506, n507,
         n508, n509, n510, n511, n512, n513, n514, n515, n516, n517, n518,
         n519, n520, n521, n522, n523, n524, n525, n526, n527, n528, n529,
         n530, n531, n532, n533, n534, n535, n536, n537, n538, n539, n540,
         n541, n542, n543, n544, n545, n546, n547, n548, n549, n550, n551,
         n552, n553, n554, n555, n556, n557, n558, n559, n560, n561, n562,
         n563, n564, n565, n566, n567, n568, n569, n570, n571, n572, n573,
         n574, n575, n576, n577, n578, n579, n580, n581, n582, n583, n584,
         n585, n586, n587, n588, n589, n590, n591, n592, n593, n594, n595,
         n596, n597, n598, n599, n600, n601, n602, n603, n604, n605, n606,
         n607, n608, n609, n610, n611, n612, n613, n614, n615, n616, n617,
         n618, n619, n620, n621, n622, n623, n624, n625, n626, n627, n628,
         n629, n630, n631, n632, n633, n634, n635, n636, n637, n638, n639,
         n640, n641, n642, n643, n644, n645, n646, n647, n648, n649, n650,
         n651, n652, n653, n654, n655, n656, n657, n658, n659, n660, n661,
         n662, n663, n664, n665, n666, n667, n668, n669, n670, n671, n672,
         n673, n674, n675, n676, n677, n678, n679, n680, n681, n682, n683,
         n684, n685, n686, n687, n688, n689, n690, n691, n692, n693, n694,
         n695, n697;
  wire   SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, SYNOPSYS_UNCONNECTED__5, 
        SYNOPSYS_UNCONNECTED__6, SYNOPSYS_UNCONNECTED__7;
  assign status_inst[7] = 1'b0;
  assign status_inst[6] = 1'b0;

  DW_fp_addsub_inst_DW_lzd_1 \U1/U2  ( .a({1'b0, \U1/fr[26] , \U1/fr[25] , 
        \U1/fr[24] , \U1/fr[23] , \U1/fr[22] , \U1/fr[21] , \U1/fr[20] , 
        \U1/fr[19] , \U1/fr[18] , \U1/fr[17] , \U1/fr[16] , \U1/fr[15] , 
        \U1/fr[14] , \U1/fr[13] , \U1/fr[12] , \U1/fr[11] , \U1/fr[10] , 
        \U1/fr[9] , \U1/fr[8] , \U1/fr[7] , \U1/fr[6] , \U1/fr[5] , \U1/fr[4] , 
        \U1/fr[3] , \U1/fr[2] , \U1/fr[1] , n198}), .enc({
        \U1/num_zeros_path1[5] , \U1/num_zeros_path1[4] , 
        \U1/num_zeros_path1[3] , \U1/num_zeros_path1[2] , 
        \U1/num_zeros_path1[1] , \U1/num_zeros_path1[0] }) );
  DW_fp_addsub_inst_DW01_add_4 \U1/add_1_root_add_273_2  ( .A({1'b0, 1'b1, 
        \U1/large_p[22] , \U1/large_p[21] , \U1/large_p[20] , \U1/large_p[19] , 
        \U1/large_p[18] , \U1/large_p[17] , \U1/large_p[16] , \U1/large_p[15] , 
        \U1/large_p[14] , \U1/large_p[13] , \U1/large_p[12] , \U1/large_p[11] , 
        \U1/large_p[10] , \U1/large_p[9] , \U1/large_p[8] , \U1/large_p[7] , 
        \U1/large_p[6] , \U1/large_p[5] , \U1/large_p[4] , \U1/large_p[3] , 
        \U1/large_p[2] , \U1/large_p[1] , \U1/large_p[0] , 1'b0, 1'b0, 1'b0}), 
        .B({n697, \U1/sig_aligned2[26] , \U1/sig_aligned2[25] , 
        \U1/sig_aligned2[24] , \U1/sig_aligned2[23] , \U1/sig_aligned2[22] , 
        \U1/sig_aligned2[21] , \U1/sig_aligned2[20] , \U1/sig_aligned2[19] , 
        \U1/sig_aligned2[18] , \U1/sig_aligned2[17] , \U1/sig_aligned2[16] , 
        \U1/sig_aligned2[15] , \U1/sig_aligned2[14] , \U1/sig_aligned2[13] , 
        \U1/sig_aligned2[12] , \U1/sig_aligned2[11] , \U1/sig_aligned2[10] , 
        \U1/sig_aligned2[9] , \U1/sig_aligned2[8] , \U1/sig_aligned2[7] , 
        \U1/sig_aligned2[6] , \U1/sig_aligned2[5] , \U1/sig_aligned2[4] , 
        \U1/sig_aligned2[3] , \U1/sig_aligned2[2] , \U1/sig_aligned2[1] , 
        \U1/sig_aligned2[0] }), .CI(n697), .SUM({\U1/adder_output[27] , 
        \U1/adder_output[26] , \U1/adder_output[25] , \U1/adder_output[24] , 
        \U1/adder_output[23] , \U1/adder_output[22] , \U1/adder_output[21] , 
        \U1/adder_output[20] , \U1/adder_output[19] , \U1/adder_output[18] , 
        \U1/adder_output[17] , \U1/adder_output[16] , \U1/adder_output[15] , 
        \U1/adder_output[14] , \U1/adder_output[13] , \U1/adder_output[12] , 
        \U1/adder_output[11] , \U1/adder_output[10] , \U1/adder_output[9] , 
        \U1/adder_output[8] , \U1/adder_output[7] , \U1/adder_output[6] , 
        \U1/adder_output[5] , \U1/adder_output[4] , \U1/adder_output[3] , 
        \U1/adder_output[2] , \U1/adder_output[1] , \U1/adder_output[0] }) );
  DW_fp_addsub_inst_DW01_inc_2 \U1/add_365  ( .A({\U1/a_norm[26] , 
        \U1/a_norm[25] , \U1/a_norm[24] , \U1/a_norm[23] , \U1/a_norm[22] , 
        \U1/a_norm[21] , \U1/a_norm[20] , \U1/a_norm[19] , \U1/a_norm[18] , 
        \U1/a_norm[17] , \U1/a_norm[16] , \U1/a_norm[15] , \U1/a_norm[14] , 
        \U1/a_norm[13] , \U1/a_norm[12] , \U1/a_norm[11] , \U1/a_norm[10] , 
        \U1/a_norm[9] , \U1/a_norm[8] , \U1/a_norm[7] , \U1/a_norm[6] , 
        \U1/a_norm[5] , \U1/a_norm[4] }), .SUM({\U1/frac1[22] , \U1/frac1[21] , 
        \U1/frac1[20] , \U1/frac1[19] , \U1/frac1[18] , \U1/frac1[17] , 
        \U1/frac1[16] , \U1/frac1[15] , \U1/frac1[14] , \U1/frac1[13] , 
        \U1/frac1[12] , \U1/frac1[11] , \U1/frac1[10] , \U1/frac1[9] , 
        \U1/frac1[8] , \U1/frac1[7] , \U1/frac1[6] , \U1/frac1[5] , 
        \U1/frac1[4] , \U1/frac1[3] , \U1/frac1[2] , \U1/frac1[1] , 
        \U1/frac1[0] }) );
  DW_fp_addsub_inst_DW01_add_5 \U1/add_317  ( .A({\U1/num_zeros_path1[4] , 
        \U1/num_zeros_path1[3] , \U1/num_zeros_path1[2] , 
        \U1/num_zeros_path1[1] , \U1/num_zeros_path1[0] }), .B({1'b0, 1'b0, 
        1'b0, 1'b0, \U1/extra_shift_path1 }), .CI(1'b0), .SUM({
        \U1/num_zeros_path1_adj[4] , \U1/num_zeros_path1_adj[3] , 
        \U1/num_zeros_path1_adj[2] , \U1/num_zeros_path1_adj[1] , 
        \U1/num_zeros_path1_adj[0] }) );
  DW_fp_addsub_inst_DW_cmp_2 \U1/lt_203  ( .A(inst_a[30:0]), .B({n66, 
        inst_b[29], n151, inst_b[27], n194, n193, n141, inst_b[23:0]}), .TC(
        1'b0), .GE_LT(1'b1), .GE_GT_EQ(1'b0), .GE_LT_GT_LE(\U1/swap ) );
  DW_fp_addsub_inst_DW_rightsh_2 \U1/srl_262_lsb_trim  ( .A({1'b1, n163, n56, 
        \U1/small_p[20] , n26, n48, n112, n86, \U1/small_p[15] , n82, n38, n87, 
        \U1/small_p[11] , n52, n105, n85, n89, n39, \U1/small_p[5] , n67, n32, 
        n8, \U1/small_p[1] , n61, 1'b0, 1'b0}), .DATA_TC(1'b0), .SH({
        \U1/mag_exp_diff[7] , \U1/mag_exp_diff[6] , \U1/mag_exp_diff[5] , 
        \U1/mag_exp_diff[4] , n189, n25, \U1/mag_exp_diff[1] , 
        \U1/mag_exp_diff[0] }), .B({\U1/sig_small_shifted[26] , 
        \U1/sig_small_shifted[25] , \U1/sig_small_shifted[24] , 
        \U1/sig_small_shifted[23] , \U1/sig_small_shifted[22] , 
        \U1/sig_small_shifted[21] , \U1/sig_small_shifted[20] , 
        \U1/sig_small_shifted[19] , \U1/sig_small_shifted[18] , 
        \U1/sig_small_shifted[17] , \U1/sig_small_shifted[16] , 
        \U1/sig_small_shifted[15] , \U1/sig_small_shifted[14] , 
        \U1/sig_small_shifted[13] , \U1/sig_small_shifted[12] , 
        \U1/sig_small_shifted[11] , \U1/sig_small_shifted[10] , 
        \U1/sig_small_shifted[9] , \U1/sig_small_shifted[8] , 
        \U1/sig_small_shifted[7] , \U1/sig_small_shifted[6] , 
        \U1/sig_small_shifted[5] , \U1/sig_small_shifted[4] , 
        \U1/sig_small_shifted[3] , \U1/sig_small_shifted[2] , 
        \U1/sig_small_shifted[1] }) );
  DW_fp_addsub_inst_DW01_add_8 \U1/U1/add_120  ( .A({1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0}), .B({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        \U1/U1/num_of_zeros[1] , n694}), .CI(1'b0), .SUM({
        SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, \U1/num_zeros_path2[1] , 
        \U1/num_zeros_path2[0] }) );
  DW_fp_addsub_inst_add_369_DP_OP_291_9752_4 \U1/add_369_DP_OP_291_9752_8  ( 
        .I1({\U1/large_p[30] , \U1/large_p[29] , \U1/large_p[28] , 
        \U1/large_p[27] , \U1/large_p[26] , \U1/large_p[25] , \U1/large_p[24] , 
        \U1/large_p[23] }), .I2({\U1/num_zeros_used[4] , 
        \U1/num_zeros_used[3] , \U1/num_zeros_used[2] , n695, 
        \U1/num_zeros_used[0] }), .O6({SYNOPSYS_UNCONNECTED__5, \U1/E1[7] , 
        \U1/E1[6] , \U1/E1[5] , \U1/E1[4] , \U1/E1[3] , \U1/E1[2] , \U1/E1[1] , 
        \U1/E1[0] }), .O2({\U1/Elz_12 , \U1/Elz[8] , \U1/Elz[7] , \U1/Elz[6] , 
        \U1/Elz[5] , \U1/Elz[4] , \U1/Elz[3] , \U1/Elz[2] , \U1/Elz[1] , 
        \U1/Elz[0] }) );
  DW_fp_addsub_inst_sub_256_DP_OP_292_7885_7 \U1/sub_256_DP_OP_292_7885_7  ( 
        .I1({inst_a[30], n195, inst_a[28:23]}), .I2(inst_b[30:23]), .O11({
        \U1/ediff[8] , \U1/ediff[7] , \U1/ediff[6] , \U1/ediff[5] , 
        \U1/ediff[4] , \U1/ediff[3] , \U1/ediff[2] , \U1/ediff[1] , 
        \U1/ediff[0] }), .O9({\U1/N99 , \U1/N98 , \U1/N97 , \U1/N96 , \U1/N95 , 
        \U1/N94 , \U1/N93 , \U1/N92 }) );
  DW_fp_addsub_inst_DW_leftsh_7 \U1/sll_315  ( .A({\U1/adder_output[27] , 
        \U1/adder_output[26] , \U1/adder_output[25] , \U1/adder_output[24] , 
        \U1/adder_output[23] , \U1/adder_output[22] , \U1/adder_output[21] , 
        \U1/adder_output[20] , \U1/adder_output[19] , \U1/adder_output[18] , 
        \U1/adder_output[17] , \U1/adder_output[16] , \U1/adder_output[15] , 
        \U1/adder_output[14] , \U1/adder_output[13] , \U1/adder_output[12] , 
        \U1/adder_output[11] , \U1/adder_output[10] , \U1/adder_output[9] , 
        \U1/adder_output[8] , \U1/adder_output[7] , \U1/adder_output[6] , 
        \U1/adder_output[5] , \U1/adder_output[4] , \U1/adder_output[3] , 
        \U1/adder_output[2] , \U1/adder_output[1] }), .SH({
        \U1/num_zeros_path1[5] , \U1/num_zeros_path1[4] , 
        \U1/num_zeros_path1[3] , \U1/num_zeros_path1[2] , 
        \U1/num_zeros_path1[1] , \U1/num_zeros_path1[0] }), .B({
        \U1/a_norm_partial_path1[27] , \U1/a_norm_partial_path1[26] , 
        \U1/a_norm_partial_path1[25] , \U1/a_norm_partial_path1[24] , 
        \U1/a_norm_partial_path1[23] , \U1/a_norm_partial_path1[22] , 
        \U1/a_norm_partial_path1[21] , \U1/a_norm_partial_path1[20] , 
        \U1/a_norm_partial_path1[19] , \U1/a_norm_partial_path1[18] , 
        \U1/a_norm_partial_path1[17] , \U1/a_norm_partial_path1[16] , 
        \U1/a_norm_partial_path1[15] , \U1/a_norm_partial_path1[14] , 
        \U1/a_norm_partial_path1[13] , \U1/a_norm_partial_path1[12] , 
        \U1/a_norm_partial_path1[11] , \U1/a_norm_partial_path1[10] , 
        \U1/a_norm_partial_path1[9] , \U1/a_norm_partial_path1[8] , 
        \U1/a_norm_partial_path1[7] , \U1/a_norm_partial_path1[6] , 
        \U1/a_norm_partial_path1[5] , \U1/a_norm_partial_path1[4] , 
        \U1/a_norm_partial_path1[3] , SYNOPSYS_UNCONNECTED__6, 
        SYNOPSYS_UNCONNECTED__7}) );
  AND2_X2 U1 ( .A1(\U1/num_zeros_path1_adj[4] ), .A2(n202), .ZN(
        \U1/num_zeros_used[4] ) );
  INV_X1 U2 ( .A(n113), .ZN(n1) );
  BUF_X2 U3 ( .A(\U1/a_norm_partial_path1[27] ), .Z(n113) );
  INV_X2 U4 ( .A(n10), .ZN(n610) );
  BUF_X1 U5 ( .A(n499), .Z(n2) );
  NAND2_X1 U6 ( .A1(n528), .A2(n529), .ZN(n499) );
  BUF_X1 U7 ( .A(n528), .Z(n3) );
  AND2_X2 U8 ( .A1(n2), .A2(n501), .ZN(n4) );
  CLKBUF_X1 U9 ( .A(n618), .Z(n5) );
  OAI21_X1 U10 ( .B1(n529), .B2(n646), .A(n3), .ZN(n6) );
  AND2_X1 U11 ( .A1(\U1/Elz_12 ), .A2(n656), .ZN(n7) );
  AND3_X1 U12 ( .A1(n663), .A2(n658), .A3(\U1/Elz[5] ), .ZN(n676) );
  MUX2_X1 U13 ( .A(inst_b[2]), .B(inst_a[2]), .S(n208), .Z(n8) );
  CLKBUF_X3 U14 ( .A(n617), .Z(n202) );
  BUF_X1 U15 ( .A(n209), .Z(n180) );
  CLKBUF_X1 U16 ( .A(n210), .Z(n187) );
  CLKBUF_X1 U17 ( .A(n48), .Z(n44) );
  OR2_X1 U18 ( .A1(n230), .A2(n229), .ZN(n9) );
  NAND2_X1 U19 ( .A1(n9), .A2(n622), .ZN(n154) );
  AND2_X1 U20 ( .A1(n1), .A2(n201), .ZN(n10) );
  NOR2_X2 U21 ( .A1(\U1/Elz[7] ), .A2(\U1/Elz[8] ), .ZN(n170) );
  BUF_X2 U22 ( .A(n123), .Z(n206) );
  OAI222_X1 U23 ( .A1(n608), .A2(n597), .B1(n610), .B2(n599), .C1(n202), .C2(
        n596), .ZN(\U1/a_norm[12] ) );
  NAND2_X1 U24 ( .A1(n657), .A2(n687), .ZN(n681) );
  OR2_X1 U25 ( .A1(\U1/Elz[0] ), .A2(\U1/Elz[1] ), .ZN(n11) );
  AND2_X1 U26 ( .A1(n622), .A2(n9), .ZN(n12) );
  AND2_X1 U27 ( .A1(\U1/num_zeros_path2[1] ), .A2(n482), .ZN(n13) );
  OR2_X1 U28 ( .A1(status_inst[4]), .A2(n684), .ZN(n14) );
  AND3_X1 U29 ( .A1(n688), .A2(n687), .A3(n686), .ZN(n15) );
  CLKBUF_X1 U30 ( .A(n560), .Z(n16) );
  AND2_X1 U31 ( .A1(n6), .A2(n530), .ZN(status_inst[4]) );
  AND2_X2 U32 ( .A1(n499), .A2(n501), .ZN(n18) );
  AOI222_X1 U33 ( .A1(n53), .A2(n694), .B1(n102), .B2(n4), .C1(n136), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n582) );
  AND3_X1 U34 ( .A1(n663), .A2(n658), .A3(\U1/Elz[3] ), .ZN(n670) );
  AND3_X1 U35 ( .A1(n663), .A2(n658), .A3(\U1/Elz[7] ), .ZN(n682) );
  BUF_X1 U36 ( .A(\U1/ediff[8] ), .Z(n165) );
  CLKBUF_X1 U37 ( .A(n672), .Z(n19) );
  NAND2_X2 U38 ( .A1(n201), .A2(n113), .ZN(n608) );
  AND2_X1 U39 ( .A1(n641), .A2(n168), .ZN(n20) );
  AND2_X1 U40 ( .A1(n641), .A2(n168), .ZN(n21) );
  AND2_X1 U41 ( .A1(n641), .A2(n168), .ZN(n161) );
  AOI222_X1 U42 ( .A1(n159), .A2(\U1/a_norm[12] ), .B1(\U1/frac1[8] ), .B2(
        n176), .C1(status_inst[4]), .C2(n624), .ZN(n22) );
  INV_X1 U43 ( .A(n22), .ZN(z_inst[8]) );
  OR2_X1 U44 ( .A1(n390), .A2(\U1/large_p[0] ), .ZN(n392) );
  BUF_X1 U45 ( .A(n174), .Z(n23) );
  CLKBUF_X3 U46 ( .A(n174), .Z(n24) );
  AND2_X1 U47 ( .A1(n206), .A2(n154), .ZN(n174) );
  MUX2_X1 U48 ( .A(\U1/ediff[2] ), .B(\U1/N94 ), .S(n57), .Z(n25) );
  MUX2_X1 U49 ( .A(inst_b[19]), .B(inst_a[19]), .S(n208), .Z(n26) );
  CLKBUF_X1 U50 ( .A(n67), .Z(n31) );
  INV_X1 U51 ( .A(n105), .ZN(n421) );
  CLKBUF_X1 U52 ( .A(\U1/adder_output[22] ), .Z(n27) );
  CLKBUF_X1 U53 ( .A(\U1/adder_output[16] ), .Z(n28) );
  INV_X1 U54 ( .A(\U1/small_p[1] ), .ZN(n29) );
  INV_X1 U55 ( .A(n424), .ZN(n30) );
  MUX2_X1 U56 ( .A(inst_b[3]), .B(inst_a[3]), .S(n209), .Z(n32) );
  AND2_X2 U57 ( .A1(n12), .A2(n697), .ZN(n33) );
  AND2_X1 U58 ( .A1(n12), .A2(n697), .ZN(n173) );
  CLKBUF_X1 U59 ( .A(\U1/adder_output[10] ), .Z(n34) );
  INV_X1 U60 ( .A(n8), .ZN(n35) );
  INV_X1 U61 ( .A(n50), .ZN(n36) );
  AOI221_X1 U62 ( .B1(\U1/sig_small_shifted[4] ), .B2(n173), .C1(n477), .C2(
        n205), .A(n24), .ZN(\U1/sig_aligned2[4] ) );
  INV_X1 U63 ( .A(n154), .ZN(n37) );
  INV_X1 U64 ( .A(n61), .ZN(n424) );
  AND2_X1 U65 ( .A1(n675), .A2(n672), .ZN(n166) );
  MUX2_X1 U66 ( .A(inst_b[13]), .B(inst_a[13]), .S(n187), .Z(n38) );
  AOI221_X4 U67 ( .B1(\U1/sig_small_shifted[6] ), .B2(n173), .C1(n475), .C2(
        n206), .A(n24), .ZN(\U1/sig_aligned2[6] ) );
  MUX2_X1 U68 ( .A(inst_b[6]), .B(inst_a[6]), .S(n100), .Z(n39) );
  CLKBUF_X1 U69 ( .A(inst_a[26]), .Z(n40) );
  BUF_X1 U70 ( .A(n210), .Z(n54) );
  AND2_X1 U71 ( .A1(n279), .A2(n282), .ZN(n41) );
  AND2_X1 U72 ( .A1(n299), .A2(n302), .ZN(n42) );
  AND2_X1 U73 ( .A1(n306), .A2(n309), .ZN(n43) );
  CLKBUF_X1 U74 ( .A(\U1/small_p[20] ), .Z(n45) );
  BUF_X1 U75 ( .A(n163), .Z(n46) );
  AND2_X1 U76 ( .A1(n357), .A2(n360), .ZN(n47) );
  INV_X1 U77 ( .A(n31), .ZN(n433) );
  MUX2_X1 U78 ( .A(inst_b[18]), .B(inst_a[18]), .S(n99), .Z(n48) );
  INV_X1 U79 ( .A(n185), .ZN(n49) );
  INV_X1 U80 ( .A(n32), .ZN(n50) );
  BUF_X1 U81 ( .A(\U1/ediff[8] ), .Z(n164) );
  BUF_X1 U82 ( .A(n87), .Z(n80) );
  AND2_X1 U83 ( .A1(\U1/sig_small_shifted[9] ), .A2(n37), .ZN(n51) );
  INV_X1 U84 ( .A(n154), .ZN(n109) );
  AOI221_X1 U85 ( .B1(\U1/sig_small_shifted[9] ), .B2(n33), .C1(n205), .C2(
        n472), .A(n24), .ZN(\U1/sig_aligned2[9] ) );
  CLKBUF_X1 U86 ( .A(n622), .Z(n64) );
  MUX2_X1 U87 ( .A(inst_b[10]), .B(inst_a[10]), .S(n49), .Z(n52) );
  AOI221_X4 U88 ( .B1(n52), .B2(n429), .C1(n428), .C2(n427), .A(n426), .ZN(
        n444) );
  INV_X1 U89 ( .A(n520), .ZN(n53) );
  INV_X1 U90 ( .A(n85), .ZN(n436) );
  BUF_X1 U91 ( .A(n89), .Z(n148) );
  BUF_X1 U92 ( .A(n210), .Z(n186) );
  CLKBUF_X1 U93 ( .A(\U1/adder_output[15] ), .Z(n55) );
  MUX2_X1 U94 ( .A(inst_b[21]), .B(inst_a[21]), .S(n209), .Z(n56) );
  INV_X1 U95 ( .A(n130), .ZN(n57) );
  CLKBUF_X1 U96 ( .A(inst_b[26]), .Z(n58) );
  CLKBUF_X1 U97 ( .A(\U1/adder_output[27] ), .Z(n65) );
  CLKBUF_X1 U98 ( .A(\U1/adder_output[26] ), .Z(n59) );
  AND2_X1 U99 ( .A1(n623), .A2(n64), .ZN(n60) );
  AND3_X1 U100 ( .A1(n621), .A2(n687), .A3(n60), .ZN(n168) );
  MUX2_X1 U101 ( .A(inst_b[0]), .B(inst_a[0]), .S(n54), .Z(n61) );
  AOI21_X1 U102 ( .B1(n373), .B2(n372), .A(n68), .ZN(\U1/fr[6] ) );
  AOI222_X1 U103 ( .A1(\U1/adder_output[14] ), .A2(n694), .B1(n55), .B2(n4), 
        .C1(n75), .C2(\U1/U1/num_of_zeros[1] ), .ZN(n590) );
  NOR3_X1 U104 ( .A1(\U1/Elz[2] ), .A2(\U1/Elz[3] ), .A3(n11), .ZN(n62) );
  CLKBUF_X1 U105 ( .A(n678), .Z(n63) );
  OR2_X1 U106 ( .A1(n222), .A2(n651), .ZN(n622) );
  NAND2_X2 U107 ( .A1(n59), .A2(n528), .ZN(n501) );
  AOI222_X1 U108 ( .A1(n102), .A2(n694), .B1(n199), .B2(n18), .C1(n53), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n580) );
  AND2_X2 U109 ( .A1(\U1/num_zeros_path1_adj[2] ), .A2(n202), .ZN(
        \U1/num_zeros_used[2] ) );
  OR2_X1 U110 ( .A1(n375), .A2(\U1/large_p[2] ), .ZN(n380) );
  INV_X1 U111 ( .A(n184), .ZN(n207) );
  BUF_X1 U112 ( .A(n196), .Z(n184) );
  BUF_X1 U113 ( .A(inst_b[30]), .Z(n66) );
  MUX2_X1 U114 ( .A(inst_b[4]), .B(inst_a[4]), .S(n99), .Z(n67) );
  AND2_X1 U115 ( .A1(n371), .A2(n374), .ZN(n68) );
  CLKBUF_X1 U116 ( .A(inst_b[28]), .Z(n69) );
  AOI221_X1 U117 ( .B1(n205), .B2(n479), .C1(\U1/sig_small_shifted[2] ), .C2(
        n33), .A(n24), .ZN(\U1/sig_aligned2[2] ) );
  INV_X1 U118 ( .A(n547), .ZN(n70) );
  AOI221_X1 U119 ( .B1(n80), .B2(n431), .C1(n128), .C2(n96), .A(n36), .ZN(n432) );
  INV_X1 U120 ( .A(n100), .ZN(n71) );
  INV_X1 U121 ( .A(n71), .ZN(n72) );
  INV_X2 U122 ( .A(n71), .ZN(n73) );
  OR3_X1 U123 ( .A1(n77), .A2(n78), .A3(n167), .ZN(z_inst[1]) );
  CLKBUF_X1 U124 ( .A(\U1/adder_output[13] ), .Z(n75) );
  CLKBUF_X1 U125 ( .A(inst_a[28]), .Z(n76) );
  AND2_X1 U126 ( .A1(n135), .A2(\U1/a_norm[5] ), .ZN(n77) );
  AND2_X1 U127 ( .A1(\U1/frac1[1] ), .A2(n204), .ZN(n78) );
  BUF_X2 U128 ( .A(n176), .Z(n204) );
  AOI222_X1 U129 ( .A1(n55), .A2(n694), .B1(n28), .B2(n4), .C1(
        \U1/adder_output[14] ), .C2(\U1/U1/num_of_zeros[1] ), .ZN(n588) );
  BUF_X1 U130 ( .A(n196), .Z(n185) );
  CLKBUF_X1 U131 ( .A(\U1/small_p[15] ), .Z(n79) );
  CLKBUF_X1 U132 ( .A(\U1/adder_output[20] ), .Z(n81) );
  CLKBUF_X1 U133 ( .A(n12), .Z(n200) );
  MUX2_X1 U134 ( .A(inst_b[14]), .B(inst_a[14]), .S(n54), .Z(n82) );
  INV_X1 U135 ( .A(n333), .ZN(n83) );
  CLKBUF_X1 U136 ( .A(\U1/num_zeros_path1[5] ), .Z(n84) );
  MUX2_X1 U137 ( .A(inst_b[8]), .B(inst_a[8]), .S(n186), .Z(n85) );
  INV_X1 U138 ( .A(n46), .ZN(n398) );
  MUX2_X1 U139 ( .A(inst_b[16]), .B(inst_a[16]), .S(n100), .Z(n86) );
  MUX2_X1 U140 ( .A(inst_b[12]), .B(inst_a[12]), .S(n100), .Z(n87) );
  AND2_X1 U141 ( .A1(n244), .A2(n247), .ZN(n88) );
  MUX2_X1 U142 ( .A(inst_b[7]), .B(n83), .S(n187), .Z(n89) );
  INV_X1 U143 ( .A(n257), .ZN(n90) );
  INV_X1 U144 ( .A(n79), .ZN(n91) );
  AND2_X1 U145 ( .A1(n319), .A2(n322), .ZN(n92) );
  CLKBUF_X1 U146 ( .A(n86), .Z(n93) );
  INV_X1 U147 ( .A(n45), .ZN(n94) );
  MUX2_X1 U148 ( .A(\U1/ediff[1] ), .B(\U1/N93 ), .S(n57), .Z(n95) );
  INV_X1 U149 ( .A(n188), .ZN(n96) );
  AND2_X1 U150 ( .A1(\U1/sig_small_shifted[13] ), .A2(n183), .ZN(n97) );
  CLKBUF_X1 U151 ( .A(n56), .Z(n98) );
  INV_X1 U152 ( .A(n211), .ZN(n99) );
  INV_X1 U153 ( .A(n211), .ZN(n100) );
  INV_X1 U154 ( .A(n211), .ZN(n209) );
  BUF_X1 U155 ( .A(inst_b[28]), .Z(n151) );
  INV_X1 U156 ( .A(n113), .ZN(n101) );
  INV_X1 U157 ( .A(\U1/a_norm_partial_path1[27] ), .ZN(\U1/extra_shift_path1 )
         );
  INV_X1 U158 ( .A(n517), .ZN(n102) );
  INV_X1 U159 ( .A(n97), .ZN(n103) );
  INV_X1 U160 ( .A(n148), .ZN(n104) );
  MUX2_X1 U161 ( .A(inst_a[9]), .B(inst_b[9]), .S(n184), .Z(n105) );
  AND2_X1 U162 ( .A1(\U1/sig_small_shifted[6] ), .A2(n200), .ZN(n106) );
  CLKBUF_X1 U163 ( .A(\U1/sig_small_shifted[6] ), .Z(n107) );
  CLKBUF_X1 U164 ( .A(\U1/adder_output[24] ), .Z(n108) );
  AOI221_X1 U165 ( .B1(\U1/sig_small_shifted[14] ), .B2(n173), .C1(n206), .C2(
        n467), .A(n24), .ZN(\U1/sig_aligned2[14] ) );
  CLKBUF_X1 U166 ( .A(n82), .Z(n110) );
  AOI222_X4 U167 ( .A1(\U1/Elz_12 ), .A2(n655), .B1(\U1/E1[0] ), .B2(n172), 
        .C1(\U1/large_p[23] ), .C2(n171), .ZN(n660) );
  CLKBUF_X1 U168 ( .A(\U1/adder_output[12] ), .Z(n111) );
  MUX2_X1 U169 ( .A(inst_b[17]), .B(inst_a[17]), .S(n99), .Z(n112) );
  OR2_X1 U170 ( .A1(n126), .A2(n167), .ZN(n114) );
  OR2_X1 U171 ( .A1(n125), .A2(n114), .ZN(z_inst[21]) );
  CLKBUF_X1 U172 ( .A(\U1/Elz_12 ), .Z(n115) );
  NAND2_X1 U173 ( .A1(\U1/num_zeros_path2[0] ), .A2(n116), .ZN(n117) );
  NAND2_X1 U174 ( .A1(\U1/num_zeros_path1_adj[0] ), .A2(n201), .ZN(n118) );
  NAND2_X1 U175 ( .A1(n117), .A2(n118), .ZN(\U1/num_zeros_used[0] ) );
  INV_X1 U176 ( .A(n201), .ZN(n116) );
  CLKBUF_X1 U177 ( .A(n162), .Z(n119) );
  OR3_X1 U178 ( .A1(n132), .A2(n133), .A3(n167), .ZN(z_inst[22]) );
  AND2_X1 U179 ( .A1(n129), .A2(n414), .ZN(n121) );
  AND2_X1 U180 ( .A1(\U1/small_p[11] ), .A2(n417), .ZN(n122) );
  NOR3_X1 U181 ( .A1(n413), .A2(n122), .A3(n121), .ZN(n446) );
  XOR2_X1 U182 ( .A(n450), .B(n178), .Z(n123) );
  INV_X1 U183 ( .A(n123), .ZN(n697) );
  AND2_X1 U184 ( .A1(n20), .A2(\U1/a_norm[25] ), .ZN(n125) );
  AND2_X1 U185 ( .A1(\U1/frac1[21] ), .A2(n203), .ZN(n126) );
  AOI222_X1 U186 ( .A1(n27), .A2(n694), .B1(n144), .B2(n18), .C1(n197), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n574) );
  AND2_X1 U187 ( .A1(n288), .A2(n37), .ZN(n127) );
  CLKBUF_X1 U188 ( .A(n26), .Z(n128) );
  INV_X1 U189 ( .A(n143), .ZN(n129) );
  INV_X1 U190 ( .A(\U1/ediff[8] ), .ZN(n130) );
  INV_X1 U191 ( .A(n454), .ZN(\U1/large_p[27] ) );
  NOR3_X1 U192 ( .A1(\U1/Elz[2] ), .A2(\U1/Elz[3] ), .A3(n11), .ZN(n487) );
  INV_X1 U193 ( .A(n545), .ZN(n131) );
  AOI222_X1 U194 ( .A1(n144), .A2(n694), .B1(n108), .B2(n18), .C1(n27), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n572) );
  AOI222_X1 U195 ( .A1(n136), .A2(n694), .B1(n53), .B2(n18), .C1(n28), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n584) );
  AND2_X1 U196 ( .A1(n161), .A2(\U1/a_norm[26] ), .ZN(n132) );
  AND2_X1 U197 ( .A1(\U1/frac1[22] ), .A2(n203), .ZN(n133) );
  BUF_X2 U198 ( .A(n176), .Z(n203) );
  AND2_X1 U199 ( .A1(n630), .A2(n168), .ZN(n134) );
  AND2_X1 U200 ( .A1(n630), .A2(n168), .ZN(n135) );
  INV_X1 U201 ( .A(n521), .ZN(n136) );
  AND2_X1 U202 ( .A1(n630), .A2(n168), .ZN(n159) );
  INV_X1 U203 ( .A(n544), .ZN(n137) );
  INV_X1 U204 ( .A(n537), .ZN(n138) );
  CLKBUF_X1 U205 ( .A(n193), .Z(n139) );
  CLKBUF_X1 U206 ( .A(inst_a[27]), .Z(n140) );
  BUF_X1 U207 ( .A(inst_b[26]), .Z(n194) );
  CLKBUF_X1 U208 ( .A(inst_b[24]), .Z(n141) );
  INV_X1 U209 ( .A(n543), .ZN(n142) );
  AOI221_X1 U210 ( .B1(\U1/sig_small_shifted[11] ), .B2(n33), .C1(n470), .C2(
        n205), .A(n24), .ZN(\U1/sig_aligned2[11] ) );
  INV_X1 U211 ( .A(n39), .ZN(n143) );
  INV_X1 U212 ( .A(n514), .ZN(n144) );
  CLKBUF_X1 U213 ( .A(\U1/sig_small_shifted[2] ), .Z(n145) );
  CLKBUF_X1 U214 ( .A(n112), .Z(n146) );
  INV_X1 U215 ( .A(n185), .ZN(n147) );
  CLKBUF_X1 U216 ( .A(n165), .Z(n156) );
  CLKBUF_X1 U217 ( .A(\U1/adder_output[11] ), .Z(n149) );
  AOI222_X1 U218 ( .A1(n150), .A2(n694), .B1(n149), .B2(n4), .C1(
        \U1/adder_output[9] ), .C2(\U1/U1/num_of_zeros[1] ), .ZN(n598) );
  MUX2_X1 U219 ( .A(n226), .B(n227), .S(n184), .Z(n452) );
  CLKBUF_X1 U220 ( .A(n72), .Z(n181) );
  INV_X1 U221 ( .A(n522), .ZN(n150) );
  AOI221_X1 U222 ( .B1(n205), .B2(n462), .C1(\U1/sig_small_shifted[19] ), .C2(
        n33), .A(n23), .ZN(\U1/sig_aligned2[19] ) );
  AOI221_X4 U223 ( .B1(\U1/sig_small_shifted[10] ), .B2(n33), .C1(n471), .C2(
        n206), .A(n24), .ZN(\U1/sig_aligned2[10] ) );
  CLKBUF_X3 U224 ( .A(n123), .Z(n205) );
  CLKBUF_X1 U225 ( .A(inst_b[23]), .Z(n152) );
  CLKBUF_X1 U226 ( .A(n651), .Z(n153) );
  NOR3_X1 U227 ( .A1(\U1/Elz_12 ), .A2(n204), .A3(n14), .ZN(n621) );
  BUF_X1 U228 ( .A(n12), .Z(n182) );
  INV_X1 U229 ( .A(n93), .ZN(n155) );
  MUX2_X2 U230 ( .A(\U1/ediff[3] ), .B(\U1/N95 ), .S(n156), .Z(n189) );
  AOI221_X4 U231 ( .B1(n206), .B2(n480), .C1(\U1/sig_small_shifted[1] ), .C2(
        n33), .A(n23), .ZN(\U1/sig_aligned2[1] ) );
  INV_X1 U232 ( .A(n518), .ZN(n157) );
  INV_X1 U233 ( .A(\U1/Elz[6] ), .ZN(n158) );
  CLKBUF_X1 U234 ( .A(n113), .Z(n160) );
  AND2_X1 U235 ( .A1(n158), .A2(n62), .ZN(n162) );
  AND2_X1 U236 ( .A1(n678), .A2(n487), .ZN(n169) );
  MUX2_X1 U237 ( .A(inst_a[22]), .B(inst_b[22]), .S(n185), .Z(n163) );
  AOI222_X1 U238 ( .A1(n108), .A2(n694), .B1(n157), .B2(n4), .C1(n144), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n570) );
  AND2_X2 U239 ( .A1(status_inst[4]), .A2(n624), .ZN(n167) );
  AOI221_X1 U240 ( .B1(\U1/sig_small_shifted[15] ), .B2(n173), .C1(n205), .C2(
        n466), .A(n24), .ZN(\U1/sig_aligned2[15] ) );
  AOI221_X1 U241 ( .B1(\U1/sig_small_shifted[12] ), .B2(n173), .C1(n469), .C2(
        n205), .A(n23), .ZN(\U1/sig_aligned2[12] ) );
  AOI221_X1 U242 ( .B1(n473), .B2(n206), .C1(\U1/sig_small_shifted[8] ), .C2(
        n33), .A(n23), .ZN(\U1/sig_aligned2[8] ) );
  AND2_X1 U243 ( .A1(n455), .A2(n239), .ZN(\U1/fr[26] ) );
  AOI221_X1 U244 ( .B1(\U1/sig_small_shifted[13] ), .B2(n33), .C1(n205), .C2(
        n468), .A(n24), .ZN(\U1/sig_aligned2[13] ) );
  AND2_X1 U245 ( .A1(n654), .A2(n653), .ZN(n171) );
  CLKBUF_X1 U246 ( .A(n419), .Z(n188) );
  AND2_X1 U247 ( .A1(n652), .A2(n653), .ZN(n172) );
  AOI221_X1 U248 ( .B1(n465), .B2(n206), .C1(\U1/sig_small_shifted[16] ), .C2(
        n33), .A(n23), .ZN(\U1/sig_aligned2[16] ) );
  AOI221_X1 U249 ( .B1(n474), .B2(n206), .C1(\U1/sig_small_shifted[7] ), .C2(
        n33), .A(n24), .ZN(\U1/sig_aligned2[7] ) );
  AND3_X1 U250 ( .A1(n443), .A2(n444), .A3(n445), .ZN(n175) );
  OR2_X1 U251 ( .A1(n404), .A2(n95), .ZN(n402) );
  AND2_X1 U252 ( .A1(n619), .A2(n620), .ZN(n176) );
  AND2_X1 U253 ( .A1(n554), .A2(n553), .ZN(n177) );
  XNOR2_X1 U254 ( .A(inst_op), .B(n449), .ZN(n178) );
  XNOR2_X1 U255 ( .A(n491), .B(n179), .ZN(n685) );
  OR2_X1 U256 ( .A1(n184), .A2(n490), .ZN(n179) );
  AOI222_X1 U257 ( .A1(n149), .A2(n694), .B1(n111), .B2(n4), .C1(n150), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n596) );
  INV_X1 U258 ( .A(n419), .ZN(\U1/mag_exp_diff[4] ) );
  INV_X1 U259 ( .A(n211), .ZN(n208) );
  BUF_X1 U260 ( .A(inst_b[25]), .Z(n193) );
  BUF_X2 U261 ( .A(n12), .Z(n183) );
  MUX2_X1 U262 ( .A(\U1/ediff[3] ), .B(\U1/N95 ), .S(n165), .Z(n190) );
  MUX2_X1 U263 ( .A(\U1/ediff[3] ), .B(\U1/N95 ), .S(n165), .Z(
        \U1/mag_exp_diff[3] ) );
  INV_X1 U264 ( .A(n546), .ZN(n191) );
  CLKBUF_X1 U265 ( .A(inst_a[23]), .Z(n192) );
  BUF_X2 U266 ( .A(inst_a[29]), .Z(n195) );
  INV_X1 U267 ( .A(\U1/swap ), .ZN(n196) );
  INV_X1 U268 ( .A(\U1/swap ), .ZN(n211) );
  CLKBUF_X1 U269 ( .A(\U1/adder_output[21] ), .Z(n197) );
  AND2_X1 U270 ( .A1(n37), .A2(n447), .ZN(n198) );
  NAND2_X1 U271 ( .A1(n446), .A2(n175), .ZN(n447) );
  INV_X1 U272 ( .A(n516), .ZN(n199) );
  AOI222_X1 U273 ( .A1(n157), .A2(n694), .B1(n18), .B2(n59), .C1(n108), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n569) );
  INV_X2 U274 ( .A(n225), .ZN(\U1/large_p[26] ) );
  INV_X2 U275 ( .A(n223), .ZN(\U1/large_p[25] ) );
  INV_X2 U276 ( .A(n499), .ZN(\U1/U1/num_of_zeros[1] ) );
  INV_X2 U277 ( .A(n501), .ZN(n694) );
  CLKBUF_X3 U278 ( .A(n617), .Z(n201) );
  INV_X1 U279 ( .A(n196), .ZN(n210) );
  INV_X1 U280 ( .A(n25), .ZN(n212) );
  INV_X1 U281 ( .A(\U1/mag_exp_diff[0] ), .ZN(n213) );
  INV_X1 U282 ( .A(\U1/num_zeros_path1[2] ), .ZN(n214) );
  INV_X1 U283 ( .A(\U1/num_zeros_path1[1] ), .ZN(n215) );
  INV_X1 U284 ( .A(\U1/num_zeros_path1[0] ), .ZN(n216) );
  INV_X1 U285 ( .A(inst_b[22]), .ZN(n231) );
  INV_X1 U286 ( .A(inst_a[22]), .ZN(n232) );
  INV_X1 U287 ( .A(inst_b[21]), .ZN(n235) );
  INV_X1 U288 ( .A(inst_a[21]), .ZN(n236) );
  OAI22_X1 U289 ( .A1(inst_a[20]), .A2(n184), .B1(n208), .B2(inst_b[20]), .ZN(
        n434) );
  INV_X1 U290 ( .A(n434), .ZN(\U1/small_p[20] ) );
  INV_X1 U291 ( .A(inst_b[19]), .ZN(n248) );
  INV_X1 U292 ( .A(inst_a[19]), .ZN(n249) );
  INV_X1 U293 ( .A(inst_b[18]), .ZN(n255) );
  INV_X1 U294 ( .A(inst_a[18]), .ZN(n256) );
  INV_X1 U295 ( .A(inst_b[17]), .ZN(n262) );
  INV_X1 U296 ( .A(inst_a[17]), .ZN(n263) );
  INV_X1 U297 ( .A(inst_b[16]), .ZN(n269) );
  INV_X1 U298 ( .A(inst_a[16]), .ZN(n270) );
  INV_X1 U299 ( .A(inst_b[15]), .ZN(n277) );
  INV_X1 U300 ( .A(inst_a[15]), .ZN(n278) );
  MUX2_X1 U301 ( .A(n277), .B(n278), .S(n209), .Z(n400) );
  INV_X1 U302 ( .A(n400), .ZN(\U1/small_p[15] ) );
  INV_X1 U303 ( .A(inst_b[14]), .ZN(n283) );
  INV_X1 U304 ( .A(inst_a[14]), .ZN(n284) );
  INV_X1 U305 ( .A(inst_b[13]), .ZN(n289) );
  INV_X1 U306 ( .A(inst_a[13]), .ZN(n290) );
  INV_X1 U307 ( .A(inst_b[12]), .ZN(n297) );
  INV_X1 U308 ( .A(inst_a[12]), .ZN(n298) );
  INV_X1 U309 ( .A(inst_b[11]), .ZN(n304) );
  INV_X1 U310 ( .A(inst_a[11]), .ZN(n305) );
  MUX2_X1 U311 ( .A(n304), .B(n305), .S(n209), .Z(n217) );
  INV_X1 U312 ( .A(n217), .ZN(\U1/small_p[11] ) );
  INV_X1 U313 ( .A(inst_b[10]), .ZN(n311) );
  INV_X1 U314 ( .A(inst_a[10]), .ZN(n312) );
  INV_X1 U315 ( .A(inst_b[9]), .ZN(n317) );
  INV_X1 U316 ( .A(inst_a[9]), .ZN(n318) );
  INV_X1 U317 ( .A(inst_b[8]), .ZN(n324) );
  INV_X1 U318 ( .A(inst_a[8]), .ZN(n325) );
  INV_X1 U319 ( .A(inst_b[7]), .ZN(n332) );
  INV_X1 U320 ( .A(inst_a[7]), .ZN(n333) );
  INV_X1 U321 ( .A(inst_b[6]), .ZN(n340) );
  INV_X1 U322 ( .A(inst_a[6]), .ZN(n341) );
  OAI22_X1 U323 ( .A1(n99), .A2(inst_b[5]), .B1(inst_a[5]), .B2(n185), .ZN(
        n415) );
  INV_X1 U324 ( .A(n415), .ZN(\U1/small_p[5] ) );
  INV_X1 U325 ( .A(inst_b[4]), .ZN(n355) );
  INV_X1 U326 ( .A(inst_a[4]), .ZN(n356) );
  INV_X1 U327 ( .A(inst_b[3]), .ZN(n362) );
  INV_X1 U328 ( .A(inst_a[3]), .ZN(n363) );
  INV_X1 U329 ( .A(inst_b[2]), .ZN(n369) );
  INV_X1 U330 ( .A(inst_a[2]), .ZN(n370) );
  INV_X1 U331 ( .A(inst_b[1]), .ZN(n376) );
  INV_X1 U332 ( .A(inst_a[1]), .ZN(n377) );
  MUX2_X1 U333 ( .A(n376), .B(n377), .S(n208), .Z(n423) );
  INV_X1 U334 ( .A(n423), .ZN(\U1/small_p[1] ) );
  INV_X1 U335 ( .A(inst_b[0]), .ZN(n384) );
  INV_X1 U336 ( .A(inst_a[0]), .ZN(n385) );
  MUX2_X1 U337 ( .A(\U1/ediff[7] ), .B(\U1/N99 ), .S(n156), .Z(
        \U1/mag_exp_diff[7] ) );
  MUX2_X1 U338 ( .A(\U1/ediff[6] ), .B(\U1/N98 ), .S(n165), .Z(
        \U1/mag_exp_diff[6] ) );
  MUX2_X1 U339 ( .A(\U1/ediff[5] ), .B(\U1/N97 ), .S(n165), .Z(
        \U1/mag_exp_diff[5] ) );
  OAI22_X1 U340 ( .A1(\U1/ediff[4] ), .A2(n164), .B1(n130), .B2(\U1/N96 ), 
        .ZN(n419) );
  MUX2_X1 U341 ( .A(\U1/ediff[1] ), .B(\U1/N93 ), .S(n164), .Z(
        \U1/mag_exp_diff[1] ) );
  MUX2_X1 U342 ( .A(\U1/ediff[0] ), .B(\U1/N92 ), .S(n165), .Z(
        \U1/mag_exp_diff[0] ) );
  INV_X1 U343 ( .A(inst_b[29]), .ZN(n226) );
  INV_X1 U344 ( .A(n195), .ZN(n227) );
  MUX2_X1 U345 ( .A(n226), .B(n227), .S(n180), .Z(n549) );
  INV_X1 U346 ( .A(n549), .ZN(n230) );
  INV_X1 U347 ( .A(n139), .ZN(n536) );
  INV_X1 U348 ( .A(n152), .ZN(n532) );
  INV_X1 U349 ( .A(inst_b[24]), .ZN(n534) );
  NOR4_X1 U350 ( .A1(n191), .A2(n66), .A3(n137), .A4(n58), .ZN(n218) );
  NAND4_X1 U351 ( .A1(n536), .A2(n532), .A3(n534), .A4(n218), .ZN(n221) );
  INV_X1 U352 ( .A(inst_a[25]), .ZN(n535) );
  INV_X1 U353 ( .A(n192), .ZN(n531) );
  INV_X1 U354 ( .A(inst_a[24]), .ZN(n533) );
  NOR4_X1 U355 ( .A1(n142), .A2(n70), .A3(n131), .A4(n138), .ZN(n219) );
  NAND4_X1 U356 ( .A1(n535), .A2(n531), .A3(n533), .A4(n219), .ZN(n220) );
  MUX2_X1 U357 ( .A(n221), .B(n220), .S(n180), .Z(n229) );
  MUX2_X1 U358 ( .A(n531), .B(n532), .S(n147), .Z(n222) );
  INV_X1 U359 ( .A(n222), .ZN(\U1/large_p[23] ) );
  MUX2_X1 U360 ( .A(n535), .B(n536), .S(n147), .Z(n223) );
  MUX2_X1 U361 ( .A(n533), .B(n534), .S(n207), .Z(n224) );
  INV_X1 U362 ( .A(n224), .ZN(\U1/large_p[24] ) );
  INV_X1 U363 ( .A(n40), .ZN(n537) );
  INV_X1 U364 ( .A(n58), .ZN(n538) );
  MUX2_X1 U365 ( .A(n537), .B(n538), .S(n186), .Z(n225) );
  INV_X1 U366 ( .A(inst_a[30]), .ZN(n547) );
  INV_X1 U367 ( .A(inst_b[30]), .ZN(n548) );
  MUX2_X1 U368 ( .A(n547), .B(n548), .S(n207), .Z(n451) );
  INV_X1 U369 ( .A(n76), .ZN(n545) );
  INV_X1 U370 ( .A(n69), .ZN(n546) );
  MUX2_X1 U371 ( .A(n545), .B(n546), .S(n100), .Z(n453) );
  INV_X1 U372 ( .A(n140), .ZN(n543) );
  INV_X1 U373 ( .A(inst_b[27]), .ZN(n544) );
  MUX2_X1 U374 ( .A(n543), .B(n544), .S(n99), .Z(n454) );
  NOR4_X1 U375 ( .A1(n451), .A2(n452), .A3(n454), .A4(n453), .ZN(n228) );
  NAND4_X1 U376 ( .A1(n228), .A2(\U1/large_p[24] ), .A3(\U1/large_p[26] ), 
        .A4(\U1/large_p[25] ), .ZN(n651) );
  INV_X1 U377 ( .A(n153), .ZN(n530) );
  NAND2_X1 U378 ( .A1(\U1/sig_small_shifted[26] ), .A2(n183), .ZN(n455) );
  NAND2_X1 U379 ( .A1(\U1/sig_small_shifted[25] ), .A2(n37), .ZN(n456) );
  INV_X1 U380 ( .A(n456), .ZN(n233) );
  MUX2_X1 U381 ( .A(n232), .B(n231), .S(n180), .Z(n234) );
  NAND2_X1 U382 ( .A1(n233), .A2(n234), .ZN(n239) );
  INV_X1 U383 ( .A(n234), .ZN(\U1/large_p[22] ) );
  NAND2_X1 U384 ( .A1(\U1/large_p[22] ), .A2(n456), .ZN(n240) );
  NAND2_X1 U385 ( .A1(\U1/sig_small_shifted[24] ), .A2(n109), .ZN(n457) );
  INV_X1 U386 ( .A(n457), .ZN(n237) );
  MUX2_X1 U387 ( .A(n236), .B(n235), .S(n181), .Z(n241) );
  NAND2_X1 U388 ( .A1(n237), .A2(n241), .ZN(n245) );
  INV_X1 U389 ( .A(n245), .ZN(n238) );
  AOI21_X1 U390 ( .B1(n240), .B2(n239), .A(n238), .ZN(\U1/fr[25] ) );
  INV_X1 U391 ( .A(n241), .ZN(\U1/large_p[21] ) );
  NAND2_X1 U392 ( .A1(\U1/large_p[21] ), .A2(n457), .ZN(n246) );
  NAND2_X1 U393 ( .A1(\U1/sig_small_shifted[23] ), .A2(n182), .ZN(n458) );
  INV_X1 U394 ( .A(n458), .ZN(n244) );
  INV_X1 U395 ( .A(inst_a[20]), .ZN(n243) );
  INV_X1 U396 ( .A(inst_b[20]), .ZN(n242) );
  MUX2_X1 U397 ( .A(n243), .B(n242), .S(n181), .Z(n247) );
  NAND2_X1 U398 ( .A1(n244), .A2(n247), .ZN(n252) );
  AOI21_X1 U399 ( .B1(n246), .B2(n245), .A(n88), .ZN(\U1/fr[24] ) );
  INV_X1 U400 ( .A(n247), .ZN(\U1/large_p[20] ) );
  NAND2_X1 U401 ( .A1(\U1/large_p[20] ), .A2(n458), .ZN(n253) );
  NAND2_X1 U402 ( .A1(\U1/sig_small_shifted[22] ), .A2(n109), .ZN(n459) );
  INV_X1 U403 ( .A(n459), .ZN(n250) );
  MUX2_X1 U404 ( .A(n249), .B(n248), .S(n180), .Z(n254) );
  NAND2_X1 U405 ( .A1(n250), .A2(n254), .ZN(n259) );
  INV_X1 U406 ( .A(n259), .ZN(n251) );
  AOI21_X1 U407 ( .B1(n253), .B2(n252), .A(n251), .ZN(\U1/fr[23] ) );
  INV_X1 U408 ( .A(n254), .ZN(\U1/large_p[19] ) );
  NAND2_X1 U409 ( .A1(\U1/large_p[19] ), .A2(n459), .ZN(n260) );
  NAND2_X1 U410 ( .A1(\U1/sig_small_shifted[21] ), .A2(n109), .ZN(n460) );
  INV_X1 U411 ( .A(n460), .ZN(n257) );
  MUX2_X1 U412 ( .A(n256), .B(n255), .S(n180), .Z(n261) );
  NAND2_X1 U413 ( .A1(n257), .A2(n261), .ZN(n266) );
  INV_X1 U414 ( .A(n266), .ZN(n258) );
  AOI21_X1 U415 ( .B1(n260), .B2(n259), .A(n258), .ZN(\U1/fr[22] ) );
  INV_X1 U416 ( .A(n261), .ZN(\U1/large_p[18] ) );
  NAND2_X1 U417 ( .A1(\U1/large_p[18] ), .A2(n90), .ZN(n267) );
  NAND2_X1 U418 ( .A1(\U1/sig_small_shifted[20] ), .A2(n109), .ZN(n461) );
  INV_X1 U419 ( .A(n461), .ZN(n264) );
  MUX2_X1 U420 ( .A(n263), .B(n262), .S(n181), .Z(n268) );
  NAND2_X1 U421 ( .A1(n264), .A2(n268), .ZN(n273) );
  INV_X1 U422 ( .A(n273), .ZN(n265) );
  AOI21_X1 U423 ( .B1(n267), .B2(n266), .A(n265), .ZN(\U1/fr[21] ) );
  INV_X1 U424 ( .A(n268), .ZN(\U1/large_p[17] ) );
  NAND2_X1 U425 ( .A1(\U1/large_p[17] ), .A2(n461), .ZN(n274) );
  NAND2_X1 U426 ( .A1(\U1/sig_small_shifted[19] ), .A2(n182), .ZN(n276) );
  INV_X1 U427 ( .A(n276), .ZN(n271) );
  MUX2_X1 U428 ( .A(n270), .B(n269), .S(n180), .Z(n275) );
  NAND2_X1 U429 ( .A1(n271), .A2(n275), .ZN(n280) );
  INV_X1 U430 ( .A(n280), .ZN(n272) );
  AOI21_X1 U431 ( .B1(n274), .B2(n273), .A(n272), .ZN(\U1/fr[20] ) );
  INV_X1 U432 ( .A(n275), .ZN(\U1/large_p[16] ) );
  NAND2_X1 U433 ( .A1(\U1/large_p[16] ), .A2(n276), .ZN(n281) );
  NAND2_X1 U434 ( .A1(\U1/sig_small_shifted[18] ), .A2(n109), .ZN(n463) );
  INV_X1 U435 ( .A(n463), .ZN(n279) );
  MUX2_X1 U436 ( .A(n278), .B(n277), .S(n181), .Z(n282) );
  NAND2_X1 U437 ( .A1(n279), .A2(n282), .ZN(n286) );
  AOI21_X1 U438 ( .B1(n281), .B2(n280), .A(n41), .ZN(\U1/fr[19] ) );
  INV_X1 U439 ( .A(n282), .ZN(\U1/large_p[15] ) );
  NAND2_X1 U440 ( .A1(\U1/large_p[15] ), .A2(n463), .ZN(n287) );
  NAND2_X1 U441 ( .A1(\U1/sig_small_shifted[17] ), .A2(n200), .ZN(n464) );
  MUX2_X1 U442 ( .A(n284), .B(n283), .S(n72), .Z(n288) );
  NAND2_X1 U443 ( .A1(n127), .A2(\U1/sig_small_shifted[17] ), .ZN(n293) );
  INV_X1 U444 ( .A(n293), .ZN(n285) );
  AOI21_X1 U445 ( .B1(n287), .B2(n286), .A(n285), .ZN(\U1/fr[18] ) );
  INV_X1 U446 ( .A(n288), .ZN(\U1/large_p[14] ) );
  NAND2_X1 U447 ( .A1(\U1/large_p[14] ), .A2(n464), .ZN(n294) );
  NAND2_X1 U448 ( .A1(\U1/sig_small_shifted[16] ), .A2(n183), .ZN(n296) );
  INV_X1 U449 ( .A(n296), .ZN(n291) );
  MUX2_X1 U450 ( .A(n290), .B(n289), .S(n73), .Z(n295) );
  NAND2_X1 U451 ( .A1(n291), .A2(n295), .ZN(n300) );
  INV_X1 U452 ( .A(n300), .ZN(n292) );
  AOI21_X1 U453 ( .B1(n294), .B2(n293), .A(n292), .ZN(\U1/fr[17] ) );
  INV_X1 U454 ( .A(n295), .ZN(\U1/large_p[13] ) );
  NAND2_X1 U455 ( .A1(\U1/large_p[13] ), .A2(n296), .ZN(n301) );
  NAND2_X1 U456 ( .A1(\U1/sig_small_shifted[15] ), .A2(n183), .ZN(n303) );
  INV_X1 U457 ( .A(n303), .ZN(n299) );
  MUX2_X1 U458 ( .A(n298), .B(n297), .S(n72), .Z(n302) );
  NAND2_X1 U459 ( .A1(n299), .A2(n302), .ZN(n307) );
  AOI21_X1 U460 ( .B1(n301), .B2(n300), .A(n42), .ZN(\U1/fr[16] ) );
  INV_X1 U461 ( .A(n302), .ZN(\U1/large_p[12] ) );
  NAND2_X1 U462 ( .A1(\U1/large_p[12] ), .A2(n303), .ZN(n308) );
  NAND2_X1 U463 ( .A1(\U1/sig_small_shifted[14] ), .A2(n183), .ZN(n310) );
  INV_X1 U464 ( .A(n310), .ZN(n306) );
  MUX2_X1 U465 ( .A(n305), .B(n304), .S(n73), .Z(n309) );
  NAND2_X1 U466 ( .A1(n306), .A2(n309), .ZN(n314) );
  AOI21_X1 U467 ( .B1(n308), .B2(n307), .A(n43), .ZN(\U1/fr[15] ) );
  INV_X1 U468 ( .A(n309), .ZN(\U1/large_p[11] ) );
  NAND2_X1 U469 ( .A1(\U1/large_p[11] ), .A2(n310), .ZN(n315) );
  MUX2_X1 U470 ( .A(n312), .B(n311), .S(n72), .Z(n316) );
  NAND2_X1 U471 ( .A1(n97), .A2(n316), .ZN(n320) );
  INV_X1 U472 ( .A(n320), .ZN(n313) );
  AOI21_X1 U473 ( .B1(n315), .B2(n314), .A(n313), .ZN(\U1/fr[14] ) );
  INV_X1 U474 ( .A(n316), .ZN(\U1/large_p[10] ) );
  NAND2_X1 U475 ( .A1(\U1/large_p[10] ), .A2(n103), .ZN(n321) );
  NAND2_X1 U476 ( .A1(\U1/sig_small_shifted[12] ), .A2(n183), .ZN(n323) );
  INV_X1 U477 ( .A(n323), .ZN(n319) );
  MUX2_X1 U478 ( .A(n318), .B(n317), .S(n72), .Z(n322) );
  NAND2_X1 U479 ( .A1(n319), .A2(n322), .ZN(n328) );
  AOI21_X1 U480 ( .B1(n321), .B2(n320), .A(n92), .ZN(\U1/fr[13] ) );
  INV_X1 U481 ( .A(n322), .ZN(\U1/large_p[9] ) );
  NAND2_X1 U482 ( .A1(\U1/large_p[9] ), .A2(n323), .ZN(n329) );
  NAND2_X1 U483 ( .A1(\U1/sig_small_shifted[11] ), .A2(n200), .ZN(n331) );
  INV_X1 U484 ( .A(n331), .ZN(n326) );
  MUX2_X1 U485 ( .A(n325), .B(n324), .S(n73), .Z(n330) );
  NAND2_X1 U486 ( .A1(n326), .A2(n330), .ZN(n336) );
  INV_X1 U487 ( .A(n336), .ZN(n327) );
  AOI21_X1 U488 ( .B1(n329), .B2(n328), .A(n327), .ZN(\U1/fr[12] ) );
  INV_X1 U489 ( .A(n330), .ZN(\U1/large_p[8] ) );
  NAND2_X1 U490 ( .A1(\U1/large_p[8] ), .A2(n331), .ZN(n337) );
  NAND2_X1 U491 ( .A1(\U1/sig_small_shifted[10] ), .A2(n183), .ZN(n339) );
  INV_X1 U492 ( .A(n339), .ZN(n334) );
  MUX2_X1 U493 ( .A(n333), .B(n332), .S(n73), .Z(n338) );
  NAND2_X1 U494 ( .A1(n334), .A2(n338), .ZN(n343) );
  INV_X1 U495 ( .A(n343), .ZN(n335) );
  AOI21_X1 U496 ( .B1(n337), .B2(n336), .A(n335), .ZN(\U1/fr[11] ) );
  INV_X1 U497 ( .A(n338), .ZN(\U1/large_p[7] ) );
  NAND2_X1 U498 ( .A1(\U1/large_p[7] ), .A2(n339), .ZN(n344) );
  NAND2_X1 U499 ( .A1(\U1/sig_small_shifted[9] ), .A2(n37), .ZN(n346) );
  MUX2_X1 U500 ( .A(n341), .B(n340), .S(n73), .Z(n345) );
  NAND2_X1 U501 ( .A1(n51), .A2(n345), .ZN(n351) );
  INV_X1 U502 ( .A(n351), .ZN(n342) );
  AOI21_X1 U503 ( .B1(n344), .B2(n343), .A(n342), .ZN(\U1/fr[10] ) );
  INV_X1 U504 ( .A(n345), .ZN(\U1/large_p[6] ) );
  NAND2_X1 U505 ( .A1(\U1/large_p[6] ), .A2(n346), .ZN(n352) );
  NAND2_X1 U506 ( .A1(\U1/sig_small_shifted[8] ), .A2(n200), .ZN(n354) );
  INV_X1 U507 ( .A(n354), .ZN(n349) );
  INV_X1 U508 ( .A(inst_a[5]), .ZN(n348) );
  INV_X1 U509 ( .A(inst_b[5]), .ZN(n347) );
  MUX2_X1 U510 ( .A(n348), .B(n347), .S(n73), .Z(n353) );
  NAND2_X1 U511 ( .A1(n349), .A2(n353), .ZN(n358) );
  INV_X1 U512 ( .A(n358), .ZN(n350) );
  AOI21_X1 U513 ( .B1(n352), .B2(n351), .A(n350), .ZN(\U1/fr[9] ) );
  INV_X1 U514 ( .A(n353), .ZN(\U1/large_p[5] ) );
  NAND2_X1 U515 ( .A1(\U1/large_p[5] ), .A2(n354), .ZN(n359) );
  NAND2_X1 U516 ( .A1(\U1/sig_small_shifted[7] ), .A2(n183), .ZN(n361) );
  INV_X1 U517 ( .A(n361), .ZN(n357) );
  MUX2_X1 U518 ( .A(n356), .B(n355), .S(n72), .Z(n360) );
  NAND2_X1 U519 ( .A1(n357), .A2(n360), .ZN(n365) );
  AOI21_X1 U520 ( .B1(n359), .B2(n358), .A(n47), .ZN(\U1/fr[8] ) );
  INV_X1 U521 ( .A(n360), .ZN(\U1/large_p[4] ) );
  NAND2_X1 U522 ( .A1(n361), .A2(\U1/large_p[4] ), .ZN(n366) );
  NAND2_X1 U523 ( .A1(n107), .A2(n200), .ZN(n368) );
  MUX2_X1 U524 ( .A(n363), .B(n362), .S(n73), .Z(n367) );
  NAND2_X1 U525 ( .A1(n106), .A2(n367), .ZN(n372) );
  INV_X1 U526 ( .A(n372), .ZN(n364) );
  AOI21_X1 U527 ( .B1(n366), .B2(n365), .A(n364), .ZN(\U1/fr[7] ) );
  INV_X1 U528 ( .A(n367), .ZN(\U1/large_p[3] ) );
  NAND2_X1 U529 ( .A1(\U1/large_p[3] ), .A2(n368), .ZN(n373) );
  NAND2_X1 U530 ( .A1(\U1/sig_small_shifted[5] ), .A2(n182), .ZN(n375) );
  INV_X1 U531 ( .A(n375), .ZN(n371) );
  MUX2_X1 U532 ( .A(n370), .B(n369), .S(n73), .Z(n374) );
  INV_X1 U533 ( .A(n374), .ZN(\U1/large_p[2] ) );
  NAND2_X1 U534 ( .A1(\U1/large_p[2] ), .A2(n375), .ZN(n381) );
  NAND2_X1 U535 ( .A1(\U1/sig_small_shifted[4] ), .A2(n183), .ZN(n383) );
  INV_X1 U536 ( .A(n383), .ZN(n378) );
  MUX2_X1 U537 ( .A(n377), .B(n376), .S(n72), .Z(n382) );
  NAND2_X1 U538 ( .A1(n378), .A2(n382), .ZN(n387) );
  INV_X1 U539 ( .A(n387), .ZN(n379) );
  AOI21_X1 U540 ( .B1(n381), .B2(n380), .A(n379), .ZN(\U1/fr[5] ) );
  INV_X1 U541 ( .A(n382), .ZN(\U1/large_p[1] ) );
  NAND2_X1 U542 ( .A1(\U1/large_p[1] ), .A2(n383), .ZN(n388) );
  NAND2_X1 U543 ( .A1(\U1/sig_small_shifted[3] ), .A2(n183), .ZN(n390) );
  MUX2_X1 U544 ( .A(n385), .B(n384), .S(n73), .Z(n389) );
  INV_X1 U545 ( .A(n392), .ZN(n386) );
  AOI21_X1 U546 ( .B1(n388), .B2(n387), .A(n386), .ZN(\U1/fr[4] ) );
  INV_X1 U547 ( .A(n389), .ZN(\U1/large_p[0] ) );
  NAND2_X1 U548 ( .A1(\U1/large_p[0] ), .A2(n390), .ZN(n393) );
  NAND2_X1 U549 ( .A1(n145), .A2(n183), .ZN(n394) );
  INV_X1 U550 ( .A(n394), .ZN(n391) );
  AOI21_X1 U551 ( .B1(n393), .B2(n392), .A(n391), .ZN(\U1/fr[3] ) );
  NAND2_X1 U552 ( .A1(\U1/sig_small_shifted[1] ), .A2(n183), .ZN(n448) );
  INV_X1 U553 ( .A(n448), .ZN(n395) );
  NOR2_X1 U554 ( .A1(n395), .A2(n394), .ZN(\U1/fr[2] ) );
  NAND2_X1 U555 ( .A1(n190), .A2(\U1/mag_exp_diff[0] ), .ZN(n437) );
  OAI21_X1 U556 ( .B1(n95), .B2(n25), .A(n190), .ZN(n397) );
  NAND2_X1 U557 ( .A1(n437), .A2(n397), .ZN(n414) );
  INV_X1 U558 ( .A(\U1/mag_exp_diff[3] ), .ZN(n420) );
  NAND2_X1 U559 ( .A1(n95), .A2(n25), .ZN(n430) );
  OAI21_X1 U560 ( .B1(n420), .B2(n430), .A(n188), .ZN(n417) );
  INV_X1 U561 ( .A(n414), .ZN(n399) );
  OAI211_X1 U562 ( .C1(n212), .C2(n213), .A(n420), .B(n430), .ZN(n440) );
  AOI211_X1 U563 ( .C1(n44), .C2(n440), .A(n38), .B(n80), .ZN(n396) );
  OAI211_X1 U564 ( .C1(n399), .C2(n398), .A(n397), .B(n396), .ZN(n410) );
  NAND2_X1 U565 ( .A1(n212), .A2(n420), .ZN(n404) );
  INV_X1 U566 ( .A(n402), .ZN(n401) );
  OAI211_X1 U567 ( .C1(n401), .C2(n91), .A(n143), .B(n35), .ZN(n409) );
  NOR3_X1 U568 ( .A1(n128), .A2(n45), .A3(n98), .ZN(n407) );
  NAND2_X1 U569 ( .A1(n96), .A2(\U1/mag_exp_diff[0] ), .ZN(n438) );
  INV_X1 U570 ( .A(n438), .ZN(n403) );
  OAI21_X1 U571 ( .B1(n403), .B2(n402), .A(n110), .ZN(n406) );
  OAI21_X1 U572 ( .B1(n93), .B2(n146), .A(n404), .ZN(n405) );
  OAI211_X1 U573 ( .C1(n407), .C2(n420), .A(n405), .B(n406), .ZN(n408) );
  NOR3_X1 U574 ( .A1(n408), .A2(n409), .A3(n410), .ZN(n412) );
  INV_X1 U575 ( .A(\U1/mag_exp_diff[7] ), .ZN(n411) );
  OAI21_X1 U576 ( .B1(n412), .B2(n188), .A(n411), .ZN(n413) );
  NAND2_X1 U577 ( .A1(n420), .A2(n188), .ZN(n422) );
  NAND3_X1 U578 ( .A1(n50), .A2(n415), .A3(n433), .ZN(n416) );
  AOI211_X1 U579 ( .C1(n422), .C2(n416), .A(\U1/mag_exp_diff[6] ), .B(
        \U1/mag_exp_diff[5] ), .ZN(n445) );
  INV_X1 U580 ( .A(n417), .ZN(n418) );
  OAI21_X1 U581 ( .B1(n212), .B2(n437), .A(n418), .ZN(n429) );
  OAI21_X1 U582 ( .B1(n212), .B2(n420), .A(n188), .ZN(n428) );
  NAND3_X1 U583 ( .A1(n436), .A2(n104), .A3(n421), .ZN(n427) );
  INV_X1 U584 ( .A(n422), .ZN(n425) );
  AOI22_X1 U585 ( .A1(n425), .A2(n212), .B1(n424), .B2(n29), .ZN(n426) );
  INV_X1 U586 ( .A(n430), .ZN(n442) );
  INV_X1 U587 ( .A(n437), .ZN(n431) );
  OAI221_X1 U588 ( .B1(n438), .B2(n94), .C1(n213), .C2(n433), .A(n432), .ZN(
        n441) );
  AOI22_X1 U589 ( .A1(\U1/mag_exp_diff[0] ), .A2(n30), .B1(n148), .B2(n190), 
        .ZN(n435) );
  OAI221_X1 U590 ( .B1(n155), .B2(n438), .C1(n437), .C2(n436), .A(n435), .ZN(
        n439) );
  AOI222_X1 U591 ( .A1(n442), .A2(n441), .B1(n8), .B2(n440), .C1(n95), .C2(
        n439), .ZN(n443) );
  NAND2_X1 U592 ( .A1(n37), .A2(n447), .ZN(n481) );
  NOR2_X1 U593 ( .A1(n198), .A2(n448), .ZN(\U1/fr[1] ) );
  INV_X1 U594 ( .A(inst_a[31]), .ZN(n489) );
  INV_X1 U595 ( .A(inst_b[31]), .ZN(n488) );
  OAI22_X1 U596 ( .A1(n180), .A2(n489), .B1(n184), .B2(n488), .ZN(n450) );
  MUX2_X1 U597 ( .A(inst_b[31]), .B(inst_a[31]), .S(n186), .Z(n449) );
  INV_X1 U598 ( .A(n451), .ZN(\U1/large_p[30] ) );
  INV_X1 U599 ( .A(n452), .ZN(\U1/large_p[29] ) );
  INV_X1 U600 ( .A(n453), .ZN(\U1/large_p[28] ) );
  XOR2_X1 U601 ( .A(n455), .B(n205), .Z(\U1/sig_aligned2[26] ) );
  XOR2_X1 U602 ( .A(n456), .B(n206), .Z(\U1/sig_aligned2[25] ) );
  XOR2_X1 U603 ( .A(n457), .B(n206), .Z(\U1/sig_aligned2[24] ) );
  XOR2_X1 U604 ( .A(n458), .B(n205), .Z(\U1/sig_aligned2[23] ) );
  XOR2_X1 U605 ( .A(n459), .B(n205), .Z(\U1/sig_aligned2[22] ) );
  XOR2_X1 U606 ( .A(n460), .B(n205), .Z(\U1/sig_aligned2[21] ) );
  XOR2_X1 U607 ( .A(n461), .B(n205), .Z(\U1/sig_aligned2[20] ) );
  INV_X1 U608 ( .A(\U1/sig_small_shifted[19] ), .ZN(n462) );
  XOR2_X1 U609 ( .A(n463), .B(n206), .Z(\U1/sig_aligned2[18] ) );
  XOR2_X1 U610 ( .A(n464), .B(n206), .Z(\U1/sig_aligned2[17] ) );
  INV_X1 U611 ( .A(\U1/sig_small_shifted[16] ), .ZN(n465) );
  INV_X1 U612 ( .A(\U1/sig_small_shifted[15] ), .ZN(n466) );
  INV_X1 U613 ( .A(\U1/sig_small_shifted[14] ), .ZN(n467) );
  INV_X1 U614 ( .A(\U1/sig_small_shifted[13] ), .ZN(n468) );
  INV_X1 U615 ( .A(\U1/sig_small_shifted[12] ), .ZN(n469) );
  INV_X1 U616 ( .A(\U1/sig_small_shifted[11] ), .ZN(n470) );
  INV_X1 U617 ( .A(\U1/sig_small_shifted[10] ), .ZN(n471) );
  INV_X1 U618 ( .A(\U1/sig_small_shifted[9] ), .ZN(n472) );
  INV_X1 U619 ( .A(\U1/sig_small_shifted[8] ), .ZN(n473) );
  INV_X1 U620 ( .A(\U1/sig_small_shifted[7] ), .ZN(n474) );
  INV_X1 U621 ( .A(\U1/sig_small_shifted[6] ), .ZN(n475) );
  INV_X1 U622 ( .A(\U1/sig_small_shifted[5] ), .ZN(n476) );
  AOI221_X1 U623 ( .B1(\U1/sig_small_shifted[5] ), .B2(n33), .C1(n206), .C2(
        n476), .A(n24), .ZN(\U1/sig_aligned2[5] ) );
  INV_X1 U624 ( .A(\U1/sig_small_shifted[4] ), .ZN(n477) );
  INV_X1 U625 ( .A(\U1/sig_small_shifted[3] ), .ZN(n478) );
  AOI221_X1 U626 ( .B1(\U1/sig_small_shifted[3] ), .B2(n33), .C1(n205), .C2(
        n478), .A(n24), .ZN(\U1/sig_aligned2[3] ) );
  INV_X1 U627 ( .A(\U1/sig_small_shifted[2] ), .ZN(n479) );
  INV_X1 U628 ( .A(\U1/sig_small_shifted[1] ), .ZN(n480) );
  XOR2_X1 U629 ( .A(n481), .B(n206), .Z(\U1/sig_aligned2[0] ) );
  INV_X1 U630 ( .A(\U1/adder_output[25] ), .ZN(n518) );
  INV_X1 U631 ( .A(n65), .ZN(n528) );
  NAND3_X1 U632 ( .A1(n518), .A2(n3), .A3(n501), .ZN(n482) );
  INV_X1 U633 ( .A(n482), .ZN(n617) );
  AND2_X1 U634 ( .A1(\U1/num_zeros_path1_adj[3] ), .A2(n202), .ZN(
        \U1/num_zeros_used[3] ) );
  INV_X1 U635 ( .A(n59), .ZN(n529) );
  OAI22_X1 U636 ( .A1(n201), .A2(n13), .B1(\U1/num_zeros_path1_adj[1] ), .B2(
        n13), .ZN(n483) );
  INV_X1 U637 ( .A(n483), .ZN(n695) );
  NOR4_X1 U638 ( .A1(\U1/large_p[24] ), .A2(\U1/large_p[23] ), .A3(
        \U1/large_p[25] ), .A4(\U1/large_p[26] ), .ZN(n485) );
  NOR4_X1 U639 ( .A1(\U1/large_p[27] ), .A2(\U1/large_p[28] ), .A3(
        \U1/large_p[29] ), .A4(\U1/large_p[30] ), .ZN(n484) );
  NAND2_X1 U640 ( .A1(n485), .A2(n484), .ZN(n656) );
  NOR4_X1 U641 ( .A1(n206), .A2(n214), .A3(n216), .A4(n215), .ZN(n486) );
  NAND4_X1 U642 ( .A1(\U1/num_zeros_path1[3] ), .A2(\U1/num_zeros_path1[4] ), 
        .A3(n84), .A4(n486), .ZN(n687) );
  NAND2_X1 U643 ( .A1(n656), .A2(n687), .ZN(n691) );
  INV_X1 U644 ( .A(n691), .ZN(n555) );
  INV_X1 U645 ( .A(\U1/Elz[6] ), .ZN(n678) );
  INV_X1 U646 ( .A(\U1/Elz[5] ), .ZN(n675) );
  INV_X1 U647 ( .A(\U1/Elz[4] ), .ZN(n672) );
  NAND3_X1 U648 ( .A1(n119), .A2(n170), .A3(n166), .ZN(n661) );
  INV_X1 U649 ( .A(n661), .ZN(n556) );
  INV_X1 U650 ( .A(inst_rnd[0]), .ZN(n689) );
  INV_X1 U651 ( .A(inst_rnd[2]), .ZN(n495) );
  MUX2_X1 U652 ( .A(n489), .B(n488), .S(n72), .Z(n491) );
  INV_X1 U653 ( .A(inst_op), .ZN(n490) );
  XOR2_X1 U654 ( .A(n685), .B(inst_rnd[0]), .Z(n492) );
  NAND2_X1 U655 ( .A1(inst_rnd[1]), .A2(n492), .ZN(n496) );
  OAI21_X1 U656 ( .B1(n689), .B2(n495), .A(n496), .ZN(n648) );
  INV_X1 U657 ( .A(n648), .ZN(n494) );
  OAI21_X1 U658 ( .B1(n556), .B2(n115), .A(n494), .ZN(n493) );
  NAND2_X1 U659 ( .A1(n493), .A2(n555), .ZN(status_inst[0]) );
  INV_X1 U660 ( .A(inst_rnd[1]), .ZN(n690) );
  NAND2_X1 U661 ( .A1(n689), .A2(n690), .ZN(n500) );
  NAND2_X1 U662 ( .A1(n494), .A2(n500), .ZN(n650) );
  INV_X1 U663 ( .A(n650), .ZN(n624) );
  NAND3_X1 U664 ( .A1(\U1/adder_output[5] ), .A2(\U1/adder_output[8] ), .A3(
        \U1/adder_output[4] ), .ZN(n511) );
  INV_X1 U665 ( .A(n108), .ZN(n510) );
  INV_X1 U666 ( .A(\U1/adder_output[1] ), .ZN(n502) );
  NAND2_X1 U667 ( .A1(n499), .A2(n501), .ZN(n503) );
  INV_X1 U668 ( .A(\U1/adder_output[3] ), .ZN(n497) );
  INV_X1 U669 ( .A(\U1/adder_output[2] ), .ZN(n647) );
  OAI222_X1 U670 ( .A1(n2), .A2(n502), .B1(n503), .B2(n497), .C1(n501), .C2(
        n647), .ZN(n560) );
  NAND3_X1 U671 ( .A1(n496), .A2(n495), .A3(n500), .ZN(n508) );
  INV_X1 U672 ( .A(n560), .ZN(n506) );
  INV_X1 U673 ( .A(\U1/adder_output[4] ), .ZN(n498) );
  OAI222_X1 U674 ( .A1(n2), .A2(n647), .B1(n503), .B2(n498), .C1(n497), .C2(
        n501), .ZN(n563) );
  NOR3_X1 U675 ( .A1(n563), .A2(inst_rnd[2]), .A3(n500), .ZN(n505) );
  OAI22_X1 U676 ( .A1(n502), .A2(n501), .B1(n503), .B2(n647), .ZN(n559) );
  INV_X1 U677 ( .A(n559), .ZN(n504) );
  AOI21_X1 U678 ( .B1(\U1/adder_output[1] ), .B2(n18), .A(\U1/adder_output[0] ), .ZN(n557) );
  OAI211_X1 U679 ( .C1(n506), .C2(n505), .A(n504), .B(n557), .ZN(n507) );
  OAI211_X1 U680 ( .C1(n560), .C2(n648), .A(n508), .B(n507), .ZN(n618) );
  INV_X1 U681 ( .A(\U1/adder_output[9] ), .ZN(n509) );
  NOR4_X1 U682 ( .A1(n618), .A2(n510), .A3(n511), .A4(n509), .ZN(n527) );
  NAND3_X1 U683 ( .A1(n197), .A2(n27), .A3(\U1/adder_output[3] ), .ZN(n515) );
  INV_X1 U684 ( .A(\U1/adder_output[23] ), .ZN(n514) );
  INV_X1 U685 ( .A(\U1/adder_output[6] ), .ZN(n513) );
  INV_X1 U686 ( .A(\U1/adder_output[7] ), .ZN(n512) );
  NOR4_X1 U687 ( .A1(n515), .A2(n514), .A3(n513), .A4(n512), .ZN(n526) );
  NAND3_X1 U688 ( .A1(n28), .A2(\U1/adder_output[14] ), .A3(
        \U1/adder_output[15] ), .ZN(n519) );
  INV_X1 U689 ( .A(\U1/adder_output[19] ), .ZN(n517) );
  INV_X1 U690 ( .A(n81), .ZN(n516) );
  NOR4_X1 U691 ( .A1(n519), .A2(n518), .A3(n517), .A4(n516), .ZN(n525) );
  NAND3_X1 U692 ( .A1(\U1/adder_output[13] ), .A2(\U1/adder_output[11] ), .A3(
        \U1/adder_output[12] ), .ZN(n523) );
  INV_X1 U693 ( .A(n34), .ZN(n522) );
  INV_X1 U694 ( .A(\U1/adder_output[17] ), .ZN(n521) );
  INV_X1 U695 ( .A(\U1/adder_output[18] ), .ZN(n520) );
  NOR4_X1 U696 ( .A1(n523), .A2(n522), .A3(n521), .A4(n520), .ZN(n524) );
  NAND4_X1 U697 ( .A1(n527), .A2(n526), .A3(n525), .A4(n524), .ZN(n646) );
  OAI21_X1 U698 ( .B1(n529), .B2(n646), .A(n3), .ZN(n649) );
  NAND2_X1 U699 ( .A1(n649), .A2(n530), .ZN(n619) );
  OAI21_X1 U700 ( .B1(n619), .B2(n624), .A(n64), .ZN(status_inst[1]) );
  MUX2_X1 U701 ( .A(n532), .B(n531), .S(n73), .Z(n542) );
  MUX2_X1 U702 ( .A(n534), .B(n533), .S(n73), .Z(n541) );
  MUX2_X1 U703 ( .A(n536), .B(n535), .S(n72), .Z(n540) );
  MUX2_X1 U704 ( .A(n538), .B(n537), .S(n73), .Z(n539) );
  NOR4_X1 U705 ( .A1(n542), .A2(n541), .A3(n540), .A4(n539), .ZN(n554) );
  MUX2_X1 U706 ( .A(n544), .B(n543), .S(n72), .Z(n552) );
  MUX2_X1 U707 ( .A(n546), .B(n545), .S(n72), .Z(n551) );
  MUX2_X1 U708 ( .A(n548), .B(n547), .S(n180), .Z(n550) );
  NOR4_X1 U709 ( .A1(n552), .A2(n551), .A3(n550), .A4(n549), .ZN(n553) );
  NAND2_X1 U710 ( .A1(n177), .A2(n697), .ZN(n623) );
  INV_X1 U711 ( .A(n623), .ZN(status_inst[2]) );
  OAI21_X1 U712 ( .B1(n556), .B2(n115), .A(n555), .ZN(n562) );
  INV_X1 U713 ( .A(n562), .ZN(status_inst[3]) );
  INV_X1 U714 ( .A(n557), .ZN(n558) );
  NOR4_X1 U715 ( .A1(status_inst[4]), .A2(n16), .A3(n559), .A4(n558), .ZN(n561) );
  NAND2_X1 U716 ( .A1(n562), .A2(n561), .ZN(status_inst[5]) );
  INV_X1 U717 ( .A(n563), .ZN(n566) );
  NAND3_X1 U718 ( .A1(\U1/a_norm_partial_path1[4] ), .A2(n160), .A3(n201), 
        .ZN(n565) );
  NAND3_X1 U719 ( .A1(\U1/a_norm_partial_path1[3] ), .A2(n101), .A3(n201), 
        .ZN(n564) );
  OAI211_X1 U720 ( .C1(n566), .C2(n201), .A(n565), .B(n564), .ZN(
        \U1/a_norm[4] ) );
  INV_X1 U721 ( .A(n608), .ZN(n567) );
  AOI22_X1 U722 ( .A1(\U1/a_norm_partial_path1[26] ), .A2(n567), .B1(
        \U1/a_norm_partial_path1[25] ), .B2(n10), .ZN(n568) );
  OAI21_X1 U723 ( .B1(n201), .B2(n569), .A(n568), .ZN(\U1/a_norm[26] ) );
  INV_X1 U724 ( .A(\U1/a_norm_partial_path1[24] ), .ZN(n573) );
  INV_X1 U725 ( .A(\U1/a_norm_partial_path1[25] ), .ZN(n571) );
  OAI222_X1 U726 ( .A1(n610), .A2(n573), .B1(n608), .B2(n571), .C1(n202), .C2(
        n570), .ZN(\U1/a_norm[25] ) );
  INV_X1 U727 ( .A(\U1/a_norm_partial_path1[23] ), .ZN(n575) );
  OAI222_X1 U728 ( .A1(n610), .A2(n575), .B1(n608), .B2(n573), .C1(n202), .C2(
        n572), .ZN(\U1/a_norm[24] ) );
  INV_X1 U729 ( .A(\U1/a_norm_partial_path1[22] ), .ZN(n577) );
  OAI222_X1 U730 ( .A1(n610), .A2(n577), .B1(n608), .B2(n575), .C1(n202), .C2(
        n574), .ZN(\U1/a_norm[23] ) );
  INV_X1 U731 ( .A(\U1/a_norm_partial_path1[21] ), .ZN(n579) );
  AOI222_X1 U732 ( .A1(n197), .A2(n694), .B1(n27), .B2(n4), .C1(n199), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n576) );
  OAI222_X1 U733 ( .A1(n610), .A2(n579), .B1(n608), .B2(n577), .C1(n202), .C2(
        n576), .ZN(\U1/a_norm[22] ) );
  INV_X1 U734 ( .A(\U1/a_norm_partial_path1[20] ), .ZN(n581) );
  AOI222_X1 U735 ( .A1(n199), .A2(n694), .B1(n197), .B2(n4), .C1(n102), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n578) );
  OAI222_X1 U736 ( .A1(n610), .A2(n581), .B1(n608), .B2(n579), .C1(n202), .C2(
        n578), .ZN(\U1/a_norm[21] ) );
  INV_X1 U737 ( .A(\U1/a_norm_partial_path1[19] ), .ZN(n583) );
  OAI222_X1 U738 ( .A1(n610), .A2(n583), .B1(n608), .B2(n581), .C1(n202), .C2(
        n580), .ZN(\U1/a_norm[20] ) );
  INV_X1 U739 ( .A(\U1/a_norm_partial_path1[18] ), .ZN(n585) );
  OAI222_X1 U740 ( .A1(n610), .A2(n585), .B1(n608), .B2(n583), .C1(n202), .C2(
        n582), .ZN(\U1/a_norm[19] ) );
  INV_X1 U741 ( .A(\U1/a_norm_partial_path1[17] ), .ZN(n587) );
  OAI222_X1 U742 ( .A1(n610), .A2(n587), .B1(n608), .B2(n585), .C1(n202), .C2(
        n584), .ZN(\U1/a_norm[18] ) );
  INV_X1 U743 ( .A(\U1/a_norm_partial_path1[16] ), .ZN(n589) );
  AOI222_X1 U744 ( .A1(n28), .A2(n694), .B1(n136), .B2(n4), .C1(n55), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n586) );
  OAI222_X1 U745 ( .A1(n610), .A2(n589), .B1(n608), .B2(n587), .C1(n202), .C2(
        n586), .ZN(\U1/a_norm[17] ) );
  INV_X1 U746 ( .A(\U1/a_norm_partial_path1[15] ), .ZN(n591) );
  OAI222_X1 U747 ( .A1(n610), .A2(n591), .B1(n608), .B2(n589), .C1(n202), .C2(
        n588), .ZN(\U1/a_norm[16] ) );
  INV_X1 U748 ( .A(\U1/a_norm_partial_path1[14] ), .ZN(n593) );
  OAI222_X1 U749 ( .A1(n610), .A2(n593), .B1(n608), .B2(n591), .C1(n202), .C2(
        n590), .ZN(\U1/a_norm[15] ) );
  INV_X1 U750 ( .A(\U1/a_norm_partial_path1[13] ), .ZN(n595) );
  AOI222_X1 U751 ( .A1(n75), .A2(n694), .B1(\U1/adder_output[14] ), .B2(n18), 
        .C1(n111), .C2(\U1/U1/num_of_zeros[1] ), .ZN(n592) );
  OAI222_X1 U752 ( .A1(n610), .A2(n595), .B1(n608), .B2(n593), .C1(n202), .C2(
        n592), .ZN(\U1/a_norm[14] ) );
  INV_X1 U753 ( .A(\U1/a_norm_partial_path1[12] ), .ZN(n597) );
  AOI222_X1 U754 ( .A1(n111), .A2(n694), .B1(n75), .B2(n18), .C1(n149), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n594) );
  OAI222_X1 U755 ( .A1(n610), .A2(n597), .B1(n608), .B2(n595), .C1(n202), .C2(
        n594), .ZN(\U1/a_norm[13] ) );
  INV_X1 U756 ( .A(\U1/a_norm_partial_path1[11] ), .ZN(n599) );
  INV_X1 U757 ( .A(\U1/a_norm_partial_path1[10] ), .ZN(n601) );
  OAI222_X1 U758 ( .A1(n610), .A2(n601), .B1(n608), .B2(n599), .C1(n202), .C2(
        n598), .ZN(\U1/a_norm[11] ) );
  INV_X1 U759 ( .A(\U1/a_norm_partial_path1[9] ), .ZN(n603) );
  AOI222_X1 U760 ( .A1(\U1/adder_output[9] ), .A2(n694), .B1(n150), .B2(n4), 
        .C1(\U1/adder_output[8] ), .C2(\U1/U1/num_of_zeros[1] ), .ZN(n600) );
  OAI222_X1 U761 ( .A1(n610), .A2(n603), .B1(n608), .B2(n601), .C1(n202), .C2(
        n600), .ZN(\U1/a_norm[10] ) );
  INV_X1 U762 ( .A(\U1/a_norm_partial_path1[8] ), .ZN(n605) );
  AOI222_X1 U763 ( .A1(\U1/adder_output[8] ), .A2(n694), .B1(
        \U1/adder_output[9] ), .B2(n4), .C1(\U1/adder_output[7] ), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n602) );
  OAI222_X1 U764 ( .A1(n610), .A2(n605), .B1(n608), .B2(n603), .C1(n202), .C2(
        n602), .ZN(\U1/a_norm[9] ) );
  INV_X1 U765 ( .A(\U1/a_norm_partial_path1[7] ), .ZN(n607) );
  AOI222_X1 U766 ( .A1(\U1/adder_output[7] ), .A2(n694), .B1(
        \U1/adder_output[8] ), .B2(n18), .C1(\U1/adder_output[6] ), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n604) );
  OAI222_X1 U767 ( .A1(n610), .A2(n607), .B1(n608), .B2(n605), .C1(n202), .C2(
        n604), .ZN(\U1/a_norm[8] ) );
  INV_X1 U768 ( .A(\U1/a_norm_partial_path1[6] ), .ZN(n609) );
  AOI222_X1 U769 ( .A1(\U1/adder_output[6] ), .A2(n694), .B1(
        \U1/adder_output[7] ), .B2(n18), .C1(\U1/adder_output[5] ), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n606) );
  OAI222_X1 U770 ( .A1(n610), .A2(n609), .B1(n608), .B2(n607), .C1(n202), .C2(
        n606), .ZN(\U1/a_norm[7] ) );
  AOI222_X1 U771 ( .A1(\U1/adder_output[5] ), .A2(n694), .B1(
        \U1/adder_output[6] ), .B2(n18), .C1(\U1/adder_output[4] ), .C2(
        \U1/U1/num_of_zeros[1] ), .ZN(n613) );
  NAND3_X1 U772 ( .A1(\U1/a_norm_partial_path1[6] ), .A2(n160), .A3(n201), 
        .ZN(n612) );
  NAND3_X1 U773 ( .A1(\U1/a_norm_partial_path1[5] ), .A2(n101), .A3(n201), 
        .ZN(n611) );
  OAI211_X1 U774 ( .C1(n201), .C2(n613), .A(n612), .B(n611), .ZN(
        \U1/a_norm[6] ) );
  AOI222_X1 U775 ( .A1(\U1/adder_output[4] ), .A2(n694), .B1(
        \U1/adder_output[5] ), .B2(n18), .C1(\U1/U1/num_of_zeros[1] ), .C2(
        \U1/adder_output[3] ), .ZN(n616) );
  NAND3_X1 U776 ( .A1(\U1/a_norm_partial_path1[5] ), .A2(n160), .A3(n201), 
        .ZN(n615) );
  NAND3_X1 U777 ( .A1(\U1/a_norm_partial_path1[4] ), .A2(n101), .A3(n201), 
        .ZN(n614) );
  OAI211_X1 U778 ( .C1(n201), .C2(n616), .A(n615), .B(n614), .ZN(
        \U1/a_norm[5] ) );
  INV_X1 U779 ( .A(n5), .ZN(n620) );
  INV_X1 U780 ( .A(n656), .ZN(n684) );
  NAND3_X1 U781 ( .A1(n169), .A2(n170), .A3(n166), .ZN(n630) );
  AOI221_X1 U782 ( .B1(n135), .B2(\U1/a_norm[4] ), .C1(\U1/frac1[0] ), .C2(
        n204), .A(n167), .ZN(n625) );
  INV_X1 U783 ( .A(n625), .ZN(z_inst[0]) );
  AOI221_X1 U784 ( .B1(n134), .B2(\U1/a_norm[6] ), .C1(\U1/frac1[2] ), .C2(
        n204), .A(n167), .ZN(n626) );
  INV_X1 U785 ( .A(n626), .ZN(z_inst[2]) );
  AOI221_X1 U786 ( .B1(n134), .B2(\U1/a_norm[7] ), .C1(\U1/frac1[3] ), .C2(
        n204), .A(n167), .ZN(n627) );
  INV_X1 U787 ( .A(n627), .ZN(z_inst[3]) );
  AOI221_X1 U788 ( .B1(n159), .B2(\U1/a_norm[8] ), .C1(\U1/frac1[4] ), .C2(
        n204), .A(n167), .ZN(n628) );
  INV_X1 U789 ( .A(n628), .ZN(z_inst[4]) );
  AOI221_X1 U790 ( .B1(n135), .B2(\U1/a_norm[9] ), .C1(\U1/frac1[5] ), .C2(
        n204), .A(n167), .ZN(n629) );
  INV_X1 U791 ( .A(n629), .ZN(z_inst[5]) );
  AOI221_X1 U792 ( .B1(n159), .B2(\U1/a_norm[10] ), .C1(\U1/frac1[6] ), .C2(
        n204), .A(n167), .ZN(n631) );
  INV_X1 U793 ( .A(n631), .ZN(z_inst[6]) );
  AOI221_X1 U794 ( .B1(n159), .B2(\U1/a_norm[11] ), .C1(\U1/frac1[7] ), .C2(
        n204), .A(n167), .ZN(n632) );
  INV_X1 U795 ( .A(n632), .ZN(z_inst[7]) );
  AOI221_X1 U796 ( .B1(n134), .B2(\U1/a_norm[13] ), .C1(\U1/frac1[9] ), .C2(
        n204), .A(n167), .ZN(n633) );
  INV_X1 U797 ( .A(n633), .ZN(z_inst[9]) );
  AOI221_X1 U798 ( .B1(n134), .B2(\U1/a_norm[14] ), .C1(\U1/frac1[10] ), .C2(
        n204), .A(n167), .ZN(n634) );
  INV_X1 U799 ( .A(n634), .ZN(z_inst[10]) );
  AOI221_X1 U800 ( .B1(n135), .B2(\U1/a_norm[15] ), .C1(\U1/frac1[11] ), .C2(
        n203), .A(n167), .ZN(n635) );
  INV_X1 U801 ( .A(n635), .ZN(z_inst[11]) );
  NAND3_X1 U802 ( .A1(n162), .A2(n170), .A3(n166), .ZN(n641) );
  AOI221_X1 U803 ( .B1(n161), .B2(\U1/a_norm[16] ), .C1(\U1/frac1[12] ), .C2(
        n203), .A(n167), .ZN(n636) );
  INV_X1 U804 ( .A(n636), .ZN(z_inst[12]) );
  AOI221_X1 U805 ( .B1(n20), .B2(\U1/a_norm[17] ), .C1(\U1/frac1[13] ), .C2(
        n203), .A(n167), .ZN(n637) );
  INV_X1 U806 ( .A(n637), .ZN(z_inst[13]) );
  AOI221_X1 U807 ( .B1(n21), .B2(\U1/a_norm[18] ), .C1(\U1/frac1[14] ), .C2(
        n203), .A(n167), .ZN(n638) );
  INV_X1 U808 ( .A(n638), .ZN(z_inst[14]) );
  AOI221_X1 U809 ( .B1(n21), .B2(\U1/a_norm[19] ), .C1(\U1/frac1[15] ), .C2(
        n203), .A(n167), .ZN(n639) );
  INV_X1 U810 ( .A(n639), .ZN(z_inst[15]) );
  AOI221_X1 U811 ( .B1(n20), .B2(\U1/a_norm[20] ), .C1(\U1/frac1[16] ), .C2(
        n203), .A(n167), .ZN(n640) );
  INV_X1 U812 ( .A(n640), .ZN(z_inst[16]) );
  AOI221_X1 U813 ( .B1(n21), .B2(\U1/a_norm[21] ), .C1(\U1/frac1[17] ), .C2(
        n203), .A(n167), .ZN(n642) );
  INV_X1 U814 ( .A(n642), .ZN(z_inst[17]) );
  AOI221_X1 U815 ( .B1(n161), .B2(\U1/a_norm[22] ), .C1(\U1/frac1[18] ), .C2(
        n203), .A(n167), .ZN(n643) );
  INV_X1 U816 ( .A(n643), .ZN(z_inst[18]) );
  AOI221_X1 U817 ( .B1(n20), .B2(\U1/a_norm[23] ), .C1(\U1/frac1[19] ), .C2(
        n203), .A(n167), .ZN(n644) );
  INV_X1 U818 ( .A(n644), .ZN(z_inst[19]) );
  AOI221_X1 U819 ( .B1(n161), .B2(\U1/a_norm[24] ), .C1(\U1/frac1[20] ), .C2(
        n203), .A(n167), .ZN(n645) );
  INV_X1 U820 ( .A(n645), .ZN(z_inst[20]) );
  OAI21_X1 U821 ( .B1(n647), .B2(n646), .A(n697), .ZN(n653) );
  INV_X1 U822 ( .A(n653), .ZN(n657) );
  NAND4_X1 U823 ( .A1(n657), .A2(n656), .A3(n648), .A4(n687), .ZN(n662) );
  INV_X1 U824 ( .A(n662), .ZN(n655) );
  OAI21_X1 U825 ( .B1(n153), .B2(n650), .A(n649), .ZN(n654) );
  INV_X1 U826 ( .A(n654), .ZN(n652) );
  NAND2_X1 U827 ( .A1(\U1/Elz_12 ), .A2(n656), .ZN(n663) );
  INV_X1 U828 ( .A(n681), .ZN(n658) );
  NAND3_X1 U829 ( .A1(\U1/Elz[0] ), .A2(n663), .A3(n658), .ZN(n659) );
  OAI211_X1 U830 ( .C1(n661), .C2(n662), .A(n660), .B(n659), .ZN(z_inst[23])
         );
  INV_X1 U831 ( .A(\U1/Elz[1] ), .ZN(n664) );
  NOR3_X1 U832 ( .A1(n7), .A2(n681), .A3(n664), .ZN(n665) );
  AOI221_X1 U833 ( .B1(n171), .B2(\U1/large_p[24] ), .C1(\U1/E1[1] ), .C2(n172), .A(n665), .ZN(n666) );
  INV_X1 U834 ( .A(n666), .ZN(z_inst[24]) );
  INV_X1 U835 ( .A(\U1/Elz[2] ), .ZN(n667) );
  NOR3_X1 U836 ( .A1(n7), .A2(n681), .A3(n667), .ZN(n668) );
  AOI221_X1 U837 ( .B1(n171), .B2(\U1/large_p[25] ), .C1(\U1/E1[2] ), .C2(n172), .A(n668), .ZN(n669) );
  INV_X1 U838 ( .A(n669), .ZN(z_inst[25]) );
  AOI221_X1 U839 ( .B1(n171), .B2(\U1/large_p[26] ), .C1(\U1/E1[3] ), .C2(n172), .A(n670), .ZN(n671) );
  INV_X1 U840 ( .A(n671), .ZN(z_inst[26]) );
  NOR3_X1 U841 ( .A1(n19), .A2(n7), .A3(n681), .ZN(n673) );
  AOI221_X1 U842 ( .B1(n171), .B2(\U1/large_p[27] ), .C1(\U1/E1[4] ), .C2(n172), .A(n673), .ZN(n674) );
  INV_X1 U843 ( .A(n674), .ZN(z_inst[27]) );
  AOI221_X1 U844 ( .B1(n171), .B2(\U1/large_p[28] ), .C1(\U1/E1[5] ), .C2(n172), .A(n676), .ZN(n677) );
  INV_X1 U845 ( .A(n677), .ZN(z_inst[28]) );
  NOR3_X1 U846 ( .A1(n63), .A2(n7), .A3(n681), .ZN(n679) );
  AOI221_X1 U847 ( .B1(n171), .B2(\U1/large_p[29] ), .C1(\U1/E1[6] ), .C2(n172), .A(n679), .ZN(n680) );
  INV_X1 U848 ( .A(n680), .ZN(z_inst[29]) );
  AOI221_X1 U849 ( .B1(n171), .B2(\U1/large_p[30] ), .C1(\U1/E1[7] ), .C2(n172), .A(n682), .ZN(n683) );
  INV_X1 U850 ( .A(n683), .ZN(z_inst[30]) );
  OAI21_X1 U851 ( .B1(n684), .B2(n177), .A(n697), .ZN(n688) );
  INV_X1 U852 ( .A(n685), .ZN(n686) );
  NOR4_X1 U853 ( .A1(inst_rnd[2]), .A2(n690), .A3(n205), .A4(n689), .ZN(n692)
         );
  OAI22_X1 U854 ( .A1(n15), .A2(n692), .B1(n15), .B2(n691), .ZN(n693) );
  INV_X1 U855 ( .A(n693), .ZN(z_inst[31]) );
endmodule

