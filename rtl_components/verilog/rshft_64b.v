module DW_shifter_inst( inst_data_in, inst_data_tc, inst_sh, 
                        inst_sh_mode, data_out_inst );

  parameter data_width = 64;
  parameter sh_width = 7;
  parameter inv_mode = 0;
  input [data_width-1 : 0] inst_data_in;
  input inst_data_tc;
  input [sh_width-1 : 0] inst_sh;
  input inst_sh_mode;
  output [data_width-1 : 0] data_out_inst;
  wire [sh_width-1 : 0] inst_sh_n;

  assign inst_sh_n = (~inst_sh)+1;

  // Instance of DW_shifter
  DW_shifter #(data_width, sh_width)
    U1 ( .data_in(inst_data_in),   .data_tc(inst_data_tc),   .sh(inst_sh_n),
         .sh_tc(1'b1),   .sh_mode(inst_sh_mode),
         .data_out(data_out_inst) );

endmodule

