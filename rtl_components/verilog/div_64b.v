module DW_div_inst (a, b, quotient, remainder, divide_by_0);

  parameter dividend_width    = 128;
  parameter divisor_width     = 64;
  parameter tc_mode  = 0;
  parameter rem_mode = 1; // corresponds to "%" in Verilog

  input  [dividend_width-1 : 0] a;
  input  [divisor_width-1 : 0] b;
  output [dividend_width-1 : 0] quotient;
  output [divisor_width-1 : 0] remainder;
  output               divide_by_0;

  // Please add +incdir+$SYNOPSYS/dw/sim_ver+ to your verilog simulator
  // command line (for simulation).

  // instance of DW_div
  DW_div #(dividend_width, divisor_width, tc_mode, rem_mode)
    U1 (.a(a), .b(b),
        .quotient(quotient), .remainder(remainder),
        .divide_by_0(divide_by_0));
endmodule

