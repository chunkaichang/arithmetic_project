#!/usr/bin/env python

"""
Some generator tests.
"""

import os
import itertools
import random
import math_helpers
from time import sleep

def feed_pipe(path, num_nets, num_faults):
    """ Generates a temporary named pipe filled with random faults.

    Args:
        num_nets (int): The number of total nets.
        num_faults (int): The number of faulty nets.

    Returns:
        None; a named pipe is created, filled, and closed as a side effect.

    """

    # dump faults (in hex) to a named pipe
    os.mkfifo(path)
    opipe = os.open(path, os.O_WRONLY)
    injected_fault_ids = []
    for fault_ids, hfault in unique_random_hex_faults(num_nets, num_faults):
        try:
            os.write(opipe, "%s\n" % hfault)
            injected_fault_ids.append(fault_ids)
        except Exception as e:
            break

    os.close(opipe)
    os.remove(path)

    return injected_fault_ids

def unique_random_faults_l(num_nets, num_faults):
    """ Returns an exhaustive, random list of faults.

    Has to load all choices into memory, so it may take a lot.

    Args:
        num_nets (int): The number of total nets.
        num_faults (int): The number of nets per fault.

    Returns:
        A list of fault ID tuples.

    """
    combos = list(itertools.combinations(xrange(num_nets), num_faults))
    random.shuffle(combos)  # in-place shuffle (now random order)
    return combos


def unique_everseen(iterable):
    """ List unique elements, preserving order.

    Remember all elements ever seen.

    Args:
        iterable: The input iterator.

    Returns:
        The target generator.

    """
    seen = set()
    for element in itertools.ifilterfalse(seen.__contains__, iterable):
        seen.add(element)
        yield element


def unique_sample(iterable, number):
    """ Returns a unique sample of number elements from the given iterator.

    Args:
        iterable: The input iterator.

    Returns:
        The target generator.

    """
    return itertools.islice(unique_everseen(iterable), 0, number)


def repeatfunc(func, times=None, *args):
    """ Repeat calls to func with specified arguments.

    Args:
        func: The function.
        times (int or None): The number of repeated calls.
        *args: An arglist of arguments to pass to the function.

    Returns:
        A generator of the results.

    """
    if times is None:
        return itertools.starmap(func, itertools.repeat(args))
    if times < 0:
        raise ValueError("Cannot repeat negative times.")
    return itertools.starmap(func, itertools.repeat(args, times))


def unique_randints(lower_bound, upper_bound, times):
    """ Returns a unique tuple of random bounded integers (inclusive).

    Samples without replacement.

    Args:
        lower_bound (int): The lower bound (inclusive).
        upper_bound (int): The upper bound (inclusive).
        times (int): The number of random samples to draw.

    Returns:
        tuple: A tuple with the results.

    """
    if upper_bound-lower_bound+1 < times:  # sanity check
        raise ValueError("Non-sensical bounds.")
    return unique_sample(repeatfunc(random.randint, None,
                                    lower_bound, upper_bound),
                         number=times)


def unique_random_faults_g(num_nets, num_faults):
    """ Generates an exhaustive, random stream of faults.

    Args:
        num_nets (int): The number of total nets.
        num_faults (int): The number of faulty nets.

    Returns:
        A generator with the results.

    """
    # create an infinite non-unique fault generator
    nonunique_fault_generator = repeatfunc(unique_randints, None,
                                           0, num_nets, num_faults)
    possible_unique_faults = math_helpers.choose(num_nets, num_faults)
    # keep drawing unique combinations from the infinite generator
    # until all options are exhausted
    return unique_sample((tuple(x) for x in nonunique_fault_generator),
                         possible_unique_faults)


def unique_random_hex_faults(num_nets, num_faults):
    """ Generates an exhaustive, random stream of hex-formatted faults.

    Args:
        num_nets (int): The number of total nets.
        num_faults (int): The number of faulty nets.

    Returns:
        A generator with the results.

    """
    def fault_tup_to_bin_str(fault_tup):
        """ Converts a fault combination to a binary string.

        (Uses 1'b1 for faulty and 1'b0 for not faulty.)

        """
        bin_list = [(lambda bp: "1" if bp in fault_tup else "0")(bit_pos)
                    for bit_pos in xrange(num_nets)]  # reversed order
        bin_list.reverse()
        return "".join(bin_list)

    for fault_gen in unique_random_faults_l(num_nets, num_faults):
        yield (tuple(fault_gen), math_helpers.bintohex(fault_tup_to_bin_str(tuple(fault_gen)),
                                      width=num_nets))
