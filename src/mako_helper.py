"""
Mako Template Helpers

Allow for regular python dictionaries to be converted into a more easily accessible format
in Mako. For example, instead of "{$variable[key]}" you can use "{$variable.key}".
"""

# Mako Helpers
class MakoBunch(dict):
    def __init__(self, d):
        """
        Args:
            d (dict): dictionary to convert
        """

        dict.__init__(self, d)
        self.__dict__.update(d)

def to_mako(dl):
    """
    Make a dict into a object (easy for mako)
    
    Args:
        dl (object): object to convert (mainly handles dicts)

    Returns:
        object: Mako friendly object
    """
    if type(dl) is list:
        # If list, just convert elements
        r = []
        for i in dl:
            r.append(to_mako(i))
        return r
    elif type(dl) is dict:
        # Convert dicts
        r = {}
        for k, v in dl.items():
            if isinstance(v, dict):
                v = to_mako(v)
            r[k] = v
        return MakoBunch(r)
