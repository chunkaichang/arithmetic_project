#!/usr/bin/env python

"""
Some math calculation and number representation helpers.
"""

from operator import mul
from fractions import Fraction


def choose(n, k):
    """ Calculate the binomial coefficient n choose k.

    http://stackoverflow.com/questions/3025162/statistics-combinations-in-python

    Very clever. :)

    Args:
        n (int): The total number of items.
        k (int): The number of items to choose.

    """
    return int(reduce(mul, (Fraction(n-i, i+1) for i in range(k)), 1))


def tobin(number, width=64):
    """ Convert a (decimal) number to two's complement binary.

    Args:
        number (int): The number to convert.
        width (int): The width to pad the output to.

    Returns:
        str: A string reperesenting the binary number.

    """
    # test negative
    if number < 0:
        raise ValueError("Number must be positive!")
    else:
        res = bin(number)[2:].rjust(width, '0')

        # test width
        if len(res) > width:
            raise ValueError("Number exceeds the target width!")

        return res


def todec(number, width=64):
    """ Pad a (decimal) number, included for completeness.

    Args:
        number (int): The number to convert.
        width (int): The width to pad the output to.

    Returns:
        str: A string reperesenting the decimal number.

    """
    # test negative
    if number < 0:
        raise ValueError("Number must be positive!")
    else:
        res = str(number).rjust(width, '0')

        # test width
        if len(res) > width:
            raise ValueError("Number exceeds the target width!")

        return res


def tohex(number, width=8):
    """ Convert a (decimal) number to two's complement hex.

    Args:
        number (int): The number to convert.
        width (int): The width to pad the output to.

    Returns:
        str: A string reperesenting the hexadecimal number.

    """
    # test negative
    if number < 0:
        raise ValueError("Number must be positive!")

    # test type
    if isinstance(number, long):  # take off the "L" descriptor
        res = hex(number)[2:-1].rjust(width, '0')
    elif isinstance(number, int):
        res = hex(number)[2:].rjust(width, '0')
    else:
        raise ValueError("Input is not a number.")

    # test width
    if len(res) > width:
        raise ValueError("Number exceeds the target width!")

    return res


def bintohex(binnum, width=64):
    """ Converts a binary number (str) to a hex number (str).

    Args:
        binnum (str): The binary string to convert.
        width (int): the resulting width.

    Returns:
        str: A hexadecimal value in a string.

    """
    return tohex(bintoint(binnum), width)


def bintoint(binnum):
    """ Converts a binary number (str) to a decimal integer.

    Args:
        binnum (str): The binary string to convert.

    Returns:
        int: A decimal integer value.

    """
    return int(binnum.replace(' ', ''), 2)


def hextoint(hexnum):
    """ Converts an unsigned hex number (str) to a decimal integer.

    Args:
        hexnum (str): The hex string to convert.

    Returns:
        int: An integer reperesenting the unsigned hex number.

    """
    return int(hexnum, 16)
