"""
Verilog Helper

Basically, Pyverilog generates some helper files (optimized lexer/parsers). Unfortunately,
if it doesn't know where these are it will keep generating it. This drastically slows down
parsing. This sets up the paths for the parser so they are inserted in the src folder and 
reused each time.

Methods
=======
"""

# Base
import os, sys, shutil
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

# Pyverilog
from pyverilog.vparser import ply
import pyverilog.vparser.parser as vparser

def parse(filelist, table_output_dir="./", preprocess_output='preprocess.output', debug=0):
    """
     Verilog parser front-end (allows for generating/reusing the table outputs)

     Args:
        filelist            (list): verilog files to parse
        table_output_dir    (str): path to table output directory [optional]
        preprocess_output   (str): name of preprocess output [optional]
        debug               (int): debug mode [optional]
    """

    text = vparser.preprocess(filelist, preprocess_output)
    parser = vparser.VerilogParser()

    # Overwrite yacc to look in right directory
    parser.parser = ply.yacc.yacc(module=parser, method="LALR", outputdir=table_output_dir)

    # Move files if generated
    files = ["parser.out", "parsetab.py", "parsetab.pyc"]
    for f in files:
        if os.path.isfile(f):
            shutil.move(f, os.path.join(table_output_dir, f))

    ast = parser.parse(text, debug=debug)
    return ast
