#! /usr/bin/env python
"""
A script that performs fault injection to a RTL gate-level circuit.
Random input is currently used. For each input pattern, the error 
patterns and the masking factor are recorded in an output csv file.  

Usage
========
    $ ./arith_project.py -c <config> 

    See /rtl_component/config/arith_project.yaml for example of config file

"""
# Base imports
import os, sys, argparse, re, subprocess, json, yaml, csv, random
import inject, mask_factor
import fileinput

# Filenames
LOG_FILE = "rtl_inject.log"
CSV_HEADER = ['Masking Factor','Num Output Ports', 'Error Patterns']

def parse_config(config_yml):
    config = None
    with open(config_yml, 'r') as yf:
        config = yaml.load(yf)

    dirs = {}
    if 'dirs' in config:
        dirs = config['dirs']
    cell_libs = {}
    if 'cell_libs' in config:
        cell_libs = config['cell_libs']

    module = {}
    if 'module' in config:
        module = config['module']
    # Lib
    lib = module['cell_lib']
    mapping = None
    rtl_dir = '.'
    if lib in cell_libs:
        rtl_dir = cell_libs[lib]['dir']
        mapping = cell_libs[lib]['mapping']
        lib = os.path.join(rtl_dir, cell_libs[lib]['src'])
    if 'dir' in module:
        rtl_dir = module['dir']
    if rtl_dir in dirs:
        rtl_dir = dirs[rtl_dir]
    rtl_src = os.path.join(rtl_dir, module['rtl_src'])
    group_mappings = inject.parse_group_mapping(mapping, lib, rtl_dir)

    # Cache
    cache = 'cache'
    if 'cache' in config:
        cache = config['cache']

   
    # Experiment
    exp = {  
        'trials' : 1,
        'errors' : 1
    }
    if 'experiment' in config:
        for k, v in config['experiment'].iteritems():
            exp[k] = v

    # Fault
    fault = {
        'type': "FLIP",
        'num' : 1,
        'targets': []
    }
    if 'fault' in config:
        for fk, fv in config['fault'].iteritems():
            fault[fk] = fv

    return lib, rtl_src, module, group_mappings, cache, exp, fault


def input_gen(module):
    input_ports = {}
    for k, v in module['inputs'].iteritems():
        if 'value' in v:
            input_ports[k] = int(v['value'])
        else:
            assert 'width' in v, "Input port (%r) width is not specified" % k
            bit_width = v['width'] 
            lower_bound = 0
            upper_bound = (1 << bit_width) - 1
            input_ports[k] = random.randint(lower_bound, upper_bound)

    return input_ports

def record_result(csv_file, num_errors, correct_vals, iterations):
    row = []
    num_outs = 0
    #Compute Masking Factor
    #Laplace's law of succession is applied to avoid zero probability
    mf = 1.0 - (num_errors+1.0)/(iterations+2.0)

    #Process Error Patterns
    diffs = []
    with open(os.path.join('.', LOG_FILE), "r") as log_f:
        parsed_lines = 0
        for line in log_f:
            rest_line = line.split(':',1)[1] #remove prefix (error x:)
            errors = rest_line.split(',')
            
            for i, val in enumerate(errors):
                err_val = int(errors[i].split(':')[1].strip())
                diff = format(err_val ^ correct_vals[i], 'x') #xor diff in hex  
                diffs.append(diff) 
            #read val 
            parsed_lines += 1
            if parsed_lines >= num_errors:
                num_outs = len(errors)
                break
    row.append(mf)        
    row.append(num_outs)
    row.extend(diffs)
    csv_file.writerow(row)        

def main():
    parser = argparse.ArgumentParser(description="Inject errors to RTL to collect error patterns and compute masking factor")
    parser.add_argument('-c', '--config', required=True, help="Required config file for injection experiments")
    parser.add_argument('-o', '--out_csv', default='arith_project.csv', help="Name of output file")
    parser.add_argument('-i', '--in_csv', default='', help="Path of input pattern file")

    options = parser.parse_args()

    lib, rtl_src, module, group_mappings, cache, exp, fault = parse_config(options.config)
    
    #Invariant ports
    output_ports = {k : 0 for k, v in module['outputs'].iteritems()}
    clk_port = module['clk'] if 'clk' in module else None
    clk_stages = module['stages'] if 'stages' in module else 0

    with open(options.out_csv, "wb") as csv_f:
        out_f = csv.writer(csv_f, dialect='excel')
        out_f.writerow(CSV_HEADER)
        
        no_input = not options.in_csv
        if no_input:
            trials = exp['trials']
            for ti in xrange(trials):
                #Generate random input patterns
                input_ports = input_gen(module)    
                #First run to generate synthesized file
                results = inject.run(lib, rtl_src, module['top_module'], fault['type'], fault['num'], fault['targets'], [], input_ports, output_ports, clk_port, clk_stages, None, None, group_mappings, cache=cache)
                #Redo injection until specifed # of target errors are injected
                result = mask_factor.reinject('.', exp['errors'], fault['num'])
                #Obtain golden output value for comparison
                correct_vals = []
                for k, v in output_ports.iteritems():
                    correct_vals.append(int(result['correct'][k])) 

                #Record outcome in csv 
                record_result(out_f, exp['errors'], correct_vals, result['iterations']) 
        else:
            #Assume header contains port names exactly same as config
            with open(options.in_csv, "rb") as in_f:
                doc = csv.reader(in_f)
                port_names = []
                for header_row in doc:
                    for n in header_row:
                        port_names.append(n)
                    break
                #Each data row corresponds to a trial
                for data_row in doc:
                    input_ports = {}
                    for k, v in module['inputs'].iteritems():
                        if 'value' in v:
                            input_ports[k] = int(v['value'])
                        else:  
                            input_ports[k] = data_row[port_names.index(k)]               
                    #First run to generate synthesized file
                    results = inject.run(lib, rtl_src, module['top_module'], fault['type'], fault['num'], fault['targets'], [], input_ports, output_ports, clk_port, clk_stages, None, None, group_mappings, cache=cache)
                    #Redo injection until specifed # of target errors are injected
                    result = mask_factor.reinject('.', exp['errors'], fault['num'])
                    #Obtain golden output value for comparison
                    correct_vals = []
                    for k, v in output_ports.iteritems():
                        correct_vals.append(int(result['correct'][k])) 

                    #Record outcome in csv 
                    record_result(out_f, exp['errors'], correct_vals, result['iterations']) 
 
  
if __name__ == '__main__':
    main()
