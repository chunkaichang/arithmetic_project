"""
Denotes a "Cacheable" object, an object which is automatically cached (saved) for 
later retrieval during a different run.

Features
========
- Can access just like a regular variable
- Will invalidate due to time constraints or dependencies
- Value dependencies (if one changes, invalidate)
- Cachable dependencies
- File dependencies
- Grouping (group data, to maintain references)

Class
=====
"""

import os, stat, sys, copy
import cPickle as pickle

class Cacheable(object):
    """
    Attributes:
        _path               (str): directory path for cache files
        _groups             (dict): mapping of groups and cache data
        _group_timestamps   (dict): timestamps for each group data
        _group_objs         (dict): Cacheable objects associated with group
    """
    _path = "./"
    _groups = {}
    _group_timestamps = {}
    _group_objs = {}

    def __init__(self, name, value_deps=[], file_deps=[], cache_deps=[], group=None):
        """
        Args:
            name        (str): name of Cacheable
            value_deps  (list): list of value dependencies (actual values)
            file_deps   (list): list of file (name) dependencies
            cache_deps  (list): list of cache_deps (Cacheable)
            group       (str): group name [optional]
        """
        # Name/Data
        self.__dict__['_name'] = name
        self.__dict__['_group'] = group
        if group:
            if group not in Cacheable._group_objs:
                Cacheable._group_objs[group] = {'count': 0, 'objs': []}
            Cacheable._group_objs[group]['count'] += 1
            Cacheable._group_objs[group]['objs'].append(self)

        self.__dict__['_data'] = {}
        self.__dict__['_save_data'] = self.__dict__['_data']
        self.__dict__['_values'] = []
        self.__dict__['_dirty'] = False
        self.__dict__['valid'] = False
        # Dependencies
        self.__dict__['_value_deps'] = value_deps
        self.__dict__['_file_deps'] = file_deps
        self.__dict__['_cache_deps'] = cache_deps
        # Recover
        self.__dict__['_loaded'] = False
        self.__dict__['_values'] = value_deps
        self.__dict__['ended'] = False
    
    # Save upon deletion
    def __del__(self):
        self.save()

    @staticmethod
    def cleanup():
        """
        Cleanup function, mainly used for saving groups.
        """

        for g_name, group in Cacheable._group_objs.iteritems():
            grouping = {}
            dirty = False
            for co in group['objs']:
                pd = {'values': co.__dict__['_values'], 'data': co.__dict__['_save_data']}
                grouping[co.__dict__['_name']] = pd
                if co.__dict__['_dirty']:
                    dirty = True

            if not dirty:
                continue
            filepath = os.path.join(Cacheable._path, "g_" + g_name + ".pkl")
            with open(filepath, 'w') as cache:
                pickle.dump(grouping, cache, -1)

    def end(self):
        """
        End the Cacheable, basically save it and allow it to not track the actual variable anymore.
        """
        if not self.__dict__['_group']:
            self.save()
        else:
            self.__dict__['_save_data'] = copy.deepcopy(self.__dict__['_data'])

        self.__dict__['ended'] = True
    
    def set(self, obj):
        """
        Set the object of the Cacheable

        Args:
            obj (object): Object to set
        """
        self.__dict__['_data'] = obj
        self.__dict__['_save_data'] = obj
        self.__dict__['_dirty'] = True

    def get(self):
        """
        Get the underlying object

        Returns:
            object: underlying object
        """
        if not self.__dict__['_loaded']:
            self._recover()
        return self.__dict__['_data']

    def save(self):
        """
        Save the object to the filesystem
        """
        if not self.__dict__['_group']:
            if self.__dict__['ended'] or not self.__dict__['_dirty']:
                return
            filepath = os.path.join(Cacheable._path, self.__dict__['_name']+".pkl")
            with open(filepath, 'w') as cache:
                pd = {'values': self.__dict__['_values'], 'data': self.__dict__['_save_data']}
                pickle.dump(pd, cache, -1)
            self.__dict__['_dirty'] = False

    def isValid(self):
        """
        Is the Cacheable valid?

        Returns:
            bool: is valid
        """
        if not self.__dict__['_loaded']:
            self._recover()
        return self.__dict__['valid']

    def _recover(self):
        """
        Recover the Cacheable from the cache (filesystem)
        """

        # Recover from pickle
        self.__dict__['_loaded'] = True
        filedate = 0
        if self.__dict__['_group']:
            if self.__dict__['_group'] not in Cacheable._groups:
                filepath = os.path.join(Cacheable._path, "g_" + self.__dict__['_group']+".pkl")
                filedate = 0
                if os.path.isfile(filepath):
                    filedate = os.stat(filepath)[stat.ST_MTIME]
                    with open(filepath, 'r') as cache:
                        try:
                            pd = pickle.load(cache)
                            Cacheable._groups[self.__dict__['_group']] = pd
                            Cacheable._group_timestamps[self.__dict__['_group']] = filedate
                        except:
                            sys.stderr.write("Cache corrupted!\n")
                            self.__dict__['valid'] = False
                            return
                else:
                    self.__dict__['valid'] = False
                    return
            
            filedate = Cacheable._group_timestamps[self.__dict__['_group']]
            if self.__dict__['_name'] in Cacheable._groups[self.__dict__['_group']]:
                pd = Cacheable._groups[self.__dict__['_group']][self.__dict__['_name']]
                self.__dict__['_data'] = pd['data']
                self.__dict__['_save_data'] = self.__dict__['_data']
                self.__dict__['_values'] = pd['values']
                self.__dict__['valid'] = True
            else:
                self.__dict__['valid'] = False
                return

        else:
            filepath = os.path.join(Cacheable._path, self.__dict__['_name']+".pkl")
            if os.path.isfile(filepath):
                filedate = os.stat(filepath)[stat.ST_MTIME]
                with open(filepath, 'r') as cache:
                    try:
                        pd = pickle.load(cache)
                        self.__dict__['_data'] = pd['data']
                        self.__dict__['_save_data'] = self.__dict__['_data']
                        self.__dict__['_values'] = pd['values']
                        self.__dict__['valid'] = True
                    except:
                        sys.stderr.write("Cache corrupted!\n")
                        self.__dict__['valid'] = False
                        return
            else:
                self.__dict__['valid'] = False
                return

        # Check dependencies
        if self.__dict__['valid']:
            for vd in self.__dict__['_value_deps']:
                if vd not in self.__dict__['_values']:
                    self.__dict__['valid'] = False
                    return
            for fd in self.__dict__['_file_deps']:
                if os.path.isfile(fd):
                    m_fd = os.stat(fd)[stat.ST_MTIME]
                    if m_fd > filedate:
                        self.__dict__['valid'] = False
                        return
            for dc in self.__dict__['_cache_deps']:
                if not dc.isValid():
                    self.__dict__['valid'] = False
                    return

    # Object methods
    def __getattr__(self, name):
        return getattr(self.get(), name)

    def __setattr__(self, name, value):
        self.__dict__['_dirty'] = True
        return setattr(self.get(), name, value)

    def __delattr__(self, name):
        return delattr(self.get(), name)

    def __len__(self):
        if type(self.get()) is list:
            return len(self.get())
        return 0

    def __getitem__(self, key):
        if type(self.get()) is list or type(self.get()) is dict:
            return self.get()[key]
        else:
            raise KeyError("Invalid type")

    def __setitem__(self, key, value):
        if type(self.get()) is list or type(self.get()) is dict:
            self.__dict__['_dirty'] = True
            self.get()[key] = value
        else:
            raise KeyError("Invalid type")

    def __contains__(self, item):
        if type(self.get()) is list or type(self.get()) is dict:
            return item in self.get()
        return False

    @classmethod
    def setupPath(cls, path, tag=None):
        """
        Set up the path to save Cacheable objects at

        Args:
            path (str): directory path to save at
        """

        if tag is not None:
            path = os.path.join(path, tag)
        cls._path = path
        exists = os.path.isdir(path) 
        if not exists:
            os.makedirs(path)

        return exists
