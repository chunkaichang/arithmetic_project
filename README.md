High-Speed Arithmetic Project Simulator
=======================================

This is an RTL gate-level fault injector for the project. It aims at studying fault masking capability of
different ALU implementations. To be more specific, we will study the masking factor (prob. of fault masked
by the circuit) and the resultant error patterns (difference between golden and corrupted output.)

Dependencies
------------
- Linux
    - iverilog (apt-get install iverilog)
    - (might not be needed) graphviz (apt-get install graphviz) (`graphviz-devel` for some distribution)
- Python 2.7 
    - pyyaml     (pip install pyyaml)
    - mako       (pip install mako) 
    - pyverilog  (pip install -I pyverilog==0.9.3)
    - pygraphviz (pip install pygraphviz) 

Usage
-----

    src/arith_project.py -c <config> [-i <input pattern file>] [-o <output file name>]   

    - config (c): target circuit config. An example config named `arith_project.yaml` can be found in `rtl_components/configs`. 
    - input  (i): (optional) input patterns for the corresponding circuit. The header row links input values to input port names. See `inputs/` for example input files.
    - output (o): (optional) output file name. By default, the simulator will output a file named `arith_project.csv` including statistics of the injection results.

Limitations
-----------

Clocked circuit injection hasn't been tested yet. 

